<?php

namespace App\Console\Commands;
use Illuminate\Support\Str;
use Caffeinated\Modules\Facades\Module;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class generateFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:files {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate files for module';
    /**
     * @var generateFiles
     */
    protected $files;

    /**
     * generateFiles constructor.
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $data=[
            'Controller'=>'Http\\Controllers\\',
            'Model'=>'Models\\',
            'Repository'=>'Repositories\\',
            'Request'=>'Http\\Requests\\',
        ];
        $slug = $this->argument('module');
        $module = Module::where('slug', $slug)->first();
        if ( is_null($module) ) {
            $this->error('Module '.$slug.'not found');
        }
        $bar = $this->output->createProgressBar(count($data));
        foreach ($data as $k=>$item){
            $bar->advance();
            $global_path=$this->module_class($slug,$item.$module.$k);
            $name = $this->qualifyClass($global_path);
            $path = $this->getPath($global_path);
            // Next, we will generate the path to the location where this class' file should get
            // written. Then, we will build the class and make the proper replacements on the
            // stub files so that it gets the correctly formatted namespace and class name.
            $this->makeDirectory($path);
            $this->files->put($path, $this->buildClass($name,$k));
        }
        $bar->finish();
        $this->info('Files created Successfully');
    }
    /**
     * Parse the class name and format according to the root namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function qualifyClass($name)
    {
        $name = ltrim($name, '\\/');

        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($name, $rootNamespace)) {
            return $name;
        }

        $name = str_replace('/', '\\', $name);

        return $this->qualifyClass(
            $this->getDefaultNamespace(trim($rootNamespace, '\\')).'\\'.$name
        );
    }

    /**
     * @param $rootNamespace
     * @return mixed
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }
    /**
     * @param $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * @param $type
     * @return string
     */
    protected function getStub($type)
    {
        $types=[
            'Controller'=>__DIR__.'/stubs/controller.resource.stub',
            'Model'=>__DIR__.'/stubs/model.stub',
            'Repository'=>__DIR__.'/stubs/repository.stub',
            'Request'=>__DIR__.'/stubs/request.stub',
        ];

        return $types[$type];
    }

    /**
     * @param $slug
     * @param $class
     * @return string
     */
    private function module_class($slug,$class)
    {
        $module = Module::where('slug', $slug);
        $namespace = config('modules.namespace').$module['basename'];
        return "{$namespace}\\{$class}";
    }
    /**
     * Build the directory for the class if necessary.
     *
     * @param  string  $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory(dirname($path))) {

            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * @param $name
     * @param $type
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildClass($name,$type)
    {
        $stub = $this->files->get($this->getStub($type));
        return $this->replaceNamespace($stub, $name)->replaceClass($stub, $name);
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            ['DummyNamespace', 'DummyRootNamespace', 'NamespacedDummyUserModel'],
            [$this->getNamespace($name), $this->rootNamespace(), config('auth.providers.users.model')],
            $stub
        );

        return $this;
    }

    /**
     * Get the full namespace for a given class, without the class name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getNamespace($name)
    {
        return trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace('DummyClass', $class, $stub);
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        return trim($this->argument('name'));
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace();
    }

}
