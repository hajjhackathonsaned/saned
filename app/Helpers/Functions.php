<?php

namespace App\Helpers;

use Aws\Ses\Exception\SesException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Trait Functions
 * @package App\Helpers
 *
 */
trait Functions {

    /**
     * @param $file
     * @param $type
     * @param $folder
     * @return array
     */
    public function uploadImage($file, $type, $folder) {
        try {
            $ext = strtolower($file->getClientOriginalExtension());
            if (!in_array($ext, $this->getExt($type))) {
                return [false, 'allow_extention_error'];
            }
            $subfolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
            $destinationPath = storage_path('app/public/uploads/' . $folder . '/' . $subfolder);
            $fileName = md5($file->getClientOriginalName()) . '-' . rand(9999, 9999999) .
                    '-' . rand(9999, 9999999) . '.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $fileName);
            return [true, $subfolder . $fileName];
        } catch (FileException $exception) {
            return [false, 'unable_upload_file'];
        }
    }

    /**
     * @param $message
     * @param array $numbers
     * @return mixed
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function sendSms($message, array $numbers) {
        try {
            $r = new \App\Repositories\Sms\Gateway(config('sms.gateway'));
        } catch (\Exception $ex) {
            $this->insertSmsLog(config('sms.gateway'), $numbers, config('sms.sender'), $message, $ex->getMessage(), false, '');
        }
        $send = $r->gateway->setUser(config('sms.username'), config('sms.password'))
                ->setNumbers($numbers)
                ->setMessage($message)
                ->setSender(config('sms.sender'))
                ->send();
        $gate_message = isset($send['message']) ? $send['message'] : '';
        $this->insertSmsLog(config('sms.gateway'), $numbers, config('sms.sender'), $message, $send['response'], $send['status'], $gate_message);
        return $send['status'];
    }

    /**
     * @param $gate
     * @param $numbers
     * @param $sender
     * @param $message
     * @param $response
     * @param $status
     * @param $gate_message
     * @return mixed
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function insertSmsLog($gate, $numbers, $sender, $message, $response, $status, $gate_message) {
        $smsRepo = new \App\Repositories\SmsLogRepository(app(), \Illuminate\Support\Collection::make());
        return $smsRepo->create([
                    'numbers' => implode(',', $numbers),
                    'sender' => $sender,
                    'message' => $message,
                    'status' => $status ? 'success' : 'failed',
                    'response' => json_encode($response),
                    'gate_message' => $gate_message,
                    'gateway' => $gate,
        ]);
    }

    /**
     * @param $key
     * @param array $vars
     * @return mixed
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function getTemplateMessage($key,array $vars) {
        $temp_repo = new \App\Repositories\TemplateRepository(app(), \Illuminate\Support\Collection::make());
        $message=$temp_repo->findBy('key_id',$key)->message;
        foreach ($vars as $k=>$val){
            $message=str_replace($k,$val,$message);
        }
        return $message;

    }
    /**
     * @param $view
     * @param $data
     * @param $subject
     * @param $emails
     * @return array
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function sendEmail($view, $data, $subject, $emails) {
        $data_us = $data;
        $setting = [];
        $repo = new \App\Repositories\SettingRepository(app(), \Illuminate\Support\Collection::make());
        $data = $repo->getWhereIn(['mail']);
        foreach ($data as $value) {
            $setting[$value->key_id] = $value->value;
        }
        $mail = [
            'driver' => $setting['mail_driver'],
            'host' => $setting['mail_smtp_host'],
            'port' => $setting['mail_smtp_port'],
            'from' => ['address' => 'info@bblash.com', 'name' => 'monir'],
            'encryption' => $setting['mail_smtp_encryption'],
            'username' => $setting['mail_smtp_username'],
            'password' => $setting['mail_smtp_password'],
            'sendmail' => $setting['mail_sendmail_path']];
        $services = [
            'mailgun.domain' => $setting['mail_mailgun_domain'],
            'mailgun.secret' => $setting['mail_mailgun_secret'],
            'ses.key' => $setting['mail_ses_key'],
            'ses.secret' => $setting['mail_ses_secret'],
            'mandrill.secret' => $setting['mail_mandrill_secret'],
            'sparkpost.secret' => $setting['mail_sparkpost_secret']
        ];
        $settings['mail'] = $mail;
        $settings['services'] = $services;
        foreach ($settings as $set => $row) {
            foreach ($row as $key => $value) {
                \Config::set($set . '.' . $key, $value);
            }
        }
        try {
            $send = \Mail::send($view, $data_us, function ($message) use ($subject, $emails) {
                        $message->to($emails)->subject($subject)->from('info@dwaa.com', 'monir');
                    });
            return [true, $send];
        } catch (\Swift_TransportException $ex) {
            return [false, $ex->getMessage()];
        } catch (SesException $ex) {
            return [false, $ex->getMessage()];
        } catch (ClientException $ex) {
            return [false, $ex->getMessage()];
        } catch (ServerException $ex) {
            return [false, $ex->getMessage()];
        }
    }

    /**
     * @param $type
     * @return array
     */
    public function getExt($type) {
        if ($type == 'image') {
            return ['gif', 'jpg', 'jpeg', 'png'];
        } elseif ($type == 'file') {
            return ['txt', 'doc', 'docx', 'xls', 'xlsx', 'pdf'];
        } else {
            return [];
        }
    }
    
}
