<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;
use App\Http\Controllers\Controller;
use App\Repositories\CitiesRepository;
use App\Repositories\CountriesRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Criteria\AdvancedSearchCriteria;

class CitiesController extends Controller {

    /**
     * QuestionsController constructor.
     */
    private $repo;
    private $repo_country;

    public function __construct(CitiesRepository $repo, CountriesRepository $repo_country) {
        parent::__construct();
        $this->repo = $repo;
        $this->repo_country = $repo_country;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $data = [];
        $data['page_breadcrumb'] = [
            ['title' => trans('cities.page_title')]
        ];
        $data['country_id'] = $request->input('country');
        $data['urls'] = [
            'get_data_url' => route('cities::get-table-data'),
            'add_region' => route('regions::index'),
            'delete_url' => route('cities::delete'),
            'delete_multi_url' => route('cities::delete-multi'),
            'update_status' => route('cities::update-status'),
            'save_url' => route('cities::create'),
            'update_url' => route('cities::update'),
        ];
        $data['permissions'] = [
            'delete' => \Auth::user()->can('delete.cities'),
            'edit' => \Auth::user()->can('edit.cities'),
            'show_regions' => \Auth::user()->can('show.regions'),
        ];
        return View('cities.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request) {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage) {
            return $perPage;
        });
        $where_obj = new \App\Repositories\Criteria\WhereObject();
        if ($request->input('country')) {
            $where_obj->pushWhere('country_id', $request->input('country'), 'eq');
        }
        $push = new \App\Repositories\Criteria\AdvancedSearchCriteria;
        $push::setWhereObject($where_obj);
        // push criteria to repository
        $this->repo->pushCriteria(new AdvancedSearchCriteria());
        $paginate = $this->repo->paginate($length, ['*']);
        //get country
        $c = $this->repo_country->find($request->input('country'));
        $country = $c->translate->where('locale', '=', \App::getLocale())->where('column_name', '=', 'title')->first();
        $items = $paginate->items();
        foreach ($items as $key => $value) {
            $items[$key]['title'] = $value->translate->where('locale', '=', \App::getLocale())->where('column_name', '=', 'title')->first();
            $items[$key]['country'] = $country->value;
            
        }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {
        try {
            $repose = $this->repo->delete([$request->input('id')]);
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request) {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateStatus(Request $request) {

        $response = $this->repo->update(['status' => $request->input('status')], $request->input('id'));
        if ($response) {
            return Response()->json([true, 'success_update_status', $response]);
        }
        return Response()->json([false, 'error_update_status', $response]);
    }

    /**
     * @param CityRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CityRequest $request) {
        $response = $this->repo->add($request);
        return Response()->json($response);
    }

    /**
     * @param CityRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CityRequest $request) {
        $response = $this->repo->updateRow($request);
        return Response()->json($response);
    }

}
