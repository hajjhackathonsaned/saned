<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CountryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CountriesRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Criteria\AdvancedSearchCriteria;

class CountriesController extends Controller {

    /**
     * QuestionsController constructor.
     */
    private $repo;

    public function __construct(CountriesRepository $repo) {
        parent::__construct();
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $data = [];
        $data['page_breadcrumb'] = [
            ['title' => trans('countries.page_title')]
        ];
        $data['urls'] = [
            'get_data_url' => route('countries::get-table-data'),
            'delete_url' => route('countries::delete'),
            'delete_multi_url' => route('countries::delete-multi'),
            'update_status' => route('countries::update-status'),
            'add_city' => route('cities::index'),
            'save_url' => route('countries::create'),
            'update_url' => route('countries::update'),
        ];
        $data['permissions'] = [
            'delete' => \Auth::user()->can('delete.countries'),
            'edit' => \Auth::user()->can('edit.countries'),
            'show_cities' => \Auth::user()->can('show.cities'),
        ];
        return View('countries.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request) {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage) {
            return $perPage;
        });
        $where_obj = new \App\Repositories\Criteria\WhereObject();
        $where_obj->pushOrder('id', 'desc');
        $push = new \App\Repositories\Criteria\AdvancedSearchCriteria;
        $push::setWhereObject($where_obj);
        $this->repo->pushCriteria(new AdvancedSearchCriteria());
        $paginate = $this->repo->paginate($length, ['*']);
        $items = $paginate->items();
        foreach ($items as $key => $value) {
            $items[$key]['title'] = $value->translate->where('locale', '=', \App::getLocale())->where('column_name', '=', 'title')->first();
        }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {
        try {
            $repose = $this->repo->delete([$request->input('id')]);
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request) {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateStatus(Request $request) {

        $response = $this->repo->update(['status' => $request->input('status')], $request->input('id'));
        if ($response) {
            return Response()->json([true, 'success_update_status', $response]);
        }
        return Response()->json([false, 'error_update_status', $response]);
    }

    /**
     * @param CountryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CountryRequest $request) {
        $response = $this->repo->add($request);
        return Response()->json($response);
    }

    /**
     * @param CountryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CountryRequest $request) {
        $response = $this->repo->updateRow($request);
        return Response()->json($response);
    }

}
