<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers\Admin
 */
class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(\Module::disabled());
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('dashboard.page_title')]
        ];
        return View('index', $data);
    }
    /**
     * @param $locale
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeLang($locale ,Request $request){
        $current=$request->segment(1);
        $rout=str_replace('/'.$current.'/','/'.$locale.'/',url()->previous());
        return redirect()->to($rout);
    }
}
