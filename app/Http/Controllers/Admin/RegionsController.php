<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\RegionRequest;
use App\Http\Controllers\Controller;
use App\Repositories\RegionsRepository;
use App\Repositories\CitiesRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Criteria\AdvancedSearchCriteria;

class RegionsController extends Controller {

    /**
     * QuestionsController constructor.
     */
    private $repo;

    public function __construct(RegionsRepository $repo, CitiesRepository $repo_city) {
        parent::__construct();
        $this->repo = $repo;
        $this->repo_city = $repo_city;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $data = [];
        $data['page_breadcrumb'] = [
            ['title' => trans('regions.page_title')]
        ];
        $data['city_id'] = $request->input('city');
        $data['urls'] = [
            'get_data_url' => route('regions::get-table-data'),
            'delete_url' => route('regions::delete'),
            'delete_multi_url' => route('regions::delete-multi'),
            'update_status' => route('regions::update-status'),
            'save_url' => route('regions::create'),
            'update_url' => route('regions::update'),
        ];
        $data['permissions'] = [
            'delete' => \Auth::user()->can('delete.regions'),
            'edit' => \Auth::user()->can('edit.regions'),
        ];
        return View('regions.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request) {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage) {
            return $perPage;
        });
        $where_obj = new \App\Repositories\Criteria\WhereObject();
        if ($request->input('city')) {
            $where_obj->pushWhere('city_id', $request->input('city'), 'eq');
        }
        $push = new \App\Repositories\Criteria\AdvancedSearchCriteria;
        $push::setWhereObject($where_obj);
        // push criteria to repository
        $this->repo->pushCriteria(new AdvancedSearchCriteria());
        $paginate = $this->repo->paginate($length, ['*']);
        $items = $paginate->items();
        //get city
        $c = $this->repo_city->find($request->input('city'));
        $city = $c->translate->where('locale', '=', \App::getLocale())->where('column_name', '=', 'title')->first();
        foreach ($items as $key => $value) {
            $items[$key]['title'] = $value->translate->where('locale', '=', \App::getLocale())->where('column_name', '=', 'title')->first();
            $items[$key]['city'] = $city->value;
        }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {
        try {
            $repose = $this->repo->delete([$request->input('id')]);
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request) {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateStatus(Request $request) {

        $response = $this->repo->update(['status' => $request->input('status')], $request->input('id'));
        if ($response) {
            return Response()->json([true, 'success_update_status', $response]);
        }
        return Response()->json([false, 'error_update_status', $response]);
    }

    /**
     * @param RegionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(RegionRequest $request) {
        $response = $this->repo->add($request);
        return Response()->json($response);
    }

    /**
     * @param RegionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RegionRequest $request) {
        $response = $this->repo->updateRow($request);
        return Response()->json($response);
    }

}
