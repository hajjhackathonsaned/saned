<?php

namespace App\Http\Controllers\Admin;

use App\Events\RolesEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RolesRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\Permission\Models\Permission;
use App\Repositories\Criteria\AdvancedSearchCriteria;
class RolesController extends Controller
{

    /**
     * QuestionsController constructor.
     */

    private $repo;
    public function __construct(RolesRepository $repo)
    {
        parent::__construct();
        $this->repo=$repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('roles.page_title')]
        ];
        $data['urls']=[
          'get_data_url'=>route('roles::get-table-data'),
          'delete_url'=>route('roles::delete'),
          'delete_multi_url'=>route('roles::delete-multi'),
          'save_url'=>route('roles::create'),
          'update_url'=>route('roles::update'),
        ];
        $permissions = Permission::all();
        $list = [];
        $format = function (&$list, $keys, $val) use(&$format) {
            $keys ? $format($list[array_shift($keys)], $keys, $val) : $list = $val;
        };
        if ($permissions) {
            foreach ($permissions as $key => $p) {
                $slug = explode('.', $p['name']);
                $permissions[$key]['group'] = $slug[1];
                $permissions[$key]['slug'] = $slug[0];
            }
            foreach ($permissions as $per) {
                $format($list, [$per['group'], $per['id']], $per['slug']);
            }
        }
        $data['permissions_role'] = [
            'delete' => \Auth::user()->can('delete.roles'),
            'edit' => \Auth::user()->can('edit.roles'),
        ];
        $data['permissions_list'] = $list;
        return View('roles.index', $data);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $search = $request->input('search');
        $order = $request->input('order');
        $columns = $request->input('columns');
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $where_obj = new \App\Repositories\Criteria\WhereObject();
        //check search
        if($search['value']){
            $where_obj->pushWhere('name', $search['value'], 'contain');
            $where_obj->pushOrWhere('created_at', $search['value'], 'contain');
        }
        $where_obj->pushOrder($columns[$order[0]['column']]['data'],$order[0]['dir']);
        $push = new \App\Repositories\Criteria\AdvancedSearchCriteria;
        $push::setWhereObject($where_obj);
        // push criteria to repository
        $this->repo->pushCriteria(new AdvancedSearchCriteria());
        $paginate = $this->repo->paginate($length,['*']);
        $items = $paginate->items();
        foreach ($items as $key => $value) {
            $items[$key]['permissions'] =($value->permissions)?$value->permissions->toArray():null;
        }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        try {
            //check if role is used
            $role=$this->repo->find($request->input('id'));
            if(!$role){
                return Response()->json([false, 'role_not_found']);
            }
            $users = \App\Models\User::role($role->name)->count();
            if($users > 0){
                return Response()->json([false, 'role_has_users']);
            }
            $repose = $this->repo->delete([$request->input('id')]);
            if ($repose) {
                event(new RolesEvent([],'delete'));
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){
        try {
            \DB::beginTransaction();
            $data=$request->except(['_token']);
            $response=$this->repo->create(['name'=>$data['name']]);
            if ($response) {
                //save all permissions
                $permissions = $request->input('permission');
                if (count($permissions) > 0) {
                    $sync=[];
                    foreach ($permissions as $id => $per) {
                        $sync[]=$id;
                    }
                    $response->syncPermissions($sync);
                }else{
                    \DB::rollback();
                    return Response()->json([false,'select_one_permission']);
                }
                \DB::commit();
                event(new RolesEvent([],'add'));
                return Response()->json([true,'success_add',$response]);
            }
            \DB::rollback();
            return Response()->json([false,'add_error']);
        } catch (\PDOException $e) {
            \DB::rollback();
            return Response()->json([false, $e->errorInfo[2]]);
        }catch (\Spatie\Permission\Exceptions\RoleAlreadyExists $e){
            \DB::rollback();
            return Response()->json([false, 'role_already_exists']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        try {
            \DB::beginTransaction();
            $data=$request->except(['_token']);
            $response=$this->repo->update(['name'=>$data['name']],$request->input('id'));
            $role=$this->repo->find($request->input('id'));
            if ($response) {
                //save all permissions
                $permissions = $request->input('permission');
                if (count($permissions) > 0) {
                    $sync=[];
                    foreach ($permissions as $id => $per) {
                        $sync[]=$id;
                    }
                    $role->syncPermissions($sync);
                }else{
                    \DB::rollback();
                    return Response()->json([false,'select_one_permission']);
                }
                \DB::commit();
                event(new RolesEvent([],'edit'));
                return Response()->json([true,'success_update',$response]);
            }
            \DB::rollback();
            return Response()->json([false,'update_error']);
        } catch (\PDOException $e) {
            \DB::rollback();
            return Response()->json([false, $e->errorInfo[2]]);
        }catch (\Spatie\Permission\Exceptions\RoleAlreadyExists $e){
            \DB::rollback();
            return Response()->json([false, 'role_already_exists']);
        }
    }
}
