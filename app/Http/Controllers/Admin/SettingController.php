<?php

namespace App\Http\Controllers\Admin;

use App\Events\SettingEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SettingRepository;
use App\Repositories\TemplateRepository;
use App\Helpers\Functions;

class SettingController extends Controller
{
    use Functions;

    /**
     * QuestionsController constructor.
     */

    private $repo;
    private $repo_temp;
    public function __construct(SettingRepository $repo,TemplateRepository $repo_temp)
    {
        parent::__construct();
        $this->repo=$repo;
        $this->repo_temp=$repo_temp;
    }

    /**
     * @param $group
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($group,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('setting.page_title')],
            ['title' => trans('setting.page_title_form_'.$group)],
        ];
        //get setting data
        $settings=$this->repo->findAllBy('set_group',$group);
        foreach ($settings as $v) {
            $data[$v->key_id] = $v->value;
        }
        $data['permissions'] = [
            'delete' => \Auth::user()->can('delete.setting'),
            'edit' => \Auth::user()->can('edit.setting'),
        ];
        if($group=='sms'){
            $data['templates']=$this->repo_temp->all(['*']);
        }
        $data['group']=$group;
        return View('setting.'.$group, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        foreach ($request->except('_token') as $k => $v) {
            $this->repo->updateSetting([
                'key_id' => $k,
                'value' => $v
            ], $k);
        }
        //check close site
        if($request->input('type')) {
            if (!$request->input('close_registration')) {
                $this->repo->updateSetting([
                    'key_id' => 'close_registration',
                    'value' => 0
                ], 'close_registration');
            }
            if (!$request->input('close_site')) {
                $this->repo->updateSetting([
                    'key_id' => 'close_site',
                    'value' => 0
                ], 'close_site');
            }
        }
        event(new SettingEvent([],'edit'));
        \Session::flash('message', trans('setting.updated-successfully'));
        \Session::flash('alert-class', 'alert-success');
        return redirect()->back();
    }
    public function updateTemplate(Request $request){
        foreach ($request->except('_token') as $k => $v) {
            $this->repo_temp->updateSetting([
                'key_id' => $k,
                'message' => $v
            ], $k);
        }
        event(new SettingEvent([],'edit'));
        \Session::flash('message', trans('setting.updated-successfully'));
        \Session::flash('alert-class', 'alert-success');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request){
        $gateway=$request->input('gateway');
        $username=$request->input('username');
        $password=$request->input('password');
        $sender=$request->input('sender');
        $message=$request->input('message');
        $number=$request->input('number');
        $r= new \App\Repositories\Sms\Gateway($gateway);
        $send = $r->gateway->setUser($username, $password)
            ->setNumbers([$number])
            ->setMessage($message)
            ->setSender($sender)
            ->send();
        return response()->json($send);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function balance(Request $request){
        $gateway=$request->input('gateway');
        $username=$request->input('username');
        $password=$request->input('password');
        $r= new \App\Repositories\Sms\Gateway($gateway);
        $send = $r->gateway->setUser($username, $password)
            ->getBalance();
        return response()->json($send);

    }
}
