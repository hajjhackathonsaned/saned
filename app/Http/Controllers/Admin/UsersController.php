<?php

namespace App\Http\Controllers\Admin;

use App\Events\UsersEvent;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Repositories\UsersRepository;
use App\Repositories\RolesRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Helpers\Functions;
use App\Repositories\Criteria\AdvancedSearchCriteria;
use Auth;

class UsersController extends Controller {

    use Functions;

    private $repo = null;
    private $repo_roles = null;

    public function __construct(UsersRepository $repo, RolesRepository $repo_roles) {
        parent::__construct();
        $this->repo = $repo;
        $this->repo_roles = $repo_roles;
    }

    public function index() {

        $data = [];
        $data['page_breadcrumb'] = [
            ['title' => trans('users.user_title')]
        ];
        $data['urls'] = [
            'get_data_url' => route('user::get-table-data'),
            'delete_url' => route('user::destroy'),
            // 'delete_multi_url'=>route('user::delete-multi'),
            'save_url' => route('user::create'),
            'update_url' => route('user::update'),
            'edit_url' => route('user::edit', ''),
        ];
        return View('users.users', $data);
    }

    public function getData(Request $request) {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $search = $request->input('search');
        $order = $request->input('order');
        $columns = $request->input('columns');

        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage) {
            return $perPage;
        });
        $where_obj = new \App\Repositories\Criteria\WhereObject();
        //check search
        if($search['value']){
            $where_obj->pushWhere('name', $search['value'], 'contain');
            $where_obj->pushOrWhere('email', $search['value'], 'contain');
        }
        $where_obj->pushOrder($columns[$order[0]['column']]['data'],$order[0]['dir']);
        $push = new \App\Repositories\Criteria\AdvancedSearchCriteria;
        $push::setWhereObject($where_obj);
        // push criteria to repository
        $this->repo->pushCriteria(new AdvancedSearchCriteria());
        $paginate = $this->repo->paginate($length, ['*']);
        $items = $paginate->items();
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    public function add() {

        $data = [];
        $roles = $this->repo_roles->all();
        $data['page_breadcrumb'] = [
            ['title' => trans('users.page_title')],
        ];

        return View('users.add', compact('roles', 'data'));
    }

    public function create(UserRequest $request) {

        $add = $this->repo->add($request);

        if ($add) {
            event(new UsersEvent([], 'add'));
            Session::flash('message', trans('users.success_add_user'));
            return redirect()->route('user::index');
        } else {
            Session::flash('message', trans('users.error_add_user'));
            return redirect()->route('user::add');
        }
    }

    public function edit($id) {

        $page_breadcrumb = [
            ['title' => trans('users.page_edit')],
        ];

        $user = $this->repo->find($id);
        $user_roles = $user->roles;
        $roles = $this->repo_roles->all();

        return view('users.add', compact('page_breadcrumb', 'user', 'user_roles', 'roles'));
    }

    public function update(UserRequest $request) {

        $update = $this->repo->updateUser($request);
        if ($update) {
            event(new UsersEvent([], 'edit'));
            Session::flash('message', trans('users.success_edit_user'));
            return redirect()->route('user::index');
        }
    }

    public function destroy(Request $request) {

        $user_id = Auth::user()->id;
        if ($user_id != $request->input('id')) {
            try {
                $repose = $this->repo->delete([$request->input('id')]);
                if ($repose) {
                    event(new UsersEvent([], 'delete'));
                    return Response()->json([true, 'deleted_success', $repose]);
                }
                return Response()->json([false, 'error_delete_row', $repose]);
            } catch (\PDOException $e) {
                return Response()->json([false, $e->errorInfo[2]]);
            }
        } else {
            return Response()->json([false, 'deleted_yourself']);
        }
    }

}
