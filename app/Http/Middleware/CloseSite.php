<?php

namespace App\Http\Middleware;

use Closure;

class CloseSite {

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function handle($request, Closure $next) {

        if (config('general.close_site') == 0) {
            return $next($request);
        } else {
            return response()->view("site_closed");
        }
    }

}
