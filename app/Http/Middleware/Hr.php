<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Hr {

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    
    public function handle($request, Closure $next, $guard = 'hr')
    {
        if (!Auth::guard($guard)->check()) {
            return redirect('/hr');
        }

        return $next($request);
    }


}
