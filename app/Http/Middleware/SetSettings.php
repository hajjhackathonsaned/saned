<?php

namespace App\Http\Middleware;

use Closure;

class SetSettings {

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function handle($request, Closure $next) {
        $repo = new \App\Repositories\SettingRepository(app(), \Illuminate\Support\Collection::make());
        $data = $repo->getWhereIn(['general', 'social', 'sms']);
        foreach ($data as $value) {
            \Config::set($value->set_group . '.' . $value->key_id, $value->value);
        }
        return $next($request);
    }

}
