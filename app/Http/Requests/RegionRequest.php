<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegionRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'title.*' => 'required',
            'city' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'title.ar' => trans('specialist.title'),
            'title.en' => trans('specialist.title').' English',
        ];
    }

}
