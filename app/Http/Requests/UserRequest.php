<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) {
        $rules = [];
        $rules['name'] = 'required|max:255';
        $rules['email'] = 'required|email|unique:users,email,' . $request->input('id');
        $rules['mobile'] = 'required|numeric|unique:users,mobile,' . $request->input('id');
        $rules['image'] = "mimes:jpeg,bmp,png";
        $rules['role'] = 'required';
        if (!$request->input('id')) {
            $rules['password'] = 'confirmed|min:8';
        }
        return $rules;
    }

}
