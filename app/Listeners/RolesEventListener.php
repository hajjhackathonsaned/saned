<?php

namespace App\Listeners;

use App\Events\RolesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RolesEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param RolesEvent $event
     */
    public function handle(RolesEvent $event)
    {

    }
}
