<?php

namespace App\Listeners;

use App\Events\SettingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SettingEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param SettingEvent $event
     */
    public function handle(SettingEvent $event)
    {

    }
}
