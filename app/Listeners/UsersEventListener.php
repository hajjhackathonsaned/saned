<?php

namespace App\Listeners;

use App\Events\UsersEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UsersEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param SpecialistEvent $event
     */
    public function handle(UsersEvent $event)
    {

    }
}
