<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cities extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'cities';

    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }  
    public function country()
    {
        return $this->belongsTo('\App\Models\Countries');
    } 
}
