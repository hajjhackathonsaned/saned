<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'countries';

    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }    
}
