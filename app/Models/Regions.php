<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Regions extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'regions';

    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }  
    public function city()
    {
        return $this->belongsTo('\App\Models\Cities');
    } 
}
