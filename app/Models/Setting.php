<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $primaryKey = 'key_id';
    public $incrementing = false;
    protected $fillable = ['key_id', 'value','set_group'];
    protected $dates = ['created_at', 'updated_at'];
    protected $table = 'setting';
}
