<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsLog extends Model
{


    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'sms_log';
}
