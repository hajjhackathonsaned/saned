<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $primaryKey = 'key_id';
    public $incrementing = false;
    protected $fillable = ['key_id', 'message','vars','type'];
    protected $dates = ['created_at', 'updated_at'];
    protected $table = 'messages_template';
}
