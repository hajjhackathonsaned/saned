<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{


    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'translation';

    public function translatable()
    {
        return $this->morphTo();
    }
}
