<?php

namespace App\Modules\Blog\Http\Controllers;

use App\Modules\Blog\Http\Requests\BlogRequest;
use App\Modules\Blog\Repositories\BlogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class BlogController extends Controller
{
    private $repo;
    public function __construct(BlogRepository $repo)
    {
        parent::__construct();
        $this->repo=$repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('blog::blog.page_title')]
        ];
        $data['urls']=[
            'get_data_url'=>route('blog::get-table-data'),
            'delete_url'=>route('blog::delete'),
            'delete_multi_url'=>route('blog::delete-multi'),
            'update_url'=>route('blog::update-status'),
            'edit_url'=>route('blog::edit',['id'=>'']),
        ];
        return View('blog::admin.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('blog::blog.page_title')],
            ['title' => trans('blog::blog.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('blog::create'),
        ];
        return View('blog::admin.form', $data);
    }

    /**
     * @param BlogRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(BlogRequest $request){
        $add=$this->repo->add($request);
        if($add[0]){
            \Session::flash('message', trans('blog::blog.'.$add[1]));
            \Session::flash('alert-class', 'alert-success');
            return redirect()->route('blog::index');
        }else{
            return redirect()->back()->withErrors([trans('blog::blog.'.$add[1])]);
        }

    }

    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $paginate = $this->repo->paginate($length,['*']);
        $items = $paginate->items();
        foreach ($items as $key => $value) {
            $items[$key]['title'] =$value->translate->where('locale','=',\App::getLocale())->where('column_name','=','title')->first();
        }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    public function delete(Request $request)
    {
        try {
            $repose = $this->repo->delete($request->input('id'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateStatus(Request $request)
    {

        $response = $this->repo->update(['status'=>$request->input('status')],$request->input('id'));
        if ($response) {
            return [true, 'success_update_status', $response];
        }
        return [false, 'error_update_status', $response];
    }
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('blog::blog.page_title')],
            ['title' => trans('blog::blog.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('blog::update'),
        ];
        $details=$this->repo->find($id);
        $data['id']=$details->id;
        foreach (config('app.supported_lang') as $lang){
            $data['title'][$lang]=$details->translate->where('locale','=',$lang)->where('column_name','=','title')->first()->value;
            $data['description'][$lang]=$details->translate->where('locale','=',$lang)->where('column_name','=','description')->first()->value;
        }
        return View('blog::admin.form', $data);
    }

    public function update(BlogRequest $request){
        $add=$this->repo->edit($request);
        if($add[0]){
            \Session::flash('message', trans('blog::blog.'.$add[1]));
            \Session::flash('alert-class', 'alert-info');
            return redirect()->route('blog::index');
        }else{
            return redirect()->back()->withErrors([trans('blog::blog.'.$add[1])]);
        }

    }
}
