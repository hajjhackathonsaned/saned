<?php

return [
    'page_title' => 'المقالات',
    'content_title' => 'المقالات',
    'table_title' => 'عرض جميع المقالات',
    'title' => 'العنوان',
    'description' => 'التفاصيل',
    'image' => 'الصورة',
    'page_title_form' => 'اضافة مقال جديد',
    'status' => 'الحالة',
    'question_add_error'=> 'حدث خطأ أثناء اضافة المقال',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه للمقال',
    'success_add'=> 'تم اضافة المقال بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات المقال بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل المقال',
    'correct'=> 'الاجابة الصحيحة',
    'js_lang' =>[
        'success_update_status'=>'تم تعديل حالة المقال بنجاح',
        'error_update_status'=>'حدث خطأ أثناء تعديل حالة المقال',
        'update_status_confirm'=>'هل انت متأكد من تعديل حالة المقال ؟',
    ],
];
