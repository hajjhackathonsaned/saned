<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

$this->group(['prefix' => 'blog', 'middleware' => ['auth'], 'as' => 'blog::'], function () {

    $this->get('/', ['as' => 'index', 'uses' => 'BlogController@index']);
    $this->get('/add', ['as' => 'add', 'uses' => 'BlogController@add']);
    $this->get('/edit/{id}', ['as' => 'edit', 'uses' => 'BlogController@edit']);
    $this->post('/getTableData/', ['as' => 'get-table-data', 'uses' => 'BlogController@getData']);
    $this->post('create', ['as' => 'create', 'uses' => 'BlogController@create']);
    $this->post('/delete', ['as' => 'delete', 'uses' => 'BlogController@delete']);
    $this->post('/deleteMulti', ['as' => 'delete-multi', 'uses' => 'BlogController@deleteAll']);
    $this->post('/update', ['as' => 'update', 'uses' => 'BlogController@update']);
    $this->post('/updateStatus', ['as' => 'update-status', 'uses' => 'BlogController@updateStatus']);

});
