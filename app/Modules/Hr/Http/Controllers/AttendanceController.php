<?php

namespace App\Modules\Hr\Http\Controllers;
use Excel;
use App\Modules\Hr\Http\Requests\AttendanceRequest;
use App\Modules\Hr\Repositories\AttendanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class AttendanceController extends HrController
{
    private $repo;
    public function __construct(\Maatwebsite\Excel\Excel $excel ,AttendanceRepository $repo )
    {
        parent::__construct();
        $this->repo=$repo;
        $this->excel = $excel;
    }
    public function uploadFile(){
    	$data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Attendance.page_title')],
            ['title' => trans('hr::Attendance.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Attendance-Create'),
        ];
        
        return View('hr::Attendance.file',$data);
    }
    public function create(AttendanceRequest $request){
    	
		$file = $request->file('file');

		$file = $request->file;
		//dd($file);
		//---------------------
		$monthFrom=date_create($request->input('month_from'));
		$monthTo=date_create($request->input('month_to'));
		$seetingId=\DB::table('attendance_settings')->insertGetId([
                'day_from' => $request->input('day_from'),
                'day_to' => $request->input('day_to'),
                'month_from' =>  date_format($monthFrom,"Y-m-d"),
                'month_to' => date_format($monthTo,"Y-m-d"),
                'title' => $request->input('title'),
                'delay_time' => $request->input('delay_time')
                ]);
		if($request->input('break_from')){
			foreach ($request->input('break_from') as $key => $value) {
				\DB::table('attendance_breaks')->insertGetId([
                'time_from' => $value,
                'time_to' => $request->input('break_to')[$key],
                'attendance_setting_id' => $seetingId
                ]);
			}
		}
		//---------------------
		$path = $request->file->getRealPath();
		$attendanceData=Excel::load($path)->get();
		$records = [];

		$attendanceData->each(function ($item, $key) use (&$records ,$seetingId) {
		    $record = [
		        'user_id' => str_replace('E', '',$item['employee_no.']),
		        'attendance_date' => $item['date'],
		        'verify_mode' => $item['verify_mode'],
		        'mode' => $item['io_mode'],
		        'attendance_setting_id' => $seetingId
		    ];
		    $records[] = $record;
		});
		\DB::table('attendance')->insert($records);
        exit;
		


    }
}
?>