<?php

namespace App\Modules\Hr\Http\Controllers;

use App\Modules\Hr\Models\DepartmentModel;
use App\Modules\Hr\Http\Requests\EmployeeRequest;
use App\Modules\Hr\Repositories\EmployeeRepository;
use App\Modules\Hr\Repositories\DepartmentRepository;
use App\Modules\Hr\Repositories\JobTitleRepository;
use App\Modules\Hr\Repositories\HrRoleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class EmployeeController extends HrController
{
    private $repo;
    private $deptRepo;
    private $jobTitleRepo;
    private $roleRepo;
    public function __construct(EmployeeRepository $repo ,DepartmentRepository $deptRepo,
                                JobTitleRepository $jobTitleRepo ,HrRoleRepository $roleRepo)
    {
        parent::__construct();
        $this->repo=$repo;
        $this->deptRepo=$deptRepo;
        $this->jobTitleRepo=$jobTitleRepo;
        $this->roleRepo=$roleRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::employees.page_title')]
        ];
        $data['urls']=[
            'get_data_url'=>route('hr::Employees-get-table-data'),
            'delete_url'=>route('hr::Employees-Delete'),
            'delete_multi_url'=>route('hr::Employees-DeleteMulti'),
            'edit_url'=>route('hr::Employees-Edit',['id'=>'']),
        ];
        return View('hr::employees.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::employees.page_title')],
            ['title' => trans('hr::employees.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Employees-Create'),
            //'save_url'=>route('hr::JobTitle/create'),
        ];
        $allDepartments = $this->deptRepo->all(['*']);
        $departments=array();
        foreach ($allDepartments as $key => $value) {
            $departments[$value->id]['name']=$value->name;
        }
        $alljobTitles = $this->jobTitleRepo->all(['*']);
        $jobTitles=array();
        foreach ($alljobTitles as $key => $value) {
            $jobTitles[$value->id]['title']=$value->title;
        }
        $allroles = $this->roleRepo->all(['*']);
        $roles=array();
        foreach ($allroles as $key => $value) {
            $roles[$value->id]['name']=$value->name;
        }
        $data['roles']=$roles;
        $data['departments']=$departments;
        $data['jobTitles']=$jobTitles;
        
        return View('hr::employees.form',$data);
    }

    /**
     * @param JobTitleRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(EmployeeRequest $request){
        $add=$this->repo->add($request);
        if($add[0]){
            \Session::flash('message', trans('hr::employees.'.$add[1]));
            \Session::flash('alert-class', 'alert-success');
            return redirect()->route('hr::Employees-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::employees.'.$add[1])]);
        }

    }

    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $paginate = $this->repo->paginate($length,['id','first_name','last_name','created_at']);

        $items = $paginate->items();
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    public function delete(Request $request)
    {
        try {
            $repose = $this->repo->delete($request->input('id'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::employees.page_title')],
            ['title' => trans('hr::employees.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Employees-Update'),
        ];
        $details=$this->repo->find($id);
        $data['id']=$details->id;
        $data['department_id']=$details->department_id;
        $data['job_title_id']=$details->job_title_id;
        $data['email']=$details->email;
        $data['code']=$details->code;
        $data['first_name']=$details->first_name;
        $data['last_name']=$details->last_name;
        $data['sex']=$details->sex;
        $data['role_id']=$details->role_id;

        //$this->debug($data,1);
        $allDepartments = $this->deptRepo->all(['*']);
        $departments=array();
        foreach ($allDepartments as $key => $value) {
            $departments[$value->id]['name']=$value->name;
        }
        $alljobTitles = $this->jobTitleRepo->all(['*'])->where('department_id',$data['department_id']);
        $jobTitles=array();
        foreach ($alljobTitles as $key => $value) {
            $jobTitles[$value->id]['title']=$value->title;
        }

        $allroles = $this->roleRepo->all(['*']);
        $roles=array();
        foreach ($allroles as $key => $value) {
            $roles[$value->id]['name']=$value->name;
        }
        $data['roles']=$roles;
        $data['departments']=$departments;
        $data['jobTitles']=$jobTitles;
        return View('hr::employees.form', $data);
    }

    public function update(EmployeeRequest $request){
        $add=$this->repo->edit($request);
        if($add[0]){
            \Session::flash('message', trans('hr::employees.'.$add[1]));
            \Session::flash('alert-class', 'alert-info');
            return redirect()->route('hr::Employees-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::department_id.'.$add[1])]);
        }

    }
    public function changeJobTitle(EmployeeRequest $request){
        $departmentId=$request->input('departmentId');
        $alljobTitles = $this->jobTitleRepo->all(['*'])->where('department_id',$departmentId);
        $jobTitles=array();
        foreach ($alljobTitles as $key => $value) {
            $jobTitles[$value->id]['title']=$value->title;
        }
        $data['jobTitles']=$jobTitles;
        echo View('hr::employees.ajax.JobTitle', $data);
    }
}
