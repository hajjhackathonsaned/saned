<?php

namespace App\Modules\Hr\Http\Controllers;
use View;
use App\Modules\Hr\Http\Requests\HrRequest;
use App\Modules\Hr\Repositories\HrRepository;
use App\Modules\Hr\Models\RequestModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class HrController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $requestLists=RequestModel::select('requests.id','requests.name')->get()->toArray();
        View::share('requestLists', $requestLists);
        
    }
    public function getRequestsList(){
        return ;

    }
    public function debug($value,$exit=0){
    	echo '<pre style="background:#FFFF00,border:1px soild #000;">';
    	var_dump($value);
    	echo '</pre>';
    	if($exit=1){
    		exit;
    	}

    }

    
}
