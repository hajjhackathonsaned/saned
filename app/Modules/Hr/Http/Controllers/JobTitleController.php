<?php

namespace App\Modules\Hr\Http\Controllers;

use App\Modules\Hr\Models\DepartmentModel;
use App\Modules\Hr\Http\Requests\JobTitleRequest;
use App\Modules\Hr\Repositories\JobTitleRepository;
use App\Modules\Hr\Repositories\DepartmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class JobTitleController extends HrController
{
    private $repo;
    public function __construct(JobTitleRepository $repo,DepartmentRepository $deprtRepo)
    {
        parent::__construct();
        $this->repo=$repo;
        $this->deprtRepo=$deprtRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::JobTitle.page_title')]
        ];
        $data['urls']=[
            'get_data_url'=>route('hr::JobTitle-get-table-data'),
            'delete_url'=>route('hr::JobTitle/delete'),
            'delete_multi_url'=>route('hr::JobTitle/delete-multi'),
            'edit_url'=>route('hr::JobTitle/edit',['id'=>'']),
        ];
        return View('hr::JobTitle.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::JobTitle.page_title')],
            ['title' => trans('hr::JobTitle.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::JobTitle/create'),
        ];
        $allDepartments = $this->deprtRepo->all(['id','name']);
        
        $departments=array();
        foreach ($allDepartments as $key => $value) {
            $departments[$value->id]['name']=$value->name;
        }
        //$this->debug($departments,1);
        $data['departments']=$departments;
        
        return View('hr::JobTitle.form',$data);
    }

    /**
     * @param JobTitleRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(JobTitleRequest $request){
        $add=$this->repo->add($request);
        if($add[0]){
            \Session::flash('message', trans('hr::JobTitle.'.$add[1]));
            \Session::flash('alert-class', 'alert-success');
            return redirect()->route('hr::JobTitle/index');
        }else{
            return redirect()->back()->withErrors([trans('hr::JobTitle.'.$add[1])]);
        }

    }

    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $paginate = $this->repo->paginate($length,['id','title','created_at']);

        $items = $paginate->items();
        foreach ($items as $key => $value) {
            $items[$key]['title'] =$value->translate->where('locale','=',\App::getLocale())->where('column_name','=','title')->first();
            //$items[$key]['department'] =$value->deprtemnt->toArray();

        }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    public function delete(Request $request)
    {
        try {
            $repose = $this->repo->delete($request->input('id'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::JobTitle.page_title')],
            ['title' => trans('hr::JobTitle.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::JobTitle/update'),
        ];
        $details=$this->repo->find($id);
        $data['id']=$details->id;
        $data['department_id']=$details->department_id;
        foreach (config('app.supported_lang') as $lang){
            $data['title'][$lang]=$details->translate->where('locale','=',$lang)->where('column_name','=','title')->first()->value;
        }
        $allDepartments = $this->deprtRepo->all(['*']);
        $departments=array();
        foreach ($allDepartments as $key => $value) {
            $departments[$value->id]['name']=$value->name;
        }
        
        $data['departments']=$departments;

        return View('hr::JobTitle.form', $data);
    }

    public function update(JobTitleRequest $request){
        $add=$this->repo->edit($request);
        if($add[0]){
            \Session::flash('message', trans('hr::JobTitle.'.$add[1]));
            \Session::flash('alert-class', 'alert-info');
            return redirect()->route('hr::JobTitle/index');
        }else{
            return redirect()->back()->withErrors([trans('hr::JobTitle.'.$add[1])]);
        }

    }
}
