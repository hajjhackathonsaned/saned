<?php

namespace App\Modules\Hr\Http\Controllers;

use App\Modules\Hr\Models\PerformanceIndicatorModel;
use App\Modules\Hr\Http\Requests\PerformanceIndicatorRequest;
use App\Modules\Hr\Repositories\PerformanceIndicatorRepository;
use App\Modules\Hr\Repositories\HrRoleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class PerformanceIndicatorController extends HrController
{
    private $repo;
    private $roleRepo;
    public function __construct(PerformanceIndicatorRepository $repo ,HrRoleRepository $roleRepo)
    {
        parent::__construct();
        $this->repo=$repo;
        $this->roleRepo=$roleRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::PerformanceIndicator.page_title')]
        ];
        $data['urls']=[
            'get_data_url'=>route('hr::PerformanceIndicator-get-table-data'),
            'delete_url'=>route('hr::PerformanceIndicator-Delete'),
            'delete_multi_url'=>route('hr::PerformanceIndicator-DeleteMulti'),
            'edit_url'=>route('hr::PerformanceIndicator-Edit',['id'=>'']),
        ];
        return View('hr::PerformanceIndicator.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::PerformanceIndicator.page_title')],
            ['title' => trans('hr::PerformanceIndicator.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::PerformanceIndicator-Create'),
        ];
        $allroles = $this->roleRepo->all(['*']);
        $roles=array();
        foreach ($allroles as $key => $value) {
            $roles[$value->id]['name']=$value->name;
        }
        $data['roles']=$roles;
        return View('hr::PerformanceIndicator.form',$data);
    }

    /**
     * @param JobTitleRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(PerformanceIndicatorRequest $request){
        $add=$this->repo->add($request);
        if($add[0]){
            \Session::flash('message', trans('hr::PerformanceIndicator.'.$add[1]));
            \Session::flash('alert-class', 'alert-success');
            return redirect()->route('hr::PerformanceIndicator-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::PerformanceIndicator.'.$add[1])]);
        }

    }

    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $paginate = $this->repo->paginate($length,['id','name','created_at']);

        $items = $paginate->items();
        foreach ($items as $key => $value) {
            $items[$key]['name'] =$value->translate->where('locale','=',\App::getLocale())->where('column_name','=','name')->first();

        }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    public function delete(Request $request)
    {
        try {
            $repose = $this->repo->delete($request->input('id'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::PerformanceIndicator.page_title')],
            ['title' => trans('hr::PerformanceIndicator.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::PerformanceIndicator-Update'),
        ];
        $details=$this->repo->find($id);
        $data['id']=$details->id;
        $data['role_id']=$details->role_id;
        foreach (config('app.supported_lang') as $lang){
            $data['name'][$lang]=$details->translate->where('locale','=',$lang)->where('column_name','=','name')->first()->value;
        }
        $allroles = $this->roleRepo->all(['*']);
        $roles=array();
        foreach ($allroles as $key => $value) {
            $roles[$value->id]['name']=$value->name;
        }
        $data['roles']=$roles;
    
        return View('hr::PerformanceIndicator.form', $data);
    }

    public function update(PerformanceIndicatorRequest $request){
        $add=$this->repo->edit($request);
        if($add[0]){
            \Session::flash('message', trans('hr::PerformanceIndicator.'.$add[1]));
            \Session::flash('alert-class', 'alert-info');
            return redirect()->route('hr::PerformanceIndicator-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::PerformanceIndicator.'.$add[1])]);
        }

    }
}
