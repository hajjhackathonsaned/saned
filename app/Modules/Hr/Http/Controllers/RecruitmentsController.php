<?php

namespace App\Modules\Hr\Http\Controllers;

use App\Modules\Hr\Http\Requests\RecruitmentsRequest;
use App\Modules\Hr\Repositories\RecruitmentsRepository;
use App\Modules\Hr\Repositories\JobTitleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class RecruitmentsController extends HrController
{
    private $repo;
    private $jobTitleRepo;
    public function __construct(RecruitmentsRepository $repo ,JobTitleRepository $jobTitleRepo)
    {
        parent::__construct();
        $this->repo=$repo;
        $this->jobTitleRepo=$jobTitleRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Recruitments.page_title')]
        ];
        $data['urls']=[
            'get_data_url'=>route('hr::Recruitments-get-table-data'),
            'delete_url'=>route('hr::Recruitments/delete'),
            'delete_multi_url'=>route('hr::Recruitments/delete-multi'),
            'edit_url'=>route('hr::Recruitments/edit',['id'=>'']),
        ];
        return View('hr::Recruitments.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Recruitments.page_title')],
            ['title' => trans('hr::Recruitments.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Recruitments-Create'),
        ];
        $alljobTitles = $this->jobTitleRepo->all(['*']);
        $jobTitles=array();
        foreach ($alljobTitles as $key => $value) {
            $jobTitles[$value->id]=$value->title;
        }
        $languages=\DB::table('languages')
                ->select(['id','name'])
                ->get()
                ->toArray();

        // $this->debug($languages,1);
        $data['jobTitles']=$jobTitles;
        $data['languages']=$languages;
        
        return View('hr::Recruitments.form',$data);
    }

    /**
     * @param JobTitleRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(RecruitmentsRequest $request){
        $add=$this->repo->add($request);
        if($add[0]){
            \Session::flash('message', trans('hr::Recruitments.'.$add[1]));
            \Session::flash('alert-class', 'alert-success');
            return redirect()->route('hr::Recruitments-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::Recruitments.'.$add[1])]);
        }

    }

    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $paginate = $this->repo->paginate($length,['id','title','created_at']);

        $items = $paginate->items();
     
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    public function delete(Request $request)
    {
        try {
            $repose = $this->repo->delete($request->input('id'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Recruitments.page_title')],
            ['title' => trans('hr::Recruitments.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Recruitments-Update'),
        ];
        $details=$this->repo->find($id);
        $data['id']=$details->id;
        $data['title']=$details->title;
        $data['salary']=$details->salary;
        $data['experiences_years']=$details->experiences_years;
        $data['age']=$details->age;
        $data['job_title_id']=$details->job_title_id;
        $data['no_employees']=$details->no_employees;
        $data['type']=$details->type;
        $data['comment_id']=$details->comment_id;
        $data['type']=$details->type;
        $data['job_title_id']=$details->job_title_id;
        $comment=$details->comment();
        $data['comment']=$comment['comment'];
        $alljobTitles = $this->jobTitleRepo->all(['*']);
        $jobTitles=array();
        foreach ($alljobTitles as $key => $value) {
            $jobTitles[$value->id]=$value->title;
        }
        $languages=\DB::table('languages')
                ->select(['id','name'])
                ->get()
                ->toArray();
        $recLang=\DB::table('recruitment_languages')
                ->where('recruitment_id',$id)
                ->select(['language_id'])
                ->get()
                ->toArray();
        $savedLang=array();
        foreach ($recLang as $key => $value) {
            $savedLang[$value->language_id]=$value->language_id;
        }
        $data['savedLang']=$savedLang;
        $data['jobTitles']=$jobTitles;
        $data['languages']=$languages;
        return View('hr::Recruitments.form', $data);
    }

    public function update(RecruitmentsRequest $request){
        $add=$this->repo->edit($request);
        if($add[0]){
            \Session::flash('message', trans('hr::Recruitments.'.$add[1]));
            \Session::flash('alert-class', 'alert-info');
            return redirect()->route('hr::Recruitments-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::Recruitments.'.$add[1])]);
        }

    }
}
