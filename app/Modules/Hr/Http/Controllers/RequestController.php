<?php

namespace App\Modules\Hr\Http\Controllers;

use App\Modules\Hr\Models\RequestModel;
use App\Modules\Hr\Models\InputOptionModel;
use App\Modules\Hr\Models\UsersInputModel;
use App\Modules\Hr\Models\UserRequestModel;

use App\Modules\Hr\Http\Requests\RequestRequest;
use App\Modules\Hr\Repositories\OtherRequestRepository;
use App\Modules\Hr\Repositories\UserInputRepository;
use App\Modules\Hr\Repositories\UserRequestRepository;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class RequestController extends HrController
{
    private $repo;
    private $userInput;
    private $userRequest;
    public function __construct(OtherRequestRepository $repo, UserInputRepository $userInput,
                                UserRequestRepository $userRequest)
    {
        parent::__construct();
        $this->repo=$repo;
        $this->userInput=$userInput;
        $this->userRequest=$userRequest;
    }


    public function build(){
    	$data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Request.page_title')],
            ['title' => trans('hr::Request.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Request-Create'),
        ];

        return View('hr::Request.form',$data);
    }
    public function create(RequestRequest $request){
    	// $this->debug($request->input('formBuliderJson'),1);
        $add=$this->repo->add($request);
        return redirect()->route('hr::Request-List');
        // if($add[0]){
        //     \Session::flash('message', trans('hr::Request.'.$add[1]));
        //     \Session::flash('alert-class', 'alert-success');
        //     return redirect()->route('hr::Request-List');
        // }else{
        //     return redirect()->back()->withErrors([trans('hr::Request.'.$add[1])]);
        // }

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Request.page_title')]
        ];
        $data['urls']=[
            'get_data_url'=>route('hr::Request-get-table-data'),
            'delete_url'=>route('hr::Request-Delete'),
            'delete_multi_url'=>route('hr::Request-DeleteMulti'),
            'edit_url'=>route('hr::Request-Edit',['id'=>'']),
        ];
        return View('hr::Request.index', $data);
    }
    public function formIndex($id)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Request.page_title')]
        ];
        $data['request_id']=$id;
        $data['urls']=[
            'get_data_url'=>route('hr::Request-get-form-table-data',['id'=>$id]),
            'delete_url'=>route('hr::Request-DeleteForm'),
            'delete_multi_url'=>route('hr::Request-formDeleteMulti'),
            'edit_url'=>route('hr::Request-EditForm',['id'=>'']),
        ];
        return View('hr::Request.formIndex', $data);
    }
    public function getFormTableData(Request $request)
    {        
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });

        $paginate = UserRequestModel::select('user_requests.*')
            ->where([
                ['user_requests.request_id', '=', $request->input("id")],
                ['user_requests.user_id', '=', 1]
                ])
            ->paginate($length);

        $items = $paginate->items();
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }
    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $paginate = $this->repo->paginate($length,['id','name','created_at']);

        $items = $paginate->items();
        // foreach ($items as $key => $value) {
        //     $items[$key]['name'] =$value->translate->where('locale','=',\App::getLocale())->where('column_name','=','name')->first();

        // }
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */ 
    public function EditForm($id,Request $request){
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Request.page_title')],
            ['title' => trans('hr::Request.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Request-updateForm'),
        ];
        $details=$this->userRequest->find($id);
        $formId=$details->request_id;
        $data['request_id']=$formId;
        $data['request']=$request;
        $data['id']=$id;
        $inputs = RequestModel::select(
            'requests.name','inputs.type','inputs.id','inputs.label',
            'inputs.required','inputs.name as inputName','users_inputs.value')
            ->join('inputs', 'inputs.request_id', '=', 'requests.id')
            ->leftJoin('users_inputs', 'users_inputs.input_id', '=', 'inputs.id')
            ->where('requests.id', $formId)
            ->where('users_inputs.user_request_id',$id)
            ->get()->toArray();
            //$this->debug($inputs,1);
        $inputsArray=array();
        foreach ($inputs as $key => $input) {
            $inputsArray[$key]['id']=$input['id'];
            $inputsArray[$key]['type']=$input['type'];
            $inputsArray[$key]['label']=$input['label'];
            $inputsArray[$key]['required']=$input['required'];
            $inputsArray[$key]['name']=$input['inputName'];
            $inputsArray[$key]['value']=$input['value'];
            $inputsArray[$key]['options']=InputOptionModel::select('input_options.*')->where('input_options.input_id',$input['id'])->get();
        }
        $data['inputs']=$inputsArray;
        return View('hr::Request.showForm',$data);

    }
    public function update(RequestRequest $request){
        $add=$this->repo->edit($request);
        return redirect()->route('hr::Request-List');
    }
    public function edit($id,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Request.page_title')],
            ['title' => trans('hr::Request.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Request-Update'),
        ];
        $details=$this->repo->find($id);
        $data['id']=$details->id;
        $data['json']=json_encode(unserialize($details->json));
        $data['name']=$details->name;
        
    
        return View('hr::Request.form', $data);
    }

    public function delete(Request $request)
    {
        try {
            $repose = $this->repo->delete($request->input('id'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }
    public function addForm($id){

    	$details=$this->repo->find($id);
    	$data=[];
        $data['page_breadcrumb'] = [
            ['title' => $details->name]
        ];
        $data['urls']=[
            'save_url'=>route('hr::Request-addForm'),
        ];
    	return View('hr::Request.addForm',$data);
    }
    public function updateForm(RequestRequest $request){
        $add=$this->userInput->edit($request);
        return redirect()->route('hr::Request-ListForm',['id'=>$request->input('request_id')]);
        
    }
    public function saveForm(RequestRequest $request){
        $addRewquest=$this->userRequest->add($request->input('request_id'));
        $add=$this->userInput->add($request,$addRewquest->id);
        return redirect()->route('hr::Request-ListForm',['id'=>$request->input('request_id')]);

    }
    public function forms($formId){
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::Request.page_title')],
            ['title' => trans('hr::Request.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::Request-saveForm'),
        ];
        $request=$this->repo->find($formId);
        $data['request_id']=$formId;
        $data['request']=$request;
        $inputs = RequestModel::select(
            'requests.name','inputs.type','inputs.id','inputs.label',
            'inputs.required','inputs.name as inputName')
            ->join('inputs', 'inputs.request_id', '=', 'requests.id')
            ->where('requests.id', $formId)
            ->get();
        $inputsArray=array();
        foreach ($inputs as $key => $input) {
            $inputsArray[$key]['id']=$input->id;
            $inputsArray[$key]['type']=$input->type;
            $inputsArray[$key]['label']=$input->label;
            $inputsArray[$key]['required']=$input->required;
            $inputsArray[$key]['name']=$input->inputName;
            $inputsArray[$key]['options']=InputOptionModel::select('input_options.*')->where('input_options.input_id',$input->id)->get();
        }
        $data['inputs']=$inputsArray;
        return View('hr::Request.showForm',$data);
    }

}