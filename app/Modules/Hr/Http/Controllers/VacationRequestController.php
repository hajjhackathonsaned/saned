<?php

namespace App\Modules\Hr\Http\Controllers;

use App\Modules\Hr\Http\Requests\VacationRequestRequest;
use App\Modules\Hr\Repositories\VacationRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class VacationRequestController extends HrController
{
    private $repo;
    public function __construct(VacationRequestRepository $repo)
    {
        parent::__construct();
        $this->repo=$repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::VacationRequest.page_title')]
        ];
        $data['urls']=[
            'get_data_url'=>route('hr::VacationRequest-get-table-data'),
            'delete_url'=>route('hr::VacationRequest/delete'),
            'delete_multi_url'=>route('hr::VacationRequest/delete-multi'),
            'edit_url'=>route('hr::VacationRequest/edit',['id'=>'']),
        ];
        return View('hr::VacationRequest.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::VacationRequest.page_title')],
            ['title' => trans('hr::VacationRequest.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::VacationRequest-Create'),
        ];
        
        return View('hr::VacationRequest.form',$data);
    }

    /**
     * @param JobTitleRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(VacationRequestRequest $request){
        $add=$this->repo->add($request);
        if($add[0]){
            \Session::flash('message', trans('hr::VacationRequest.'.$add[1]));
            \Session::flash('alert-class', 'alert-success');
            return redirect()->route('hr::VacationRequest-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::VacationRequest.'.$add[1])]);
        }

    }

    public function getData(Request $request)
    {
        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage)
        {
            return $perPage;
        });
        $paginate = \DB::table('vacation_requests')
                        ->select('vacation_requests.id','vacation_requests.title','vacation_requests.created_at','hr_users.name as user_name')
                        ->join('hr_users', 'hr_users.id', '=', 'vacation_requests.created_by')
                        ->paginate($length);

        $items = $paginate->items();
     
        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $paginate->total();
        $d['recordsFiltered'] = $paginate->total();
        return Response()->json($d);
    }

    public function delete(Request $request)
    {
        try {
            $repose = $this->repo->delete($request->input('id'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        try {
            $repose = $this->repo->deleteAll($request->input('ids'));
            if ($repose) {
                return Response()->json([true, 'deleted_success', $repose]);
            }
            return Response()->json([false, 'error_delete_row', $repose]);
        } catch (\PDOException $e) {
            return Response()->json([false, $e->errorInfo[2]]);
        }
    }
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Request $request)
    {

        $data=[];
        $data['page_breadcrumb'] = [
            ['title' => trans('hr::VacationRequest.page_title')],
            ['title' => trans('hr::VacationRequestVacationRequest.page_title_form')],
        ];
        $data['urls']=[
            'save_url'=>route('hr::VacationRequest-Update'),
        ];
        $details=$this->repo->find($id);
        $data['id']=$details->id;
        $data['title']=$details->title;
        $data['date_from']=$details->date_from;
        $data['date_to']=$details->date_to;
        $data['comment_id']=$details->comment_id;
        $data['type']=$details->type;
        $comment=$details->comment();
        $employee=$details->employee();
        $data['comment']=$comment['comment'];
        $data['employee']=$employee['name'];
        return View('hr::VacationRequest.form', $data);
    }

    public function update(VacationRequestRequest $request){
        $add=$this->repo->edit($request);
        if($add[0]){
            \Session::flash('message', trans('hr::VacationRequest.'.$add[1]));
            \Session::flash('alert-class', 'alert-info');
            return redirect()->route('hr::VacationRequest-List');
        }else{
            return redirect()->back()->withErrors([trans('hr::VacationRequest.'.$add[1])]);
        }

    }
}
