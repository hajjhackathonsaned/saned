<?php

namespace App\Modules\Hr\Http\Requests;
use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [];
        if($request->ajax()){
            return $rules;
        }
        $rules['first_name'] = 'required|max:255';
        $rules['last_name'] = 'required|max:255';
        $rules['department_id'] = 'required';
        $rules['job_title_id'] = 'required';
        $rules['role_id'] = 'required';
        $rules['email'] = 'required|email|unique:users,email,' . $request->input('id');
        if (!$request->input('id')) {
            $rules['password'] = 'confirmed|min:8';
        }
        return $rules;
        
    }
}
