<?php

namespace App\Modules\Hr\Models;

use Illuminate\Database\Eloquent\Model;

class HrModel extends Model
{
    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = '';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }

    public function getValue($colum)
    {
        return $this->morphMany('\App\Models\Translate', 'translatable')->where('locale','=',\App::getLocale())->where('column_name','=',$colum)->first()->value;
    }
}
