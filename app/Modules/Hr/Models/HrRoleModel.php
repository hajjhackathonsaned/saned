<?php

namespace App\Modules\Hr\Models;

use Illuminate\Database\Eloquent\Model;

class HrRoleModel extends Model
{
    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'hr_roles';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }
     public function jobTitle() {
        return $this->belongsTo(JobTitleModel::class);
    }
    public function department()
    {
        return $this->hasOne(DepartmentModel::class);
    }
}
