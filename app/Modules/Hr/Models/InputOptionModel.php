<?php

namespace App\Modules\Hr\Models;

use Illuminate\Database\Eloquent\Model;

class InputOptionModel extends Model
{
    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'input_options';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }
    public function getValue($colum,$lang)
    {
        return $this->morphMany('\App\Models\Translate', 'translatable')->where('locale','=',$lang)->where('column_name','=',$colum)->first()->value;
    }
    
    
}
