<?php

namespace App\Modules\Hr\Models;

use Illuminate\Database\Eloquent\Model;

class JobTitleModel extends Model
{
    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'job_titles';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }
    public function getValue($colum,$lang)
    {
        return $this->morphMany('\App\Models\Translate', 'translatable')->where('locale','=',$lang)->where('column_name','=',$colum)->first()->value;
    }
    public function department()
    {
        return $this->hasOne(DepartmentModel::class,'department_id');
    }
}
