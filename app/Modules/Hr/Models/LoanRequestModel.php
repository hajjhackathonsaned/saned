<?php

namespace App\Modules\Hr\Models;

use Illuminate\Database\Eloquent\Model;

class LoanRequestModel extends Model
{
    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'loan_requests';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }
    public function getValue($colum,$lang)
    {
        return $this->morphMany('\App\Models\Translate', 'translatable')->where('locale','=',$lang)->where('column_name','=',$colum)->first()->value;
    }
    public function comment()
    {
        return $this->belongsTo(CommentModel::class,'comment_id')->select(['comment'])->first()->toArray();;
    }
    public function employee()
    {
        return $this->belongsTo(HrUserModel::class,'user_id')->select(['name'])->first()->toArray();;
    }
    
}
