<?php

namespace App\Modules\Hr\Models;

use Illuminate\Database\Eloquent\Model;

class RecruitmentsModel extends Model
{
    protected $guarded = ['id'];
    protected $connection = 'mysql';
    protected $table = 'recruitments';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function translate()
    {
        return $this->morphMany('\App\Models\Translate', 'translatable');
    }
    public function getValue($colum,$lang)
    {
        return $this->morphMany('\App\Models\Translate', 'translatable')->where('locale','=',$lang)->where('column_name','=',$colum)->first()->value;
    }
    public function comment()
    {
        return $this->belongsTo(CommentModel::class,'comment_id')->select(['comment'])->first()->toArray();;
    }
    
    
}
