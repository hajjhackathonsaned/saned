<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Storage;

class ComplaintsRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\ComplaintsModel';
    }
    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function add($request){
        $data=[];
        $dataComment=[];

        $data['title']=$request->input('title');
        $data['created_by']=1;
        $data['type']=$request->input('type');
        $data['comment_id']=\DB::table('comments')->insertGetId([
                'comment' => $request->input('comment'),
                'entity_id'=>2
                ]);
        $file = $request->attachment;
        $fileName=time()."_".$file->getClientOriginalName();
        $path = $request->attachment->getRealPath();
        Storage::disk('local')->put($fileName, file_get_contents($path));
        $data['attachment']=$fileName;         //begin insert data
        try {
            $add = $this->create($data);
            if (!$add) {
                \DB::rollback();
                return [false, 'add_error', $add];
            }else{
               \DB::commit();
                return [true, 'success_add', $add]; 
            }
            
        } catch (\PDOException $ex) {
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
            
        }
        
    }
    /**
     * @param $request
     * @return array
     */
    public function edit($request){
        $data=[];
        $data['title']=$request->input('title');
        $data['type']=$request->input('type');
        \DB::table('comments')
            ->where('id',$request->input('comment_id'))
            ->update(['comment' => $request->input('comment')]);
        $file = $request->attachment;
        $fileName=time()."_".$file->getClientOriginalName();
        $path = $request->attachment->getRealPath();
        Storage::disk('local')->put($fileName, file_get_contents($path));
        $data['attachment']=$fileName;

        
        try {
            $update = $this->update($data,$request->input('id'));
            if (!$update) {
                \DB::rollback();
                return [false, 'add_error', $update];
            }else{
               \DB::commit();
                return [true, 'success_add', $update]; 
            }
            
        } catch (\PDOException $ex) {
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
            
        }
       
    }
}