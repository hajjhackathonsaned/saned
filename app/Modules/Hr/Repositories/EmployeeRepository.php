<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class EmployeeRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\EmployeeModel';
    }
    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function add($request){
        $data=[];
        $data['first_name']=$request->input('first_name');
        $data['last_name']=$request->input('last_name');
        $data['sex']=$request->input('sex');
        $data['job_title_id']=$request->input('job_title_id');
        $data['department_id']=$request->input('department_id');
        $data['code']=$request->input('code');
        $data['email']=$request->input('email');
        $data['role_id']=$request->input('role_id');
        $data['password']= bcrypt($request->input('password'));
        // set data language
        $translate=[];
        
       
        try{
            \DB::beginTransaction();
            $add = $this->create($data); //begin insert data
            if (!$add) {
                \DB::rollback();
                return [false, 'add_error', $add];
            }
            // start to insert question locale translation
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_add', $add];
        }catch (\PDOException $ex){
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
        }
    }
    /**
     * @param $request
     * @return array
     */
    public function edit($request){
        $emp = $this->find($request->input('id'));
        $data=[];
        $data['first_name']=$request->input('first_name');
        $data['last_name']=$request->input('last_name');
        $data['sex']=$request->input('sex');
        $data['job_title_id']=$request->input('job_title_id');
        $data['department_id']=$request->input('department_id');
        $data['code']=$request->input('code');
        $data['email']=$request->input('email');
        $data['role_id']=$request->input('role_id');
        $password = $emp->password;

        if ($request->input('password') != NULL) {
            $password = bcrypt($request->input('password'));
        } else {
            $password = $password;
        }
        $data['password']=$password;
        // set data language
        $translate=[];
        try{
            \DB::beginTransaction();
            $update = $this->update($data,$request->input('id')); //begin insert data
            if (!$update) {
                \DB::rollback();
                return [false, 'update_error', $update];
            }
            // start to insert question locale translation
            $add = $this->find($request->input('id'));
            $add->translate()->delete();
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_update', $add];
        }catch (\PDOException $ex){
            \DB::rollback();
            return [false,'database_error'];
        }
    }
}