<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class HrRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\HrModel';
    }
    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function add($request){
        $data=[];
        $data['title']=$request->input('title')[app()->getLocale()];
        $data['description']=$request->input('description')[app()->getLocale()];
        $data['status']=1;
        // set data language
        $translate=[];
        foreach (config('app.supported_lang') as $lang){
            $translate[]=[
                'column_name'=>'title',
                'locale'=>$lang,
                'value'=>$request->input('title')[$lang],
            ];
            $translate[]=[
                'column_name'=>'description',
                'locale'=>$lang,
                'value'=>$request->input('description')[$lang],
            ];
        }
        try{
            \DB::beginTransaction();
            $add = $this->create($data); //begin insert data
            if (!$add) {
                \DB::rollback();
                return [false, 'add_error', $add];
            }
            // start to insert question locale translation
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_add', $add];
        }catch (\PDOException $ex){
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
        }
    }
    /**
     * @param $request
     * @return array
     */
    public function edit($request){
        $data=[];
        $data['title']=$request->input('title')[app()->getLocale()];
        $data['description']=$request->input('description')[app()->getLocale()];
        // set data language
        $translate=[];
        foreach (config('app.supported_lang') as $lang){
            $translate[]=[
                'column_name'=>'title',
                'locale'=>$lang,
                'value'=>$request->input('title')[$lang],
            ];
            $translate[]=[
                'column_name'=>'description',
                'locale'=>$lang,
                'value'=>$request->input('description')[$lang],
            ];
        }
        try{
            \DB::beginTransaction();
            $update = $this->update($data,$request->input('id')); //begin insert data
            if (!$update) {
                \DB::rollback();
                return [false, 'update_error', $update];
            }
            // start to insert question locale translation
            $add = $this->find($request->input('id'));
            $add->translate()->delete();
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_update', $add];
        }catch (\PDOException $ex){
            \DB::rollback();
            return [false,'database_error'];
        }
    }
}
