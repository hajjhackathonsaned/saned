<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class LoanRequestRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\LoanRequestModel';
    }
    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function add($request){
        $data=[];
        $dataComment=[];

        $data['amount']=$request->input('amount');
        $data['user_id']=1;
        $data['status_id']=$request->input('status_id');
        $data['comment_id']=\DB::table('comments')->insertGetId([
                'comment' => $request->input('comment')
                ]);
         //begin insert data
        try {
            $add = $this->create($data);
            if (!$add) {
                \DB::rollback();
                return [false, 'add_error', $add];
            }else{
               \DB::commit();
                return [true, 'success_add', $add]; 
            }
            
        } catch (\PDOException $ex) {
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
            
        }
        
    }
    /**
     * @param $request
     * @return array
     */
    public function edit($request){
        $data=[];
        $data['amount']=$request->input('amount');
        $data['status_id']=$request->input('status_id');
        \DB::table('comments')
            ->where('id',$request->input('comment_id'))
            ->update(['comment' => $request->input('comment')]);
        
        $data['amount']=$request->input('amount');
        try {
            $update = $this->update($data,$request->input('id'));
            if (!$update) {
                \DB::rollback();
                return [false, 'add_error', $update];
            }else{
               \DB::commit();
                return [true, 'success_add', $update]; 
            }
            
        } catch (\PDOException $ex) {
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
            
        }
       
    }
}