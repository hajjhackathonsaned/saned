<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class OtherRequestRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\RequestModel';
    }
    public function edit($request){
        $data=[];
        $fields=json_decode($request->input('formBuliderJson'));
        $data['name']=$request->input('requestName');
        $data['json']=serialize($fields);
        // $data['json']=$request->input('formBuliderJson');
        $this->update($data,$request->input('id'));
    }
    public function updateInputs($fields ,$requestId){
        foreach ($fields as $key => $value) {
            $type=$value['type'];
            $label=$value['label'];
            $name=$value['name'];
            $check= DB::table('inputs')
                    ->select('*')
                    ->where([
                                ['name','=',$name],['request_id','=',$requestId]
                            ])
                    ->get()->toArray();
            if($check&&!empty($check)){
                DB::table('inputs')
                    ->where('id', $check[0]['id'])
                    ->update([
                            'name'=>$name,
                            'label'=>$label
                        ]);
            }else{
                $inputId = \DB::table('inputs')->insertGetId([
                'type' => $type,
                'label' => $label,
                'name' => $name,
                'request_id'=>$requestId
                ]);
                if(isset($value->values)){
                    foreach ($value->values as $optionKey => $option) {
                        $optionValue=$option->value;
                        $optionlabel=$option->label;
                        $inputId = \DB::table('input_options')->insertGetId([
                            'input_id' => $inputId,
                            'label' => $optionlabel,
                            'value' => $optionValue
                            ]);

                    }

                }
            }     
            
        }
    }
    public function setInputs($fields ,$requestId){
        foreach ($fields as $key => $value) {
            $type=$value->type;
            $label=$value->label;
            $name=$value->name;
            $inputId = \DB::table('inputs')->insertGetId([
                'type' => $type,
                'label' => $label,
                'name' => $name,
                'request_id'=>$requestId
                ]);
            if(isset($value->values)){
                foreach ($value->values as $optionKey => $option) {
                    $optionValue=$option->value;
                    $optionlabel=$option->label;
                    $inputId = \DB::table('input_options')->insertGetId([
                        'input_id' => $inputId,
                        'label' => $optionlabel,
                        'value' => $optionValue
                        ]);

                }

            }
        }
    }
     public function add($request){
        $data=[];
        $data['name']=$request->input('requestName');
        $fields=json_decode($request->input('formBuliderJson'));
        $data['json']=serialize($fields);
        $add = $this->create($data);
        
        
        $this->setInputs($fields,$add->id);
        
    }
    
}