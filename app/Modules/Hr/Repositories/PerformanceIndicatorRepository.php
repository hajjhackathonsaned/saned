<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class PerformanceIndicatorRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\PerformanceIndicatorModel';
    }
    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function add($request){
        $data=[];
        $data['name']=$request->input('name')[app()->getLocale()];
        $data['role_id']=$request->input('role_id');

        // set data language
        $translate=[];
        foreach (config('app.supported_lang') as $lang){
            $translate[]=[
                'column_name'=>'name',
                'locale'=>$lang,
                'value'=>$request->input('name')[$lang],
            ];
           
        }
        try{
            \DB::beginTransaction();
            $add = $this->create($data); //begin insert data
            if (!$add) {
                \DB::rollback();
                return [false, 'add_error', $add];
            }
            // start to insert question locale translation
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_add', $add];
        }catch (\PDOException $ex){
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
        }
    }
    /**
     * @param $request
     * @return array
     */
    public function edit($request){
        $data=[];
        $data['name']=$request->input('name')[app()->getLocale()];
        $data['role_id']=$request->input('role_id');

        // set data language
        $translate=[];
        foreach (config('app.supported_lang') as $lang){
            $translate[]=[
                'column_name'=>'name',
                'locale'=>$lang,
                'value'=>$request->input('name')[$lang],
            ];
            
        }
        try{
            \DB::beginTransaction();
            $update = $this->update($data,$request->input('id')); //begin insert data
            if (!$update) {
                \DB::rollback();
                return [false, 'update_error', $update];
            }
            // start to insert question locale translation
            $add = $this->find($request->input('id'));
            $add->translate()->delete();
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_update', $add];
        }catch (\PDOException $ex){
            \DB::rollback();
            return [false,'database_error'];
        }
    }
}