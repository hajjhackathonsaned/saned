<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class RecruitmentsRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\RecruitmentsModel';
    }
    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
    public function setLanguages($recId,$languages){
        \DB::table('recruitment_languages')
                ->where('recruitment_id', $recId)
                ->delete();
        foreach ($languages as $key => $value) {
            $check=\DB::table('recruitment_languages')
                    ->select(['id'])
                    ->where('recruitment_id','=',$recId)
                    ->where('language_id','=',$value)
                    ->get()->toArray();
            if(!$check){
               
                \DB::table('recruitment_languages')->insertGetId([
                'recruitment_id' => $recId,
                'language_id'=>$value
                ]);
            }
        }
        
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function add($request){
        $data=[];
        $dataComment=[];

        $data['title']=$request->input('title');
        $data['salary']=$request->input('salary');
        $data['experiences_years']=$request->input('experiences_years');
        $data['age']=$request->input('age');
        $data['job_title_id']=$request->input('job_title_id');
        $data['no_employees']=$request->input('no_employees');
        $data['type']=$request->input('type');
        $data['comment_id']=\DB::table('comments')->insertGetId([
                'comment' => $request->input('comment'),
                'entity_id'=>2
                ]);
         //begin insert data
        try {
            $add = $this->create($data);
            if (!$add) {
                \DB::rollback();
                return [false, 'add_error', $add];
            }else{
                $this->setLanguages($add->id,$request->input('languages'));
               \DB::commit();
                return [true, 'success_add', $add]; 
            }
            
        } catch (\PDOException $ex) {
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
            
        }
        
    }
    /**
     * @param $request
     * @return array
     */
    public function edit($request){
        $data=[];
        $data['title']=$request->input('title');
        $data['salary']=$request->input('salary');
        $data['experiences_years']=$request->input('experiences_years');
        $data['age']=$request->input('age');
        $data['job_title_id']=$request->input('job_title_id');
        $data['no_employees']=$request->input('no_employees');
        $data['type']=$request->input('type');
        \DB::table('comments')
            ->where('id',$request->input('comment_id'))
            ->update(['comment' => $request->input('comment')]);
        
        try {
            $update = $this->update($data,$request->input('id'));
            if (!$update) {
                \DB::rollback();
                return [false, 'add_error', $update];
            }else{
                $this->setLanguages($request->input('id'),$request->input('languages'));

               \DB::commit();
                return [true, 'success_add', $update]; 
            }
            
        } catch (\PDOException $ex) {
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
            
        }
       
    }
}