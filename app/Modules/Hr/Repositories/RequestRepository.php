<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class RequestRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\RequestModel';
    }
    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function customOptions($inputValues ,$inputId){
        if(isset($inputValues['min'])){
            $option = DB::table('input_options')
                        ->insertGetId([
                            'input_id' => $inputId,
                            'label' => 'min',
                            'value' => $inputValues['min']
                        ]);
        }
        if(isset($inputValues['max'])){
            $option = DB::table('input_options')
                        ->insertGetId([
                            'input_id' => $inputId,
                            'label' => 'min',
                            'value' => $inputValues['max']
                        ]);
        }
        if(isset($inputValues['step'])){
            $option = DB::table('input_options')
                        ->insertGetId([
                            'input_id' => $inputId,
                            'label' => 'step',
                            'value' => $inputValues['step']
                        ]);
        }
        if(isset($inputValues['value'])){
            $option = DB::table('input_options')
                        ->insertGetId([
                            'input_id' => $inputId,
                            'label' => 'value',
                            'value' => $inputValues['value']
                        ]);
        }
        


        
    }
    public function setInputs($fields ,$requestId){
        foreach ($fields as $key => $value) {
            $type=$value['type'];
            $label=$value['label'];
            $name=$value['name'];
            $inputId = DB::table('inputs')->insertGetId([
                'type' => $type,
                'label' => $label,
                'name' => $name,
                'request_id'=>$requestId
                ]);
            if(isset($value['values'])){
                foreach ($value['values'] as $optionKey => $option) {
                    $optionValue=$option['value'];
                    $optionlabel=$option['label'];
                    $option = DB::table('input_options')->insertGetId([
                        'input_id' => $inputId,
                        'label' => $optionlabel,
                        'value' => $optionValue
                        ]);

                }

            }
            $this->customOptions($value,$inputId);
        }
    }
    public function updateInputs($fields ,$requestId){
        foreach ($fields as $key => $value) {
            $type=$value['type'];
            $label=$value['label'];
            $name=$value['name'];
            $check= DB::table('inputs')
                    ->select('*')
                    ->where([
                                ['name','=',$name],['request_id','=',$requestId]
                            ])
                    ->get()->toArray();
            if($check&&!empty($check)){
                DB::table('inputs')
                    ->where('id', $check[0]['id'])
                    ->update([
                            'name'=>$name,
                            'label'=>$label
                        ]);
            }else{
                $inputId = DB::table('inputs')->insertGetId([
                'type' => $type,
                'label' => $label,
                'name' => $name,
                'request_id'=>$requestId
                ]);
                if(isset($value['values'])){
                    foreach ($value['values'] as $optionKey => $option) {
                        $optionValue=$option['value'];
                        $optionlabel=$option['label'];
                        $inputId = DB::table('input_options')->insertGetId([
                            'input_id' => $inputId,
                            'label' => $optionlabel,
                            'value' => $optionValue
                            ]);

                    }

                }
            }     
            
        }
    }
    public function edit($request){
        $data=[];
        $fields=json_decode($request->input('formBuliderJson'));
        $data['name']=$request->input('requestName');
        $data['json']=$request->input('formBuliderJson');
        $this->update($data,$request->input('id'));
    }
    public function add($request){
        $data=[];
        $fields=json_decode($request->input('formBuliderJson'));
        $data['name']=$request->input('requestName');
        $add = $this->create($data);
        $this->setInputs($fields,$add->id);  
    }
    /**
     * @param $request
     * @return array
     */
    // public function edit($request){
    //     $data=[];
    //     $data['name']=$request->input('name')[app()->getLocale()];

    //     // set data language
    //     $translate=[];
    //     foreach (config('app.supported_lang') as $lang){
    //         $translate[]=[
    //             'column_name'=>'name',
    //             'locale'=>$lang,
    //             'value'=>$request->input('name')[$lang],
    //         ];
            
    //     }
    //     try{
    //         \DB::beginTransaction();
    //         $update = $this->update($data,$request->input('id')); //begin insert data
    //         if (!$update) {
    //             \DB::rollback();
    //             return [false, 'update_error', $update];
    //         }
    //         // start to insert question locale translation
    //         $add = $this->find($request->input('id'));
    //         $add->translate()->delete();
    //         $translate=$add->translate()->createMany($translate);
    //         if(!$translate){
    //             \DB::rollback();
    //             return [false, 'translation_add_error', $add];
    //         }
    //         \DB::commit();
    //         return [true, 'success_update', $add];
    //     }catch (\PDOException $ex){
    //         \DB::rollback();
    //         return [false,'database_error'];
    //     }
    // }
}