<?php

namespace App\Modules\hr\Repositories;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Class UsersRepository
 * @package App\Core\Repositories
 */
class SettingRepository extends Repository
{

    /**
     * @return string
     */
    public function model()
    {
        return 'App\Modules\Hr\Models\Setting';
    }

    /**
     * @param array $data
     * @param $key
     * @return mixed
     */
    public function updateSetting(array $data,$key){
        return $this->model->where('key_id',$key)->update($data);
    }
    /**
     * @param array $data
     * @return mixed
     */
    public function getWhereIn(array $data){
        return $this->model->whereIn('set_group',$data)->get();
    }
}
