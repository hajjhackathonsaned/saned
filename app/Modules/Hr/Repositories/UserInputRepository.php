<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Storage;
class UserInputRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\UserInputModel';
    }
    public function edit($request){
    	$inputs=$request->input('formRequest');
    	foreach ($inputs as $key => $value) {
    		$input=Self::findWhere([
    			['input_id',"=",$key],
    			['user_request_id',"=",$request->input('id')]
    			])->toArray();
    		$data=[];
    		$data['input_id']=$key;
    		$data['value']=$value;
    		$update = $this->update($data,$input[0]['id']);
    	}
        
    }
    public function checkInputType($inputId){
        $check=\DB::table('inputs')
                    ->select("type")
                    ->where("id",$inputId)
                    ->first();
        return $check->type;
    }
    public function add($request ,$userRequestId){
    	$inputs=$request->input('formRequest');
        $files=$request->files;
        if(!empty($files)){
           foreach($files as $key=>$value){
                foreach ($value as $key_ => $value_) {
                        $fileName=time()."_".$value_->getClientOriginalName();
                        $path = $value_->getRealPath();
                        Storage::disk('local')->put($fileName, file_get_contents($path));
                        $data=[];
                        $data['user_request_id']=$userRequestId;
                        $data['input_id']=$key_;
                        $data['value']=$fileName;
                        $this->create($data);
                }
            } 
        }
        
    	foreach ($inputs as $key => $value) {
            
    		$data=[];
    		$data['user_request_id']=$userRequestId;
    		$data['input_id']=$key;
    		$data['value']=$value;
    		$this->create($data);
    	}
        exit;
        
    }
}