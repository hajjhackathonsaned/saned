<?php

namespace App\Modules\hr\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class UserRequestRepository extends Repository
{
    /**
     * @return string
     */
    public function model() {
        return 'App\Modules\Hr\Models\UserRequestModel';
    }
    public function add($request){
        $data=[];
        $data['request_id']=$request;
        $data['user_id']=1;
        return $this->create($data);        
    }
}