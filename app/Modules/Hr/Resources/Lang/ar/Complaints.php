<?php

return [
    'page_title' => 'شكاوى وأقتراحات',
    'content_title' => 'شكاوى وأقتراحات',
    'table_title' => 'عرض جميع شكاوى\أقترحات',
    'page_title_form' => 'اضافة شكوى\أقتراح',
    'type' => 'نوع الطلب',
    'title'=>'عنوان الطلب',
    'question_add_error'=> 'حدث خطأ أثناء اضافة شكوى\أقتراح',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه لشكوى\أقتراح',
    'success_add'=> 'تم اضافة طلب شكوى\أقتراح  بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات شكوى\أقتراح بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل شكوى\أقتراح',
    'correct'=> 'الاجابة الصحيحة',
    'comment'=>'التعليق',
    'Suggestions'=>'أقتراح',
    'Complaints'=>'شكوى',
    'employee'=>'الموظف',
    'attachment'=>'المرفقات'
];
