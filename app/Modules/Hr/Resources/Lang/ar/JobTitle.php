<?php

return [
    'page_title' => 'المسمى الوظيفى',
    'content_title' => 'المسمى الوظيفى',
    'table_title' => 'عرض جميع المسميات',
    'title' => 'العنوان',
    'description' => 'التفاصيل',
    'image' => 'الصورة',
    'name' =>'الاسم',
    'page_title_form' => 'اضافة مسمى وظيفى جديد',
    'status' => 'الحالة',
    'question_add_error'=> 'حدث خطأ أثناء اضافة المسمى الوظيفى',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه للمسمى الوظيفى',
    'success_add'=> 'تم اضافة المسمى الوظيفى  بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات المسمى الوظيفى بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل المسمى الوظيفى',
    'correct'=> 'الاجابة الصحيحة',
    'department'=>'القسم',
    'select-department'=>'اختر القسم'
];
