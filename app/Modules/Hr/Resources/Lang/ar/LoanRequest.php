<?php

return [
    'page_title' => 'طلب سلفة',
    'content_title' => 'طلب سلفة',
    'table_title' => 'عرض جميع طلبات السلف',
    'amount'=>'المبلغ',
    'page_title_form' => 'اضافة طلب سلفة جديد',
    'status' => 'الحالة',
    'question_add_error'=> 'حدث خطأ أثناء اضافة طلب سلفة',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه لطلب السلفة',
    'success_add'=> 'تم اضافة طلب السلفة  بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات طلب السلفة بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل طلب السلفة',
    'correct'=> 'الاجابة الصحيحة',
    'select-status'=>'اختر الحالة',
    'comment'=>'التعليق',
    'employee'=>'الموظف'
];
