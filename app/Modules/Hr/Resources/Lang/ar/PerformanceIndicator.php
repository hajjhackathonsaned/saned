<?php

return [
    'page_title' => 'مؤشرات الأداء',
    'content_title' => 'مؤشرات الأداء',
    'table_title' => 'عرض جميع مؤشرات الأداء',
    'name' => 'الأسم',
    'page_title_form' => 'اضافة مؤشر أداء جديد',
    'question_add_error'=> 'حدث خطأ أثناء اضافة مؤشر أداء',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه لمؤشر أداء',
    'success_add'=> 'تم اضافة مؤشر أداء  بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات مؤشر أداء بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل مؤشر أداء',
    'select-role'=> 'أختار الصلحية',
    'role_id'=>'الصلحيات'
];
