<?php

return [
    'page_title' => 'طلبات التوظيف',
    'content_title' => 'طلبات التوظيف',
    'table_title' => 'عرض جميع طلبات التوظيف',
    'page_title_form' => 'اضافة طلب توظيف',
    'type' => 'نوع التوظيف',
    'title'=>'عنوان الطلب',
    'question_add_error'=> 'حدث خطأ أثناء اضافة طلب التوظيف',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه طلب التوظيف',
    'success_add'=> 'تم اضافة طلب التوظيف  بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات طلب التوظيف بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل طلب التوظيف',
    'correct'=> 'الاجابة الصحيحة',
    'comment'=>'الوصف',
    'job_title'=>'المسمى الوظيفى',
    'no_employees'=>'عدد الموظفين',
    'age'=>'العمر',
    'salary'=>'متوسط المرتب',
    'langauges'=>'اللغات',
    'freelance'=>'حر',
    'part-time'=>'دوام جزئى',
    'full-time'=>'دوام كامل',
    'select-type'=>'اختر نوع التوظيف',
    'select-job-title'=>'اختر المسمى الوظيفى',
    'experiences_years'=>'عدد سنوات الخبرة'
];
