<?php

return [
    'page_title' => 'طلبات الاجازات',
    'content_title' => 'طلبات الاجازات',
    'table_title' => 'عرض جميع طلبات الاجازات',
    'page_title_form' => 'اضافة طلب أجازة',
    'title'=>'عنوان الطلب',
    'question_add_error'=> 'حدث خطأ أثناء اضافة طلب أجازة',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه طلب أجازة',
    'success_add'=> 'تم اضافة طلب أجازة  بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات طلب أجازة بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل طلب أجازة',
    'correct'=> 'الاجابة الصحيحة',
    'comment'=>'التعليق',
    'date_from'=>'الاجازة من',
    'date_to'=>'الاجازة الى',
    'employee'=>'الموظف',
    'attachment'=>'المرفقات'
];
