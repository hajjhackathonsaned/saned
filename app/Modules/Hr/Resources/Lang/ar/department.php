<?php

return [
    'page_title' => 'الاقسام',
    'content_title' => 'الاقسام',
    'table_title' => 'عرض جميع الاقسام',
    'title' => 'العنوان',
    'description' => 'التفاصيل',
    'image' => 'الصورة',
    'name' =>'الاسم',
    'page_title_form' => 'اضافة قسم جديد',
    'status' => 'الحالة',
    'question_add_error'=> 'حدث خطأ أثناء اضافة القسم',
    'translation_add_error'=> 'حدث خطأ أثناء اضافة اللغه للقسم',
    'success_add'=> 'تم اضافة قسم بنجاح',
    'database_error'=> 'حدث خطأ فى قاعدة البيانات',
    'success_update'=> 'تم تعديل بيانات القسم بنجاح',
    'update_error'=> 'حدث خطأ اثناء تعديل القسم',
    'correct'=> 'الاجابة الصحيحة',
    
];
