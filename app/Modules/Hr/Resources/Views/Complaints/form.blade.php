@extends('hr::layout')

@section('page-site-title')
    {{trans('hr::Complaints.page_title_form')}}
@endsection

@section('page-title')
    {{trans('hr::Complaints.content_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('hr::layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{trans('hr::Complaints.page_title_form')}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{$urls['save_url']}}" method="post" class="form-horizontal" id="add_form"  enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type='hidden' name='id' value='@if(isset($id)){{$id}}@endif'/>
                        <div class="form-body" id="">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span> {{trans('common.correct_form_error')}} </span>
                            </div>
                            <div role="tabpanel">
                                <!-- Nav tabs -->
                               
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    @if(isset($employee))
                                        <div class="form-group">
                                            <label class="control-label col-md-2">{{trans('hr::Complaints.employee')}}<span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-10">
                                                {{$employee}}
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::Complaints.title')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="title" value="@if(isset($title)){{$title}}@endif"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::Complaints.type')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="type">
                                                <option value="1" @if(isset($type)&&$type==1) selected="selected" @endif >{{trans('hr::Complaints.Suggestions')}}</option>
                                                <option value="2" @if(isset($type)&&$type==2) selected="selected" @endif >{{trans('hr::Complaints.Complaints')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::Complaints.attachment')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="file" name="attachment">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::Complaints.comment')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="hidden" name="comment_id" value="@if(isset($comment_id)) {{$comment_id}} @endif">
                                            <textarea class="form-control" name="comment">@if(isset($comment)){{$comment}}@endif</textarea>
                                        </div>
                                    </div>

                                        
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-footer-js')
    <script type="application/javascript">
        var model_js_lang = {!! json_encode(trans('question.js_lang'))  !!};
        var urls = {!! json_encode($urls) !!};
    </script>
    <script type = "text/javascript" src = "{{ asset('assets/global/plugins/datatables/datatables.min.js')}}" ></script>
    <script src = "{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/moment.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/models/js/Complaints.js')}}" type = "text/javascript" ></script>

@endsection

@section('page-heads')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    @if(trans('app.dir') == 'rtl')
        <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css') }}" rel="stylesheet" type="text/css" />
    @else
        <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    @endif
@endsection