@extends('hr::layout')

@section('page-site-title')
    {{trans('hr::Request.page_title_form')}}
@endsection

@section('page-title')
    {{trans('hr::Request.content_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('hr::layouts.widgets.page-side')
@endsection

@section('page-content')
<script>
var formDataJson="";
@if(isset($json)&&!empty($json))
    formDataJson = '<?php echo $json;?>';
@endif
</script>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{trans('hr::Request.page_title_form')}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{$urls['save_url']}}" method="post" class="form-horizontal" id="add_form">
                        {!! csrf_field() !!}
                        <input type='hidden' name='id' value='@if(isset($id)){{$id}}@endif'/>
                        <div class="form-body" id="">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span> {{trans('common.correct_form_error')}} </span>
                            </div>
                            <div role="tabpanel">
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="requestName" value="@if(isset($name)){{$name}}@endif"/>
                                    </div>
                                    <label class="control-label col-md-2">{{trans('hr::Request.name')}}<span class="required" aria-required="true"> * </span></label>

                                </div>
                                <!-- Nav tabs -->
                                <!-- ul class="nav nav-tabs" role="tablist">
                                    @foreach(config('app.supported_lang') as $lang)
                                        <li role="presentation" class="@if ($lang == \App::getLocale())  active @endif"><a href="#{{$lang}}" aria-controls="#{{$lang}}" role="tab" data-toggle="tab">{{trans("app.{$lang}")}}</a></li>
                                    @endforeach
                                </ul> -->
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="stage1" class="build-wrap"></div>
                                    <br /><br />
                                    <button id="getJSON" class="btn btn-success" type="button">{{trans('common.save')}}</button>
                                    <input type="hidden" name="formBuliderJson" value="" id="formBuliderJson" />
                                    <input type="hidden" id="formDataJson" value="@isset($json) {{$json}} @endif">
                                    <br /><br />

                                   
                                </div>
                            </div>
                        </div>
                        
                        <!-- <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                                </div>
                            </div>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-footer-js')
    <script type="application/javascript">
        var formData="";
        var model_js_lang = {!! json_encode(trans('question.js_lang'))  !!};
        var urls = {!! json_encode($urls) !!};
    </script>
    <script type = "text/javascript" src = "{{ asset('assets/global/plugins/datatables/datatables.min.js')}}" ></script>
    <script src = "{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/moment.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type = "text/javascript" ></script>

    <script src = "{{ asset('assets/FormBuilder/js/vendor.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/FormBuilder/js/form-builder.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/FormBuilder/js/form-render.min.js')}}" type = "text/javascript" ></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.js"></script>
    @if(!isset($json))
    <script src = "{{ asset('assets/FormBuilder/js/demo.js')}}" type = "text/javascript" ></script>
    @endif
    @if(isset($json)&&!empty($json))
        <script>
        var fbEditor = document.getElementById('stage1');
        var formBuilder = $(fbEditor).formBuilder();
        //var formDataJson = '<?php //echo $json;?>';
        $(function(){
            formBuilder.actions.setData(formDataJson);
            document.getElementById('getJSON').addEventListener('click', function() {
              document.getElementById('formBuliderJson').value =formBuilder.actions.getData('json', true);
              $("#add_form").submit();      
            });
        });
        </script>
    @endif
@endsection

@section('page-heads')
    <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.css">

   
    
@endsection