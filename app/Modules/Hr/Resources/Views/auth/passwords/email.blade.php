@extends('layouts.login')

<!-- Main Content -->
@section('content')
<div class="logo">
    <a href="index.html">
        <img src="{{ asset('assets/global/img/logo-big.png')}}" alt="" />
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <form class="" action="{{ route('auth-password-email') }}" method="post">
        {!! csrf_field() !!}
        <h3 class="font-green">{{trans('passwords.forget_password')}}</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> {{trans('common.login_empty_error')}} </span>
        </div>
        @if ($errors->has('email'))
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span> {{ $errors->first('email') }}</span>
        </div>
        @endif
        <p> {{trans('passwords.enter_email_to_reset')}}</p>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input class="form-control placeholder-no-fix" value="{{ old('email') }}" type="email" required autocomplete="off" placeholder="{{trans('passwords.email')}}" name="email" /> </div>
        <div class="form-actions">
            <a type="button" id="back-btn" href='{{ route('auth-login-form') }}' class="btn btn-default">{{trans('passwords.back')}}</a>
            <button type="submit" class="btn btn-success uppercase pull-right">{{trans('passwords.reste_pass')}}</button>
        </div>
    </form>
</div>
@endsection
