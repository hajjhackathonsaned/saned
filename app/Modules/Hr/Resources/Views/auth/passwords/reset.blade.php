@extends('layouts.login')
@section('page-site-title')
    {{trans('auth.page-title')}}
@endsection
@section('content')
    <div class="user-login-5">
        <div class="row bs-reset">
            <div class="col-md-6 bs-reset mt-login-5-bsfix">
                <div class="login-bg" style="background-image:url({{ asset('assets/pages/img/login/bg1.jpg') }})">
                    <img class="login-logo" src="{{ asset('assets/pages/img/login/logo.png') }}" /> </div>
            </div>
            <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                <div class="login-content">
                    <h1>Metronic Admin Login</h1>
                    <p> Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing. </p>
                    <form action="{{ route('auth-password-reset') }}" class="login-form" method="post">
                        {!! csrf_field() !!}
                        {{--@if ($errors->has('email'))--}}
                            {{--<div class="alert alert-danger">--}}
                                {{--<button class="close" data-close="alert"></button>--}}
                                {{--<span> {{ $errors->first('email') }}</span>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                        {{--@if ($errors->has('password'))--}}
                            {{--<div class="alert alert-danger">--}}
                                {{--<button class="close" data-close="alert"></button>--}}
                                {{--<span> {{ $errors->first('password') }}</span>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                        {{--@if ($errors->has('password_confirmation'))--}}
                            {{--<div class="alert alert-danger">--}}
                                {{--<button class="close" data-close="alert"></button>--}}
                                {{--<span> {{ $errors->first('password_confirmation') }}</span>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="email" value="{{ $email or old('email') }}">

                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            <span> {{trans('auth.login_empty_error')}} </span>
                        </div>
                        @if ($errors->any())
                            <br>
                            <div class="alert fade in alert-danger">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{!! $error !!}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="{{trans('passwords.password_field')}}" name="password" required/> </div>
                            <div class="col-xs-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="{{trans('passwords.password_field_conf')}}" name="password_confirmation" required/> </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <button class="btn green" type="submit">{{trans('passwords.reset_password')}}</button>
                            </div>
                        </div>
                    </form>
                    <!-- BEGIN FORGOT PASSWORD FORM -->
                </div>
                <div class="login-footer">
                    <div class="row bs-reset">
                        <div class="col-xs-5 bs-reset">
                            <ul class="login-social">
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-dribbble"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-7 bs-reset">
                            <div class="login-copyright text-right">
                                <p>Copyright &copy; Keenthemes 2015</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
