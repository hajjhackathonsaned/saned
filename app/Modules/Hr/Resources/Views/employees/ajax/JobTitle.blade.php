<option value="">{{trans('hr::employees.select-job_title')}}</option>
@if(isset($jobTitles))
	@foreach($jobTitles as $key=>$jobTitle)
	    <option value="{{$key}}"  >{{$jobTitle['title']}}</option>

	@endforeach
@endif