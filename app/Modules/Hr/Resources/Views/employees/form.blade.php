@extends('hr::layout')

@section('page-site-title')
    {{trans('hr::employees.page_title_form')}}
@endsection

@section('page-title')
    {{trans('hr::employees.content_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('hr::layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{trans('hr::employees.page_title_form')}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{$urls['save_url']}}" method="post" class="form-horizontal" id="add_form">
                        {!! csrf_field() !!}
                        <input type='hidden' name='id' value='@if(isset($id)){{$id}}@endif'/>
                        <div class="form-body" id="">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span> {{trans('common.correct_form_error')}} </span>
                            </div>
                            <div role="tabpanel">
                                <!-- Nav tabs -->
                                
                                <!-- Tab panes -->
                                <div class="tab-content">
                                   <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.first_name')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="first_name" value="@if(isset($first_name)){{$first_name}}@endif"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.last_name')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="last_name" value="@if(isset($last_name)){{$last_name}}@endif"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.sex')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="radio" name="sex" value="Male" @if(isset($sex)&&$sex=="Male") checked="checked" @endif> {{trans('hr::employees.male')}}<br>
                                            <input type="radio" name="sex" value="Female"  @if(isset($sex)&&$sex=="Female") checked="checked" @endif> {{trans('hr::employees.female')}}<br>                    
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.department')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="department_id" id="departmentId" >
                                                <option value="">{{trans('hr::employees.select-department')}}</option>
                                                @foreach($departments as $key=>$department)
                                                <option value="{{$key}}" @if(isset($department_id)&&$key==$department_id) selected="selected" @endif >{{$department['name']}}</option>
                                                @endforeach
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group" @if(!isset($job_title_id)) style="display:none;" @endif id="jobTitleDiv">
                                        <label class="control-label col-md-2">{{trans('hr::employees.job_title')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <select class="form-control" id="jobTitleId" name="job_title_id">
                                                <option value="">{{trans('hr::employees.select-job_title')}}</option>
                                                @foreach($jobTitles as $key=>$jobTitle)
                                                <option value="{{$key}}" @if(isset($job_title_id)&&$key==$job_title_id) selected="selected" @endif >{{$jobTitle['title']}}</option>
                                                @endforeach
                                            </select>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.code')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="code" value="@if(isset($code)){{$code}}@endif"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.email')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="email" value="@if(isset($email)){{$email}}@endif"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.roleId')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="role_id"  >
                                                <option value="">{{trans('hr::employees.select-roleId')}}</option>
                                                @foreach($roles as $key=>$role)
                                                <option value="{{$key}}" @if(isset($role_id)&&$key==$role_id) selected="selected" @endif >{{$role['name']}}</option>
                                                @endforeach
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.password')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control" name="password" id="password" value=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{trans('hr::employees.passwordConfirmation')}}<span class="required" aria-required="true"> * </span></label>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-footer-js')
    <script type="application/javascript">
        var model_js_lang = {!! json_encode(trans('question.js_lang'))  !!};
        var urls = {!! json_encode($urls) !!};
    </script>
    <script type = "text/javascript" src = "{{ asset('assets/global/plugins/datatables/datatables.min.js')}}" ></script>
    <script src = "{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/moment.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/models/js/questions.js')}}" type = "text/javascript" ></script>
    <script>
    $("#departmentId").on("change",function(){
        $.ajax({
            
                url: "{{route('hr::Employees-changeJobTitle')}}",
                datatType : 'html',
                type: 'POST',
                data: {departmentId:$(this).val()},
                cache: false,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                success: function (opt){
                    $("#jobTitleDiv").css("display","block");
                    $("#jobTitleId").empty();
                    $("#jobTitleId").append(opt);
                }
            });
    });
    </script>

@endsection

@section('page-heads')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    @if(trans('app.dir') == 'rtl')
        <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css') }}" rel="stylesheet" type="text/css" />
    @else
        <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    @endif
@endsection