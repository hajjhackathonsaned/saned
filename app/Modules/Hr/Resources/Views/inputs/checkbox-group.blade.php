<div class="form-group">
    <label class="control-label col-md-2">{{$input['label']}}<span class="required" aria-required="true"> * </span></label>
    <div class="col-md-10">
		<div class="mt-checkbox-list">
			@if(isset($input['options'])&&!empty($input['options']))
				@foreach($input['options'] as $key=>$option)
					<label class="mt-checkbox mt-checkbox-outline"> {{$option->label}}
		                <input type="checkbox" value="{{$option->value}}" @if(isset($input['value'])&&$input['value']==$option->value) checked="checked" @endif name="formRequest[{{$input['id']}}]" />
		                <span></span>
		            </label>
				@endforeach
			@endif
            
        </div>
    </div>
</div>