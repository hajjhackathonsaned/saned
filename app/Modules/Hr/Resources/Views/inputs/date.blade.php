<div class="form-group">
    <label class="control-label col-md-2">{{$input['label']}}<span class="required" aria-required="true"> * </span></label>
    <div class="col-md-10">
		<div class="input-group date form_datetime">
	        <input type="text" size="16"  class="form-control date-picker" value="@if(isset($input['value'])){{$input['value']}}@endif" name="formRequest[{{$input['id']}}]">
	        <!-- <span class="input-group-btn">
	            <button class="btn default date-set" type="button">
	                <i class="fa fa-calendar"></i>
	            </button>
	        </span> -->
	    </div>
    </div>
</div>
@section('page-footer-js')
<script type="text/javascript">
	$(function(){
            $('.date-picker').datepicker();
        });
</script>
<script src = "{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type = "text/javascript" ></script>
<script src = "{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type = "text/javascript" ></script>

@endsection
@section('page-heads')
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@endsection