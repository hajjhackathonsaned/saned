<div class="form-group">
    <label for="exampleInputFile" class="col-md-2 control-label">{{$input['label']}}</label>
    <div class="col-md-10">
        <input type="file" id="exampleInputFile" name="formRequest[{{$input['id']}}]">
        <p class="help-block"> some help text here. </p>
    </div>
</div>