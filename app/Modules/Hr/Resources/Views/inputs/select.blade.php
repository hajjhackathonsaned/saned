
<div class="form-group">
    <label class="control-label col-md-2">{{$input['label']}}<span class="required" aria-required="true"> * </span></label>
    <div class="col-md-10">
    	@if(isset($input['options'])&&!empty($input['options']))
			<select class="form-control input-lg select2-single" name="formRequest[{{$input['id']}}]">
				@foreach($input['options'] as $key=>$option)
					<option value="{{$option->value}}" @if(isset($input['value'])&&$input['value']==$option->value) selected="selected" @endif>{{$option->label}}</option>
				@endforeach
			</select>
		@endif
    </div>
</div>
