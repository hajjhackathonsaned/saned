<div class="form-group">
    <label class="control-label col-md-2">{{$input['label']}}<span class="required" aria-required="true"> * </span></label>
    <div class="col-md-10">
        <input type="text" class="form-control" name="formRequest[{{$input['id']}}]" value="@if(isset($input['value'])){{$input['value']}}@endif"/>
    </div>
</div>