<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{route('dashboard')}}">{{trans('app.home')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    @if(isset($page_breadcrumb))
        @foreach($page_breadcrumb as $bc)
    <li>
        <span>{{$bc['title']}}</span>
        <i class="fa fa-circle"></i>
    </li>
        @endforeach
    @endif
</ul>