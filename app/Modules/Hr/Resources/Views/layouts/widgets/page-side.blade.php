<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="nav-item @if(in_array(app('request')->route()->getName(), ['dashboard'])) active open @endif">
                <a href="{{Route('dashboard')}}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('menu.dashboard')}}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @can('show.setting')
            <li class="nav-item  @if(in_array(app('request')->route()->getName(), ['setting::index'])) active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">{{trans('menu.setting')}}</span>
                    <span class="arrow"></span>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if((in_array(app('request')->route()->getName(), ['setting::index'])) && $group == 'general') active open @endif">
                        <a href="{{Route('setting::index',['group'=>'general'])}}" class="nav-link ">
                            <span class="title">{{trans('menu.general_setting')}}</span>
                        </a>
                    </li>
                    <li class="nav-item @if((in_array(app('request')->route()->getName(), ['setting::index'])) && $group == 'sms') active open @endif">
                        <a href="{{Route('setting::index',['group'=>'sms'])}}" class="nav-link ">
                            <span class="title">{{trans('menu.sms_setting')}}</span>
                        </a>
                    </li>
                    <li class="nav-item @if((in_array(app('request')->route()->getName(), ['setting::index'])) && $group == 'mail') active open @endif">
                        <a href="{{Route('setting::index',['group'=>'mail'])}}" class="nav-link ">
                            <span class="title">{{trans('menu.mail_setting')}}</span>
                        </a>
                    </li>
                    <li class="nav-item @if((in_array(app('request')->route()->getName(), ['setting::index'])) && $group == 'social') active open @endif">
                        <a href="{{Route('setting::index',['group'=>'social'])}}" class="nav-link ">
                            <span class="title">{{trans('menu.social_setting')}}</span>
                        </a>
                    </li>

                </ul>
            </li>
            @endcan
            @can('show.roles')
            <li class="nav-item  @if(in_array(app('request')->route()->getName(), ['user::index','user::add','user::edit','roles::index'])) active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">{{trans('menu.roles')}}</span>
                    <span class="arrow"></span>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    @can('show.roles')
                    <li class="nav-item @if(app('request')->route()->getName()=='roles::index') active open @endif">
                        <a href="{{Route('roles::index')}}" class="nav-link ">
                            <span class="title">{{trans('menu.roles')}}</span>
                        </a>
                    </li>
                    @endcan
                    @can('show.users')
                    <li class="nav-item @if(in_array(app('request')->route()->getName(), ['user::index','user::add','user::edit'])) active open @endif">
                        <a href="{{Route('user::index')}}" class="nav-link ">
                            <span class="title">{{trans('menu.moderators')}}</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            <li class="nav-item  @if(in_array(app('request')->route()->getName(), ['countries::index','cities::index','regions::index'])) active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-grid"></i>
                    <span class="title">{{trans('menu.content')}}</span>
                    <span class="arrow"></span>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    @can('show.countries')
                    <li class="nav-item @if(app('request')->route()->getName()=='countries::index') active open @endif">
                        <a href="{{Route('countries::index')}}" class="nav-link ">
                            <span class="title">{{trans('menu.countries')}}</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::department/index','hr::department/add','hr::department/edit'])) active open @endif">
                <a href="{{Route('hr::department/index')}}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('hr::menu.departments')}}</span>
                </a>
            </li>
            <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::JobTitle/index','hr::JobTitle/add','hr::JobTitle/edit'])) active open @endif">
                <a href="{{Route('hr::JobTitle/index')}}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('hr::menu.jobTitle')}}</span>
                </a>
            </li>
            <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::Employees-List','hr::Employees-Add','hr::Employees-Edit'])) active open @endif">
                <a href="{{Route('hr::Employees-List')}}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('hr::menu.Employess')}}</span>
                </a>
            </li>
            <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::VacationType-List','hr::VacationType-Add','hr::VacationType-Edit'])) active open @endif">
                <a href="{{Route('hr::VacationType-List')}}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('hr::menu.VacationType')}}</span>
                </a>
            </li>
            <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::EmploymentDocument-List','hr::EmploymentDocument-Add','hr::EmploymentDocument-Edit'])) active open @endif">
                <a href="{{Route('hr::EmploymentDocument-List')}}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('hr::menu.EmploymentDocument')}}</span>
                </a>
            </li>
            <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::PerformanceIndicator-List','hr::PerformanceIndicator-Add','hr::PerformanceIndicator-Edit'])) active open @endif">
                <a href="{{Route('hr::PerformanceIndicator-List')}}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('hr::menu.PerformanceIndicator')}}</span>
                </a>
            </li>
            <li class="nav-item  @if(in_array(app('request')->route()->getName(), ['setting::index'])) active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">{{trans('hr::menu.attendance')}}</span>
                    <span class="arrow"></span>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::Attendance-uploadFile'])) active open @endif">
                        <a href="{{Route('hr::Attendance-uploadFile')}}" class="nav-link ">
                            <span class="title">{{trans('hr::menu.attendance-settings')}}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  @if(in_array(app('request')->route()->getName(), ['setting::index'])) active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">{{trans('hr::menu.employee-requests')}}</span>
                    <span class="arrow"></span>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::Request-bulid'])) active open @endif">
                        <a href="{{Route('hr::Request-List')}}" class="nav-link ">
                            <span class="title">{{trans('hr::menu.OtherRequest')}}</span>
                        </a>
                    </li>
                    <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::LoanRequest-List'])) active open @endif">
                        <a href="{{Route('hr::LoanRequest-List')}}" class="nav-link ">
                            <span class="title">{{trans('hr::menu.LoanRequest')}}</span>
                        </a>
                    </li>
                    <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::Complaints-List'])) active open @endif">
                        <a href="{{Route('hr::Complaints-List')}}" class="nav-link ">
                            <span class="title">{{trans('hr::menu.Complaints')}}</span>
                        </a>
                    </li>
                    <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::Recruitments-List'])) active open @endif">
                        <a href="{{Route('hr::Recruitments-List')}}" class="nav-link ">
                            <span class="title">{{trans('hr::menu.Recruitments')}}</span>
                        </a>
                    </li>
                    <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::VacationRequest-List'])) active open @endif">
                        <a href="{{Route('hr::VacationRequest-List')}}" class="nav-link ">
                            <span class="title">{{trans('hr::menu.VacationRequest')}}</span>
                        </a>
                    </li>
                    @if(isset($requestLists)&&!empty($requestLists))
                        @foreach($requestLists as $key=>$value)
                        <li class="nav-item @if(in_array(app('request')->route()->getName(), ['hr::Request-bulid'])) active open @endif">
                            <a href="{{Route('hr::Request-Form',['id'=>$value['id']])}}" class="nav-link ">
                                <span class="title">{{$value['name']}}</span>
                            </a>
                        </li>
                        @endforeach
                    @endif
                </ul>
            </li>

        </ul>
    </div>
</div>