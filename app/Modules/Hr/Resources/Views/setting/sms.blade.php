@extends('layouts.app')

@section('page-site-title')
    {{trans('setting.page_title')}}
@endsection

@section('page-title')
    {{trans('setting.page_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-8">
            <div class="portlet light portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class=" fa fa-send font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">  {{trans('setting.page_title_form_'.$group)}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" id="sms_setting_form" action="{{route('setting::update')}}">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <div class="input-icon">
                                <select id="gateway" name="gateway" class="form-control">
                                    <option @if($gateway == 'Jawaly') selected="selected" @endif value="Jawaly">4Jawaly</option>
                                    <option @if($gateway == 'Infobip') selected="selected" @endif value="Infobip">Infobip</option>
                                    <option @if($gateway == 'Mobilyws') selected="selected" @endif value="Mobilyws">Mobilyws</option>
                                </select>
                                <label for="gateway">{{trans('setting.gateway')}}</label>
                                    <i class="fa fa-bars"></i>
                                </div>
                            </div>
                            {!! csrf_field() !!}
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <div class="input-icon">
                                    <input value="@if(isset($username)){{$username}}@endif"  id="username"  name="username" class="form-control" type="text">
                                    <label for="site_title">{{trans('setting.username')}}</label>
                                    <i class="fa fa-user"></i>
                                </div>
                            </div>

                            <div class="form-group form-md-line-input form-md-floating-label">
                                <div class="input-icon">
                                    <input value="@if(isset($password)){{$password}}@endif"  id="password" name="password" class="form-control" type="password">
                                    <label for="site_title">{{trans('setting.password')}}</label>
                                    <i class="fa fa-compass"></i>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <div class="input-icon">
                                    <input value="@if(isset($sender)){{$sender}}@endif"  id="sender" name="sender" class="form-control" type="text">
                                    <label for="site_title">{{trans('setting.sender')}}</label>
                                    <i class="fa fa-send"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    @can('edit.setting')
                                    <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                                @endcan
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="portlet light portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class=" fa fa-send font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">  {{trans('setting.page_title_form_test')}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                    <input id="number" name="number" class="form-control" type="text">
                                    <label for="number">{{trans('setting.number')}}</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                    <textarea id="message" name="message" class="form-control"></textarea>
                                    <label for="message">{{trans('setting.message')}}</label>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                        <button  class="btn btn-info" id="test_send" data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{trans('setting.send_loading')}}"><i class="fa fa-send"></i> {{trans('setting.send')}}</button>
                                        <button  class="btn btn-warning" id="get_balance" data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{trans('setting.balance_loading')}}"><i class="fa fa-dollar"></i> {{trans('setting.balance')}}</button>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class=" fa fa-envelope font-blue"></i>
                        <span class="caption-subject font-blue sbold uppercase">  {{trans('setting.page_title_form_template')}}</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only blue" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only green" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only red" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" id="sms_setting_form" action="{{route('setting::update-template')}}">
                        <div class="form-body">
                            {!! csrf_field() !!}
                            <div class="row">
                            @foreach($templates as $row)

                                <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                            <textarea id="message" name="{{$row->key_id}}" class="form-control">{{$row->message}}</textarea>
                                            <label for="message">{{trans('setting.'.$row->key_id)}}</label>
                                </div>
                                </div>

                            @endforeach
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    @can('edit.setting')
                                        <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-footer-js')
    <script type="application/javascript">
        $("#test_send").click(function () {
            if (!$('#message').val() && !$('#number').val()) {
                showNotify('error', js_lang.message_empty);
                return false;
            }else{
                var my_data = $('#sms_setting_form').serialize()+'&message='+$('#message').val()+'&number='+$('#number').val();
                $('#test_send').button('loading');
                $.post('{{route("setting::test_send")}}', my_data, function (data) {
                    $('#test_send').button('reset');
                    if (data.status) {
                        showNotify('success', data.message);
                    } else {
                        showNotify('error', data.message);
                    }
                }, 'json');
            }
        });

        $("#get_balance").click(function () {
                var my_data = $('#sms_setting_form').serialize();
                $('#get_balance').button('loading');
                $.post('{{route("setting::get_balance")}}', my_data, function (data) {
                    $('#get_balance').button('reset');
                    if (data.status) {
                        showNotify('success', js_lang.balance+' '+data.response);
                    } else {
                        showNotify('error', data.message);
                    }
                }, 'json');
        });

    </script>
@endsection

@section('page-heads')
@endsection