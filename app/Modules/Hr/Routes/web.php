<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'hr'], function () {
    Route::get('/', function () {
        dd('This is the hr module index page. Build something great!');
    });
});
$this->group(['prefix' => 'hr', 'middleware' => ['auth']], function () {
    $this->get('login', ['as' => 'hr-auth-login-form', 'uses' => 'Auth\LoginController@showLoginForm']);
    $this->get('auth/login', ['as' => 'hr-auth-login-action', 'uses' => 'Auth\LoginController@login']);
    $this->post('auth/login', ['as' => 'hr-auth-login-action', 'uses' => 'Auth\LoginController@login']);
    $this->get('auth/logout', ['as' => 'hr-auth-logout-action', 'uses' => 'Auth\LoginController@logout']);

    // $this->get('/login', 'LoginController@showLoginForm')->name('hr-auth-login-form');
    // $this->get('auth/login', 'Auth\LoginController@login')->name('hr-auth-login-action');
    // $this->post('auth/login', 'Auth\LoginController@login')->name('hr-auth-login-action');
    // $this->get('auth/logout', 'Auth\LoginController@logout')->name('hr-auth-logout-action');
});

$this->group(['prefix' => 'hr', 'middleware' => ['auth'], 'as' => 'hr::'], function () {
	// department section
    $this->get('department/index', ['as' => 'department/index', 'uses' => 'DepartmentController@index']);
    $this->get('department/add', ['as' => 'department/add', 'uses' => 'DepartmentController@add']);
    $this->get('department/edit/{id}', ['as' => 'department/edit', 'uses' => 'DepartmentController@edit']);
    $this->post('department/getTableData/', ['as' => 'department-get-table-data', 'uses' => 'DepartmentController@getData']);
    $this->post('department/create', ['as' => 'department/create', 'uses' => 'DepartmentController@create']);
    $this->post('department/delete', ['as' => 'department/delete', 'uses' => 'DepartmentController@delete']);
    $this->post('department/deleteMulti', ['as' => 'department/delete-multi', 'uses' => 'DepartmentController@deleteAll']);
    $this->post('department/update', ['as' => 'department/update', 'uses' => 'DepartmentController@update']);

	// department JobTitle
    $this->get('JobTitle/index', ['as' => 'JobTitle/index', 'uses' => 'JobTitleController@index']);
    $this->get('JobTitle/add', ['as' => 'JobTitle/add', 'uses' => 'JobTitleController@add']);
    $this->get('JobTitle/edit/{id}', ['as' => 'JobTitle/edit', 'uses' => 'JobTitleController@edit']);
    $this->post('JobTitle/getTableData/', ['as' => 'JobTitle-get-table-data', 'uses' => 'JobTitleController@getData']);
    $this->post('JobTitle/create', ['as' => 'JobTitle/create', 'uses' => 'JobTitleController@create']);
    $this->post('JobTitleJobTitle/delete', ['as' => 'JobTitle/delete', 'uses' => 'JobTitleController@delete']);
    $this->post('JobTitle/deleteMulti', ['as' => 'JobTitle/delete-multi', 'uses' => 'JobTitleController@deleteAll']);
    $this->post('JobTitle/update', ['as' => 'JobTitle/update', 'uses' => 'JobTitleController@update']);




});
//Employess
$this->group(['prefix' => 'hr/employees', 'middleware' => ['auth'], 'as' => 'hr::Employees'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'EmployeeController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'EmployeeController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'EmployeeController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'EmployeeController@create']);
    $this->get('edit/{id}', ['as' => '-Edit', 'uses' => 'EmployeeController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'EmployeeController@update']);
    $this->post('/delete', ['as' => '-Delete', 'uses' => 'EmployeeController@delete']);
    $this->post('/deleteMulti', ['as' => '-DeleteMulti', 'uses' => 'EmployeeController@deleteAll']);
    $this->post('/changeJobTitle', ['as' => '-changeJobTitle', 'uses' => 'EmployeeController@changeJobTitle']);

});
//VacationType
$this->group(['prefix' => 'hr/VacationType', 'middleware' => ['auth'], 'as' => 'hr::VacationType'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'VacationTypeController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'VacationTypeController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'VacationTypeController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'VacationTypeController@create']);
    $this->get('edit/{id}', ['as' => '-Edit', 'uses' => 'VacationTypeController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'VacationTypeController@update']);
    $this->post('/delete', ['as' => '-Delete', 'uses' => 'VacationTypeController@delete']);
    $this->post('/deleteMulti', ['as' => '-DeleteMulti', 'uses' => 'VacationTypeController@deleteAll']);
});
//EmploymentDocument
$this->group(['prefix' => 'hr/EmploymentDocument', 'middleware' => ['auth'], 'as' => 'hr::EmploymentDocument'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'EmploymentDocumentController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'EmploymentDocumentController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'EmploymentDocumentController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'EmploymentDocumentController@create']);
    $this->get('edit/{id}', ['as' => '-Edit', 'uses' => 'EmploymentDocumentController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'EmploymentDocumentController@update']);
    $this->post('/delete', ['as' => '-Delete', 'uses' => 'EmploymentDocumentController@delete']);
    $this->post('/deleteMulti', ['as' => '-DeleteMulti', 'uses' => 'EmploymentDocumentController@deleteAll']);
});
//PerformanceIndicator
$this->group(['prefix' => 'hr/PerformanceIndicator', 'middleware' => ['auth'], 'as' => 'hr::PerformanceIndicator'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'PerformanceIndicatorController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'PerformanceIndicatorController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'PerformanceIndicatorController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'PerformanceIndicatorController@create']);
    $this->get('edit/{id}', ['as' => '-Edit', 'uses' => 'PerformanceIndicatorController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'PerformanceIndicatorController@update']);
    $this->post('/delete', ['as' => '-Delete', 'uses' => 'PerformanceIndicatorController@delete']);
    $this->post('/deleteMulti', ['as' => '-DeleteMulti', 'uses' => 'PerformanceIndicatorController@deleteAll']);
});
//other request
$this->group(['prefix' => 'hr/Request', 'middleware' => ['auth'], 'as' => 'hr::Request'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'RequestController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'RequestController@getData']);
    $this->get('/build', ['as' => '-Build', 'uses' => 'RequestController@build']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'RequestController@create']);
    $this->get('edit/{id}', ['as' => '-Edit', 'uses' => 'RequestController@edit']);
    $this->get('form/{id}', ['as' => '-AddForm', 'uses' => 'RequestController@addForm']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'RequestController@update']);
    $this->post('/delete', ['as' => '-Delete', 'uses' => 'RequestController@delete']);
    $this->post('/deleteMulti', ['as' => '-DeleteMulti', 'uses' => 'RequestController@deleteAll']);
    $this->get('forms/{form_id}', ['as' => '-Form', 'uses' => 'RequestController@forms']);
    $this->post('/saveForm', ['as' => '-saveForm', 'uses' => 'RequestController@saveForm']);
    $this->get('/formIndex/{id}', ['as' => '-ListForm', 'uses' => 'RequestController@formIndex']);
    $this->post('/getFormTableData/', ['as' => '-get-form-table-data', 'uses' => 'RequestController@getFormTableData']);
    $this->get('EditForm/{id}', ['as' => '-EditForm', 'uses' => 'RequestController@EditForm']);
    $this->post('/DeleteForm', ['as' => '-DeleteForm', 'uses' => 'RequestController@DeleteForm']);
    $this->post('/formDeleteMulti', ['as' => '-formDeleteMulti', 'uses' => 'RequestController@formDeleteMulti']);
    $this->post('/updateForm', ['as' => '-updateForm', 'uses' => 'RequestController@updateForm']);
});
//LoansRequest
$this->group(['prefix' => 'hr/LoanRequest', 'middleware' => ['auth'], 'as' => 'hr::LoanRequest'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'LoanRequestController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'LoanRequestController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'LoanRequestController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'LoanRequestController@create']);
    $this->get('edit/{id}', ['as' => '/edit', 'uses' => 'LoanRequestController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'LoanRequestController@update']);
    $this->post('/delete', ['as' => '/delete', 'uses' => 'LoanRequestController@delete']);
    $this->post('/deleteMulti', ['as' => '/delete-multi', 'uses' => 'LoanRequestController@deleteAll']);
});
//Complaints & Suggestions
$this->group(['prefix' => 'hr/Complaints', 'middleware' => ['auth'], 'as' => 'hr::Complaints'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'ComplaintsController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'ComplaintsController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'ComplaintsController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'ComplaintsController@create']);
    $this->get('edit/{id}', ['as' => '/edit', 'uses' => 'ComplaintsController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'ComplaintsController@update']);
    $this->post('/delete', ['as' => '/delete', 'uses' => 'ComplaintsController@delete']);
    $this->post('/deleteMulti', ['as' => '/delete-multi', 'uses' => 'ComplaintsController@deleteAll']);
});
//Recruitments
$this->group(['prefix' => 'hr/Recruitments', 'middleware' => ['auth'], 'as' => 'hr::Recruitments'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'RecruitmentsController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'RecruitmentsController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'RecruitmentsController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'RecruitmentsController@create']);
    $this->get('edit/{id}', ['as' => '/edit', 'uses' => 'RecruitmentsController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'RecruitmentsController@update']);
    $this->post('/delete', ['as' => '/delete', 'uses' => 'RecruitmentsController@delete']);
    $this->post('/deleteMulti', ['as' => '/delete-multi', 'uses' => 'RecruitmentsController@deleteAll']);
});
//VacationRequest
$this->group(['prefix' => 'hr/VacationRequest', 'middleware' => ['auth'], 'as' => 'hr::VacationRequest'], function () {
    $this->get('/index', ['as' => '-List', 'uses' => 'VacationRequestController@index']);
    $this->post('/getTableData/', ['as' => '-get-table-data', 'uses' => 'VacationRequestController@getData']);
    $this->get('/add', ['as' => '-Add', 'uses' => 'VacationRequestController@add']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'VacationRequestController@create']);
    $this->get('edit/{id}', ['as' => '/edit', 'uses' => 'VacationRequestController@edit']);
    $this->post('/update', ['as' => '-Update', 'uses' => 'VacationRequestController@update']);
    $this->post('/delete', ['as' => '/delete', 'uses' => 'VacationRequestController@delete']);
    $this->post('/deleteMulti', ['as' => '/delete-multi', 'uses' => 'VacationRequestController@deleteAll']);
});
//Attendance
$this->group(['prefix' => 'hr/Attendance', 'middleware' => ['auth'], 'as' => 'hr::Attendance'], function () {
    $this->get('/uploadFile', ['as' => '-uploadFile', 'uses' => 'AttendanceController@uploadFile']);
    $this->post('/create', ['as' => '-Create', 'uses' => 'AttendanceController@create']);

});