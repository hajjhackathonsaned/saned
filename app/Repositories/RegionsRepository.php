<?php

namespace App\Repositories;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;


class RegionsRepository extends Repository
{

    public function model()
    {
        return '\App\Models\Regions';
    }
    /**
     * @param $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function add($request){
        $data=[];
        $data['status']=1;
        $data['city_id']=$request->input('city');
        // set data language
        $translate=[];
        foreach (config('app.supported_lang') as $lang){
            $translate[]=[
                'column_name'=>'title',
                'locale'=>$lang,
                'value'=>$request->input('title')[$lang],
            ];
        }
        try{
            \DB::beginTransaction();
            $add = $this->create($data); //begin insert question data
            if (!$add) {
                \DB::rollback();
                return [false, 'regions_add_error', $add];
            }
            // start to insert question locale translation
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_add_regions', $add];
        }catch (\PDOException $ex){
            dd($ex);
            \DB::rollback();
            return [false,'database_error'];
        }
    }
    public function updateRow($request){
        $data=[];
        $data['city_id']=$request->input('city');
        // set data language
        $translate=[];
        foreach (config('app.supported_lang') as $lang){
            $translate[]=[
                'column_name'=>'title',
                'locale'=>$lang,
                'value'=>$request->input('title')[$lang],
            ];
        }
        try{
            \DB::beginTransaction();
            $update = $this->update($data,$request->input('id')); //begin insert question data
            if (!$update) {
                \DB::rollback();
                return [false, 'regions_update_error', $update];
            }
            // start to insert question locale translation
            $add = $this->find($request->input('id'));
            $add->translate()->delete();
            $translate=$add->translate()->createMany($translate);
            if(!$translate){
                \DB::rollback();
                return [false, 'translation_add_error', $add];
            }
            \DB::commit();
            return [true, 'success_update_regions', $add];
        }catch (\PDOException $ex){
            \DB::rollback();
            return [false,'database_error'];
        }
    }

    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    public function deleteAll(array $data ,$attribute="id") {
        return $this->model->whereIn($attribute,$data)->delete();
    }
}
