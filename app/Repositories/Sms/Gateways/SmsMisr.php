<?php

namespace App\Repositories\Sms\Gateways;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

class SmsMisr implements GatewayInterface
{

    const SUCCESS_CODES = [1901];
    const CREDIT_SUCCESS_CODES = [117];

    private static $instance;
    private $message;
    private $numbers;
    private $sender;
    private $username;
    private $password;

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function setUser($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
        return $this;
    }

    public function setSender($sender)
    {
        $this->sender = trim($sender);
        return $this;
    }

    public function setNumbers(array $numbers)
    {
        $this->numbers = implode(',', $numbers);
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = trim($message);
        return $this;
    }

    public function send()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://www.smsmisr.com/'
        ]);
        try {
            $response = $client->post('api/send', ['form_params' => [
                    'Username' => $this->username,
                    'password' => $this->password,
                    'sender' => $this->sender,
                    'message' => $this->message,
                    'Mobile' => $this->numbers,
                    'language' => 1
                ]
            ]);
            $jsonResponse = explode(',',$response->getBody());
            $status = $jsonResponse[0];
            if (in_array($status, self::SUCCESS_CODES)) {
                return ['status' => true, 'response' => $jsonResponse];
            }
            return ['status' => false, 'response' => $jsonResponse];
        } catch (RequestException $ex) {
            return ['status' => false, 'response' => $ex->getResponse()->getBody() !== null ? $ex->getResponse()->getBody() : $ex->getMessage()];
        } catch (ClientException $ex) {
            return ['status' => false, 'response' => $ex->getResponse()->getBody() !== null ? $ex->getResponse()->getBody() : $ex->getMessage()];
        }
    }

    public function getBalance()
    {

    }

    /**
     *
     */
    protected function __construct()
    {
        
    }

    /**
     *
     */
    private function __clone()
    {
        
    }

    /**
     *
     */
    private function __wakeup()
    {
        
    }

}
