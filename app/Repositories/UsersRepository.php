<?php

namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use App\Repositories\RolesRepository;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Helpers\Functions;

/**
 * Class UsersRepository
 * @package App\Core\Repositories
 */
class UsersRepository extends Repository {

    use Functions;

    /**
     * @return string
     */
    public function model() {
        return '\App\Models\User';
    }

    public function delete($id) {
        return $this->model->where('id', $id)->delete();
    }

    public function add($request) {
        $role = $request->input('role');

        try {
            \DB::beginTransaction();

            //encrypt password
            $password = bcrypt($request->input('password'));

            //insert user data
            $data = ['name' => $request->input('name'), 'email' => $request->input('email'), 'mobile' => $request->input('mobile'), 'password' => $password];

            //check for uploaded image
            if ($request->hasFile('image')) {
                $upload = $this->uploadImage($request->file('image'), 'image', 'users');
                if (!$upload[0]) {
                    return [false, trans('common.' . $upload[1])];
                }
                $data['image'] = $upload[1];
            }

            $user = $this->create($data);

            $add = $user->assignRole($role);
            if (!$add) {
                \DB::rollback();
                return [false, 'user_add_error', $add];
            }
            \DB::commit();
            return Response()->json([true, 'success_add_user', $add]);
        } catch (\PDOException $ex) {
            dd($ex);
            \DB::rollback();
            return [false, 'database_error'];
        }
    }

    public function updateUser($request) {
        $user = $this->find($request->input('id'));
        $role = $request->input('role');
        $password = $user->password;

        if ($request->input('password') != NULL) {
            $password = bcrypt($request->input('password'));
        } else {
            $password = $password;
        }
        try {
            \DB::beginTransaction();

            //update data
            $data = ['name' => $request->input('name'), 'email' => $request->input('email'), 'mobile' => $request->input('mobile'), 'password' => $password];

            //check for uploaded image
            if ($request->hasFile('image')) {
                $upload = $this->uploadImage($request->file('image'), 'image', 'users');
                //dd($upload);
                if (!$upload[0]) {
                    return [false, trans('common.' . $upload[1])];
                }
                $data['image'] = $upload[1];
            } else {
                $data['image'] = $user->image;
            }
            $this->update($data, $request->input('id'));

            $add = $user->syncRoles($role);

            if (!$add) {
                \DB::rollback();
                return [false, 'user_edit_error', $add];
            }
            \DB::commit();
            return Response()->json([true, 'success_edit_user', $add]);
        } catch (\PDOException $ex) {
            \DB::rollback();
            return [false, 'database_error'];
        }
    }

}
