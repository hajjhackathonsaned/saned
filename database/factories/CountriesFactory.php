<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Countries::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->code,
        'prefix' => $faker->unique()->prefix,
        'currency' => $faker->unique()->currency, // secret
        'status' => $faker->status,
    ];
});

$factory->define(App\Models\Translate::class, function (Faker $faker) {
    foreach (config('app.supported_lang') as $lang) {
        return [
            'translatable_type' => 'App\Models\Countries',
            'column_name' => 'title',
            'translatable_id' => $faker->numberBetween($min = 1, $max = 50),
            'locale' => $lang,
            'value' => $faker->value,
        ];
    }
});
