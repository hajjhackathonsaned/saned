<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //app settings
        \App\Models\Setting::create(['key_id'=>'site_title','value'=>'Dawa2','set_group'=>'general']);
        \App\Models\Setting::create(['key_id'=>'phone','value'=>'+0123456789','set_group'=>'general']);
        \App\Models\Setting::create(['key_id'=>'email','value'=>'info@dwaa.com','set_group'=>'general']);
        \App\Models\Setting::create(['key_id'=>'version','value'=>'4','set_group'=>'general']);
        \App\Models\Setting::create(['key_id'=>'copy_right','value'=>'All copy right reserved','set_group'=>'general']);
        \App\Models\Setting::create(['key_id'=>'close_site','value'=>'0','set_group'=>'general']);
        //sms settings
        \App\Models\Setting::create(['key_id'=>'username','value'=>'username','set_group'=>'sms']);
        \App\Models\Setting::create(['key_id'=>'password','value'=>'password','set_group'=>'sms']);
        \App\Models\Setting::create(['key_id'=>'sender','value'=>'test','set_group'=>'sms']);
        \App\Models\Setting::create(['key_id'=>'gateway','value'=>'Jawaly','set_group'=>'sms']);
        //social settings
        \App\Models\Setting::create(['key_id'=>'facebook','value'=>'http://facebook.com','set_group'=>'social']);
        \App\Models\Setting::create(['key_id'=>'twitter','value'=>'http://twitter.com','set_group'=>'social']);
        \App\Models\Setting::create(['key_id'=>'youtube','value'=>'http://youtube.com','set_group'=>'social']);
        \App\Models\Setting::create(['key_id'=>'instagram','value'=>'http://instagram.com','set_group'=>'social']);
        //mail settings
        \App\Models\Setting::create(['key_id'=>'mail_driver','value'=>'mail','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_smtp_host','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_smtp_port','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_smtp_username','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_smtp_password','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_smtp_encryption','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_ses_key','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_ses_secret','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_mailgun_domain','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_mailgun_secret','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_mandrill_secret','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_sendmail_path','value'=>'','set_group'=>'mail']);
        \App\Models\Setting::create(['key_id'=>'mail_sparkpost_secret','value'=>'test','set_group'=>'mail']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
