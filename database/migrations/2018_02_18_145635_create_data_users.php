<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateDataUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $modules=['roles','users','setting','countries','cities','regions'];
        $role=Role::create(['name' => 'superAdmin']);
        //add permissions
        foreach ($modules as $item){
            Permission::create(['name'=>'add.'.$item,'guard_name'=>'web']);
            Permission::create(['name'=>'delete.'.$item,'guard_name'=>'web']);
            Permission::create(['name'=>'edit.'.$item,'guard_name'=>'web']);
            Permission::create(['name'=>'show.'.$item,'guard_name'=>'web']);
        }
        //role assign permission
        $role->syncPermissions(['add.roles','delete.roles','edit.roles','show.roles']);
        $user=\App\Models\User::create(['name'=>'admin','email'=>'admin@admin.net','password'=>\Hash::make('123123'),'mobile'=>'201009686377']);
        $user->assignRole('superAdmin');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
