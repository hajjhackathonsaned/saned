<?php

use Illuminate\Database\Seeder;
use App\Models\Countries;
use App\Repositories\CountriesRepository;

class CountriesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
    
        factory(App\Models\Countries::class, 50);
        factory(App\Models\Translate::class, 50);
    }

}
