<?php

use Illuminate\Database\Seeder;

class TranslateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Translate::class, 50);
    }
}
