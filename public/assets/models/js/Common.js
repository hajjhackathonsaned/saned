String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ajaxStart(function () {
    App.startPageLoading();
});
$(document).ajaxStop(function () {
    App.stopPageLoading();
    Layout.fixContentHeight(); // fix content height
    App.initAjax(); // initialize core stuff
});

jQuery('#table_view #table_checkall').change(function () {
    var set = $(this).attr("data-set");
    if ($(this).is(":checked")) {
        $(set).prop("checked", true);
        $(set).parent().addClass("checked");
    } else {
        $(set).prop("checked", false);
        $(set).parent().removeClass("checked");
    }
});

function showNotify(type, message) {
    $(".toast").hide();
    toastr.options.preventDuplicates = true;
    switch (type) {
        case "error":
            toastr.error(message);
            break;
        case "success":
            toastr.success(message);
            break;
        case "info":
            toastr.info(message);
            break;
    }
}

var dialog_is = {
    title: '',
    message: '',
    buttons: {
        success: {
            label: js_lang.save,
            className: "btn-success",
            callback: function () {
                return false;
            }
        }, danger: {
            label: js_lang.close,
            className: "btn-danger",
            callback: function () {
                bootbox.hideAll();
            }
        }
    }
};
bootbox.addLocale('ar', {
    OK: js_lang.yes,
    CANCEL: js_lang.cancel,
    CONFIRM: js_lang.CONFIRM
});

bootbox.setLocale('ar');

(function( $ ){
    $.fn.GetDataTable = function(options) {
        dataTable = $(this).DataTable({
            "language": {
                "sLengthMenu": js_lang.sLengthMenu + " _MENU_",
                "sZeroRecords": js_lang.sZeroRecords,
                "sInfo": js_lang.show + " _START_ " + js_lang.to + " _END_ " + js_lang.from_source + " _TOTAL_ " + js_lang.entry,
                "sInfoEmpty": js_lang.sInfoEmpty,
                "sInfoFiltered": "(" + js_lang.result_out + " _MAX_ " + js_lang.entry + ")",
                "sInfoPostFix": "",
                "sSearch": js_lang.search,
                "sUrl": "",
                "oPaginate": {
                    "sFirst": js_lang.sFirst,
                    "sPrevious": js_lang.sPrevious,
                    "sNext": js_lang.sNext,
                    "sLast": js_lang.sLast
                }
            },
            "pagingType": "bootstrap_full_number",
            //"processing": true,
            'fnDrawCallback': function ()
            {
                $('#table_view').show();
            },
            "serverSide": true,
            "ajax": {
                type: "POST",
                url: options.route
            },
            responsive: {
                details: {
                    type: 'column',
                }
            },
            "autoWidth": false,
            "searching": options.searching,
            "columns": options.fields,
            "lengthMenu": [
                [100, 200, 500, 1000],
                [100, 200, 500, 1000] // change per page values here

            ],
            "pageLength": options.paginate,
            "order": [[options.order, 'desc']],
        });

        $(this).on("m-datatable--on-layout-updated", function(){
            $(this).find(".m-loader").remove();
        });
    };
})( jQuery );

