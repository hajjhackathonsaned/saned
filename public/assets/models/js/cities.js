var users_info = {};
var form1 = $('#cities_form');
$('#create_btn').click(function () {
    $(form1)[0].reset();
    $(form1).find('input[name=id]').val('');
    $('#admins-form-modal').modal('show');
});
/**
 * datatable ajax
 * @type {jQuery}
 */
$('#table_view').GetDataTable({
    route: urls.get_data_url + '?country=' + country,
    order: 4,
    paginate: 50,
    searching: false,
    fields: [
        {
            "orderable": false,
            "data": function (row, type, val, meta) {
                return '<div style="width: 100%;text-align: center;"><input type="checkbox" name="ids[]" class="checkboxes" value="' + row.id + '" title="" /></div>';
            }, width: '1%'
        },
        {
            "orderable": false,
            "data": function (row, type, val, meta) {
                return (row.title) ? row.title.value : '';
            }
        },
     
        {"data": "country", "orderable": false},
        {"data": function (row, type, val, meta) {
                if (row.status == 1) {
                    return '<a onclick="updateStatus(' + row.id + ',0)"><span class="badge badge-success"><i class="icon-check"></i></span></a>';
                } else if (row.status == 0) {
                    return '<a onclick="updateStatus(' + row.id + ',1)"><span class="badge badge-danger"> <i class="icon-close"></i> </span></a>';
                } else {
                    return '';
                }
            }, "orderable": false
        },
        {"data": "created_at", "orderable": false},
        {
            "orderable": false,
            "data": function (row, type, val, meta) {
                users_info[row.id] = row;
                var html = '<div style="width: 100%;text-align: center;">';
                if (permissions.edit) {
                    html += '<a title="' + js_lang.edit_record + '" class="btn btn-sm blue" onclick="editRow(' + row.id + ',' + row.country_id + ')"><i class="fa fa-edit"></i> ' + js_lang.edit_record + '</a>';
                }
                if (permissions.delete) {
                    html += '<a title="' + js_lang.delete_record + '" class="btn btn-sm red" onclick="deleteRow(' + row.id + ')"><i class="fa fa-trash-o"></i> ' + js_lang.delete_record + '</a>';
                }
                if (permissions.show_regions) {
                    html += '<a title="' + model_js_lang.regions + '" href="' + urls.add_region + '?city=' + row.id + '" class="btn btn-sm green"><i class="fa fa fa-building"></i> ' + model_js_lang.regions + '</a>';
                }
                html += '</div>';
                return html;
            }, "width": "40%"
        }
    ]
});
/**
 *
 * @param id
 */
function deleteRow(id) {
    bootbox.confirm(js_lang.confirm_delete, function (result) {
        if (result) {
            var my_data = {};
            my_data.id = id;

            //my_data._token = token_code;
            $.post(urls.delete_url, my_data, function (data) {
                if (data[0]) {
                    showNotify('success', js_lang[data[1]]);
                } else {
                    if (js_lang[data[1]]) {
                        showNotify('error', js_lang[data[1]]);
                    } else {
                        showNotify('error', model_js_lang[data[1]]);
                    }
                }
                dataTable.ajax.reload();
            }, 'json');
        }
    });
}

/**
 *
 * @param id
 */
function editRow(id, country)
{
    var data = users_info[id];
    $(form1).find('input[name=id]').val(data.id);
    $(form1).find('input[name=country]').val(country);
    $.each(data.translate, function (key, val) {
        $(form1).find("input[name='title[" + val.locale + "]']").val(val.value);
    });
    $('#admins-form-modal').modal('show');
}
/**
 *
 * @param id
 * @param type
 */
function updateStatus(id, type) {
    bootbox.confirm(model_js_lang.update_status_confirm, function (result) {
        if (result) {
            var my_data = {};
            my_data.id = id;
            if (type == 0) {
                my_data.status = 0;
            } else {
                my_data.status = 1;
            }
            $.post(urls.update_status, my_data, function (data) {
                if (data[0]) {
                    showNotify('success', model_js_lang[data[1]]);
                } else {
                    if (js_lang[data[1]]) {
                        showNotify('error', js_lang[data[1]]);
                    } else {
                        showNotify('error', model_js_lang[data[1]]);
                    }
                }
                dataTable.ajax.reload();
            }, 'json');
        }
    });
}

$("#data_delete_btn").click(function () {
    if ($('input[name="ids[]"]:checked').length) {
        bootbox.confirm(js_lang.confirm_delete, function (result) {
            if (result) {
                var ids = [];
                $.each($('table#table_view tbody input[type="checkbox"]:checked'), function (x, y) {
                    ids.push($(y).val());
                });
                var my_data = {};
                my_data.ids = ids;
                $.post(urls.delete_multi_url, my_data, function (data) {
                    dataTable.ajax.reload();
                    bootbox.hideAll();
                    if (data[0]) {
                        showNotify('success', js_lang[data[1]]);
                    } else {
                        if (js_lang[data[1]]) {
                            showNotify('error', js_lang[data[1]]);
                        } else {
                            showNotify('error', model_js_lang[data[1]]);
                        }
                    }
                }, 'json');
            }
        });
    } else {
        showNotify('error', js_lang.box_message);
        return false;
    }
});
var error1 = $('.alert-danger', form1);
form1.validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules: {
        'title[ar]': {
            required: true
        },
        'title[en]': {
            required: true
        },
    },
    messages: {// custom messages for radio buttons and checkboxes
        'title[ar]': {
            required: js_lang.required
        },
        'title[en]': {
            required: js_lang.required
        },
    },
    invalidHandler: function (event, validator) { //display error alert on form submit
        error1.show();
        $('#admins-form-modal').animate({scrollTop: 0}, 600);
    },
    highlight: function (element) { // hightlight error inputs
        $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },
    success: function (label) {
        label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function (form) {
        error1.hide();
        if ($(form).find('input[name=id]').val() == '') {
            var url = urls.save_url;
        } else {
            var url = urls.update_url;
        }
        var formData = new FormData(form1[0]);
        $('#add_admins_btn').button('loading');
        $.ajax({
            url: url,
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            error: function (response) {
                $('#add_admins_btn').button('reset');
                try {
                    if (response.status == 422) {
                        var message = '<ul>';
                        $.each(response.responseJSON.errors, function (key, err) {
                            $.each(err, function (k, val) {
                                message += '<li>' + val + '</li>';
                            });
                        });
                        message += '</ul>';
                        showNotify('error', message);
                    } else {
                        showNotify('error', js_lang.connection_error);
                    }
                } catch (error) {
                    showNotify('error', js_lang.connection_error);
                }
            },
            success: function (data) {
                $('#add_admins_btn').button('reset');
                if (data[0] == true) {
                    $(form)[0].reset();
                    $('#admins-form-modal').modal('hide');
                    showNotify('success', model_js_lang[data[1]]);
                    dataTable.ajax.reload();
                } else {
                    $('#add_admins_btn').button('reset');
                    var message = '<ul>';
                    if ($.isArray(data[1])) {
                        $.each(data[1], function (key, err) {
                            message += '<li>' + model_js_lang[err] + '</li>';
                        });
                    } else {
                        message = '<li>' + model_js_lang[data[1]] + '</li>';
                    }
                    message += '</ul>';
                    showNotify('error', message);
                }
            }
        });
        return false;
    }
});