var users_info = {};
var form1 = $('#users_form');
var error1 = $('.alert-danger', form1);
form1.validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules: {
        'name': {
            required: true
        },
        email: {
            required: true,
            email: true
        },
        mobile: {
            required: true,
            minlength: 8,
            digits: true
        },
        image: {
            accept: "jpg|jpeg|png|JPG|JPEG|PNG",
        },        
        password: {
            minlength: 8
        },
        password_confirmation: {
            minlength: 8,
            equalTo: "#password"
        },
        "role[]": "required"       
  
    },
    messages: {// custom messages for radio buttons and checkboxes
        'name': {
            required: js_lang.required
        },
        'email': {
            required: js_lang.email_required
        },
        'mobile': {
            required: js_lang.required,
            digits: js_lang.mobile_required,
            minlength: js_lang.mobile_required
        },
        'image': {
            accept: js_lang.correct_image
        },        
        'password': {
            minlength: js_lang.minlength_password
        },        
         'password_confirmation': {
            minlength: js_lang.minlength_password,
            equalTo: js_lang.equalTo_pass
        }, 
        'role[]': {
            required: js_lang.required
        },        

    },
    invalidHandler: function (event, validator) { //display error alert on form submit
        error1.show();
        $('#admins-form-modal').animate({scrollTop: 0}, 600);
    },
    highlight: function (element) { // hightlight error inputs
        $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },
    success: function (label) {
        label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function (form) {
        form.submit();
    }
});
/**
 * datatable ajax
 * @type {jQuery}
 */
$('#table_view').GetDataTable({
    route: urls.get_data_url,
    order: 3,
    paginate: 50,
    searching: true,
    fields: [
        {"data": "name", "orderable": true},
        {"data": "email", "orderable": true},
        {"data": "mobile", "orderable": true},
        {"data": "created_at", "orderable": true},
        {
            "orderable": false,
            "data": function (row, type, val, meta) {
                users_info[row.id] = row;
                var html = '<div style="width: 100%;text-align: center;">';
                //if (permissions.edit) {
                html += '<a title="' + js_lang.edit_record + '" href="' + urls.edit_url + '/' + row.id + ' " class="btn btn-sm blue"><i class="fa fa-edit"></i> ' + js_lang.edit_record + '</a>';
                //}
                //if (permissions.delete) {
                html += '<a title="' + js_lang.delete_record + '" class="btn btn-sm red" onclick="deleteRow(' + row.id + ')"><i class="fa fa-trash-o"></i> ' + js_lang.delete_record + '</a>';
                //}
                html += '</div>';
                return html;
            }, "width": "30%"
        }
    ]
});
/**
 *
 * @param id
 */
function deleteRow(id) {
    bootbox.confirm(js_lang.confirm_delete, function (result) {
        if (result) {
            var my_data = {};
            my_data.id = id;
            //my_data._token = token_code;
            $.post(urls.delete_url, my_data, function (data) {
                if (data[0]) {
                    showNotify('success', js_lang[data[1]]);
                } else {
                    if (js_lang[data[1]]) {
                        showNotify('error', js_lang[data[1]]);
                    } else {
                        showNotify('error', model_js_lang[data[1]]);
                    }
                }
                dataTable.ajax.reload();
            }, 'json');
        }
    });
}


/**
 *
 * @param id
 * @param type
 */

$("#data_delete_btn").click(function () {
    if ($('input[name="ids[]"]:checked').length) {
        bootbox.confirm(js_lang.confirm_delete, function (result) {
            if (result) {
                var ids = [];
                $.each($('table#table_view tbody input[type="checkbox"]:checked'), function (x, y) {
                    ids.push($(y).val());
                });
                var my_data = {};
                my_data.ids = ids;
                $.post(urls.delete_multi_url, my_data, function (data) {
                    dataTable.ajax.reload();
                    bootbox.hideAll();
                    if (data[0]) {
                        showNotify('success', js_lang[data[1]]);
                    } else {
                        if (js_lang[data[1]]) {
                            showNotify('error', js_lang[data[1]]);
                        } else {
                            showNotify('error', model_js_lang[data[1]]);
                        }
                    }
                }, 'json');
            }
        });
    } else {
        showNotify('error', js_lang.box_message);
        return false;
    }
});



