var users_info = {};
var form1 = $('#users_form');
var error1 = $('.alert-danger', form1);
form1.validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules: {
        'name': {
            required: true
        },
        email: {
            required: true,
            email: true
        },
        mobile: {
            required: true,
            minlength: 8,
            digits: true
        },
        image: {
            accept: "jpg|jpeg|png|JPG|JPEG|PNG",
        },        
        password: {
            minlength: 8
        },
        password_confirmation: {
            minlength: 8,
            equalTo: "#password"
        },
        "role[]": "required"       
  
    },
    messages: {// custom messages for radio buttons and checkboxes
        'name': {
            required: js_lang.required
        },
        'email': {
            required: js_lang.email_required
        },
        'mobile': {
            required: js_lang.required,
            digits: js_lang.mobile_required,
            minlength: js_lang.mobile_required
        },
        'image': {
            accept: js_lang.correct_image
        },        
        'password': {
            minlength: js_lang.minlength_password
        },        
         'password_confirmation': {
            minlength: js_lang.minlength_password,
            equalTo: js_lang.equalTo_pass
        }, 
        'role[]': {
            required: js_lang.required
        },        

    },
    invalidHandler: function (event, validator) { //display error alert on form submit
        error1.show();
        $('#admins-form-modal').animate({scrollTop: 0}, 600);
    },
    highlight: function (element) { // hightlight error inputs
        $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },
    success: function (label) {
        label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function (form) {
        form.submit();
    }
});


