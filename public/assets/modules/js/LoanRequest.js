var users_info = {};
var form1 = $('#admins_form');
/**
 * datatable ajax
 * @type {jQuery}
 */
$('#table_view').GetDataTable({
    route : urls.get_data_url,
    order: 0,
    paginate: 50,
    searching: false,
    fields:  [
        {
            "orderable": false,
            "name": "id",
            "data": function (row, type, val, meta) {
                return '<div style="width: 100%;text-align: center;"><input type="checkbox" name="ids[]" class="checkboxes" value="' + row.id + '" title="" /></div>';
            },width:'1%'
        },
        {
            "orderable": false,
            "data": function (row, type, val, meta) {
                    return (row.amount)?row.amount:'';
            }
        },
        {
            "orderable": false,
            "data": function (row, type, val, meta) {
                    return (row.user_name)?row.user_name:'';
            }
        },
        {"data": "created_at","name": "created_at", "orderable": false},
        {
            "orderable": false,
            "data": function (row, type, val, meta) {
                var html = '<div style="width: 100%;text-align: center;">';
                    html += '<a href="' + urls.edit_url + '/' + row.id + '" title="' + js_lang.edit_record + '" class="btn btn-sm blue"><i class="fa fa-edit"></i> ' + js_lang.edit_record + '</a>';
                    html += '<a title="' + js_lang.delete_record + '" class="btn btn-sm red" onclick="deleteRow(' + row.id + ')"><i class="fa fa-trash-o"></i> ' + js_lang.delete_record + '</a>';
                html += '</div>';
                return html;
            } ,"width": "30%"
        }
    ]
});

/**
 *
 * @param id
 */
function deleteRow(id) {
    bootbox.confirm(js_lang.confirm_delete, function (result) {
        if (result) {
            var my_data = {};
            my_data.id = id;
            //my_data._token = token_code;
            $.post(urls.delete_url, my_data, function (data) {
                if (data[0]) {
                    showNotify('success', js_lang[data[1]]);
                } else {
                    if (js_lang[data[1]]) {
                        showNotify('error', js_lang[data[1]]);
                    } else {
                        showNotify('error', model_js_lang[data[1]]);
                    }
                }
                dataTable.ajax.reload();
            }, 'json');
        }
    });
}

/**
 *
 * @param id
 * @param type
 */
function updateStatus(id,type) {
    bootbox.confirm(model_js_lang.update_status_confirm, function (result) {
        if (result) {
            var my_data = {};
            my_data.id = id;
            if(type==0){
                my_data.status=0;
            }else{
                my_data.status=1;
            }
            $.post(urls.update_url, my_data, function (data) {
                if (data[0]) {
                    showNotify('success', model_js_lang[data[1]]);
                } else {
                    if (js_lang[data[1]]) {
                        showNotify('error', js_lang[data[1]]);
                    } else {
                        showNotify('error', model_js_lang[data[1]]);
                    }
                }
                dataTable.ajax.reload();
            }, 'json');
        }
    });
}

$("#data_delete_btn").click(function () {
    if ($('input[name="ids[]"]:checked').length) {
        bootbox.confirm(js_lang.confirm_delete, function (result) {
            if (result) {
                var ids = [];
                $.each($('table#table_view tbody input[type="checkbox"]:checked'), function (x, y) {
                    ids.push($(y).val());
                });
                var my_data = {};
                my_data.ids = ids;
                $.post(urls.delete_multi_url, my_data, function (data) {
                    dataTable.ajax.reload();
                    bootbox.hideAll();
                    if (data[0]) {
                        showNotify('success', js_lang[data[1]]);
                    } else {
                        if (js_lang[data[1]]) {
                            showNotify('error', js_lang[data[1]]);
                        } else {
                            showNotify('error', model_js_lang[data[1]]);
                        }
                    }
                }, 'json');
            }
        });
    } else {
        showNotify('error', js_lang.box_message);
        return false;
    }
});
var form1 = $('#add_form');
var error1 = $('.alert-danger', form1);
form1.validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    rules: {
        'allowed_users': {
            required: true,
            number: true
        },
        'reward': {
            required: true,
            number: true
        },
        'deduction': {
            required: true,
            number: true
        },
        'correct': {
            required: true
        },
        'from': {
            required: true
        },
        'to': {
            required: true
        },
        'title[ar]': {
            required: true
        },
        'title[en]': {
            required: true
        },
        'answer[ar][]': {
            required: true
        },
        'answer[en][]': {
            required: true
        }
    },
    messages: {
        'allowed_users': {
            required: js_lang.required,
            number: js_lang.number
        },
        'reward': {
            required: js_lang.required,
            number: js_lang.number
        },
        'deduction': {
            required: js_lang.required,
            number: js_lang.number
        },
        'title[ar]': {
            required: js_lang.required
        },
        'title[en]': {
            required: js_lang.required
        },
        'answer[ar][]': {
            required: js_lang.required
        },
        'answer[en][]': {
            required: js_lang.required
        },
        'correct': {
            required: js_lang.required
        },
        'from': {
            required: js_lang.required
        },
        'to': {
            required: js_lang.required
        }
    },
    invalidHandler: function(event, validator) { //display error alert on form submit
        error1.show();
        App.scrollTo(error1, -200);
    },
    errorPlacement: function (error, element) { // render error placement for each input type
        if (element.parents('.mt-radio-list').size() > 0 || element.parents('.mt-checkbox-list').size() > 0) {
            if (element.parents('.mt-radio-list').size() > 0) {
                error.appendTo(element.parents('.mt-radio-list')[0]);
            }
            if (element.parents('.mt-checkbox-list').size() > 0) {
                error.appendTo(element.parents('.mt-checkbox-list')[0]);
            }
        } else if (element.parents('.mt-radio-inline').size() > 0 || element.parents('.mt-checkbox-inline').size() > 0) {
            if (element.parents('.mt-radio-inline').size() > 0) {
                error.appendTo(element.parents('.mt-radio-inline')[0]);
            }
            if (element.parents('.mt-checkbox-inline').size() > 0) {
                error.appendTo(element.parents('.mt-checkbox-inline')[0]);
            }
        } else if (element.parent(".input-group").size() > 0) {
            error.insertAfter(element.parent(".input-group"));
        } else if (element.attr("data-error-container")) {
            error.appendTo(element.attr("data-error-container"));
        } else {
            error.insertAfter(element); // for other inputs, just perform default behavior
        }
    },
    highlight: function(element) { // hightlight error inputs
        $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    success: function(label) {
        label
            .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function(form) {
        form.submit(); // form validation success, call ajax form submit
    }
});


