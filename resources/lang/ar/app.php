<?php

return [
    'dir' => 'rtl',
    'ar'=>'عربى',
    'en'=>'English',
    'save'=>'حفظ',
    'reset'=>'استرجاع',
    'logout'=>'تسجيل خروج',
    'home'=>'الرئيسية',
    'lang'=>'English',
    'create'=>'إضافة',
    'delete_selected'=>'حذف المحدد',
    'active_selected'=>'تفعيل المحدد',
    'deActive_selected'=>'الغاء تفعيل المحدد',
    'operations'=>'عمليات',
    'created_at'=>'التاريخ',
	'edit_profile'=>'الملف الشخصي',
    'loading'=>'جارى الحفظ',
];
