<?php

return [
    'page_title' => 'المدن',
    'content_title' => 'المدن',
    'table_title' => 'عرض المدن',
    'page_title_form' => 'اضافة مدينة',
    'title' => 'اسم المدينة',
    'country' => 'الدولة',
    'status' => 'الحالة',
    'js_lang' =>[
        'success_update_status'=>'تم تعديل حالة المدينة بنجاح',
        'error_update_status'=>'حدث خطأ أثناء تعديل حالة المدينة',
        'update_status_confirm'=>'هل انت متأكد من تعديل حالة المدينة',
        'cities_add_error'=>'حدث خطأ أثناء اضافة المدينة',
        'translation_add_error'=>'حدث خطأ أثناء اضافة الترجمة',
        'success_add_cities'=>'تم اضافة المدينة بنجاح',
        'database_error'=>'حدث خطأ غير متوقع',
        'success_update_cities'=>'تم تعديل بيانات المدينة بنجاح',
        'cities_update_error'=>'فشل فى تعديل بيانات المدينة',
        'add_region' =>'إضافة منطقة',
        'regions' => 'المناطق',           
    ],
];
