<?php

return [
    'page_title' => 'الدول',
    'content_title' => 'الدول',
    'table_title' => 'عرض الدول',
    'page_title_form' => 'اضافة دولة',
    'title' => 'اسم الدولة',
    'code' => 'الكود',
    'prefix' => 'مفتاح البدئ',
    'currency' => 'العملة',
    'status' => 'الحالة',
    'js_lang' =>[
        'success_update_status'=>'تم تعديل حالة الدولة بنجاح',
        'error_update_status'=>'حدث خطأ أثناء تعديل حالة الدولة',
        'update_status_confirm'=>'هل انت متأكد من تعديل حالة الدولة؟',
        'countries_add_error'=>'حدث خطأ أثناء اضافة الدولة',
        'translation_add_error'=>'حدث خطأ أثناء اضافة الترجمة',
        'success_add_countries'=>'تم اضافة الدولة بنجاح',
        'database_error'=>'حدث خطأ غير متوقع',
        'success_update_countries'=>'تم تعديل بيانات الدولة بنجاح',
        'countries_update_error'=>'فشل فى تعديل بيانات الدولة',
        'add_city' => 'إضافة مدينة',
        'cities' => 'المدن',
    ],
];
