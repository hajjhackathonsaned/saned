<?php

return [
    'page-title' => 'دواء',
    'search-for' => 'ابحث عن',
    'page-header' => 'افضل الخدمات الطبيه من حولك',
    'register' => 'التسجيل',
	'welcome' => 'مرحبا بك',
	'login' => 'تسجيل دخول',
	'sign-out' => 'تسجيل خروج',
	'profile' => 'ملفك الشخصي',	
	'site-close' => 'الموقع تحت الصيانة',
	'register-close' => 'التسجيل مغلق',
	'my-reservation' => 'حجوزاتي',
	'clinic' => 'عيادة',
	'doctor' => 'دكتور',
	'hospital' => 'مستشفي',
	'follow-us' => 'تابعنا علي',
	'contact-us' => 'تواصل معنا',
	];
