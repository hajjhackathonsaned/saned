<?php

return [
    'dashboard' => 'الرئيسية',
    'moderators' => 'المستخدمين',
    'users' => 'المستخدمين',
    'roles' => 'الصلاحيات',
    'setting' => 'الإعدادات',
    'general_setting' => 'الاعدادات العامة',
    'sms_setting' => 'الرسائل القصيرة',
    'mail_setting' => 'البريد الالكترونى',
    'social_setting' => 'التواصل الاجتماعى',
    'archive' => 'ارشيف الرسائل',
    'content' => 'ادارة البيانات',
    'blog' => 'المقالات',
    'countries' => 'الدول',
    'cities' => 'المدن',

    ];
