<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'يجب أن لا يقل طول كلمة المرور عن ستة أحرف، كما يجب أن تتطابق مع حقل التأكيد',
    'reset'    => 'تمت إعادة تعيين كلمة المرور',
    'sent'     => 'تم إرسال تفاصيل استعادة كلمة المرور الخاصة بك إلى بريدك الإلكتروني',
    'token'    => '.رمز استعادة كلمة المرور الذي أدخلته غير صحيح',
    'user'     => 'لم يتم العثور على أيّ حسابٍ بهذا العنوان الإلكتروني',
    'email'     => 'البريد الالكترونى',
    'back'     => 'رجوع',
    'reste_pass'     => 'ارسال',
    'forget'     => 'نسيت كلمة المرور ؟',
    'forget-info'     => 'ادخل البريد الالكترونى بالاسفل لتتمكن من استعادة كلمة المرور .',
    'password_field'     => 'كلمة المرور',
    'password_field_conf'     => 'تأكيد كلمة المررو',
    'reset_password'     => 'استعادة كلمة المرور',

];
