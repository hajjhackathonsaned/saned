<?php

return [
    'page_title' => 'المناطق',
    'content_title' => 'المناطق',
    'table_title' => 'عرض المناطق',
    'page_title_form' => 'اضافة منطقة',
    'title' => 'اسم المنطقة',
    'country' => 'الدولة',
    'city' => 'المدينة',
    'status' => 'الحالة',
    'js_lang' =>[
        'success_update_status'=>'تم تعديل حالة المنطقة',
        'error_update_status'=>'حدث خطأ أثناء تعديل حالة المنطقة',
        'update_status_confirm'=>'هل انت متأكد من تعديل حالة المنطقة',
        'regions_add_error'=>'حدث خطأ أثناء اضافة المنطقة',
        'translation_add_error'=>'حدث خطأ أثناء اضافة الترجمة',
        'success_add_regions'=>'تم اضافة المنطقه',
        'database_error'=>'حدث خطأ غير متوقع',
        'success_update_regions'=>'تم تعديل بيانات المنطقه',
        'regions_update_error'=>'فشل فى تعديل بيانات المنطقة',
        'add_regions' =>'إضافة منطقة',
        'regions' => 'المناطق',        
    ],
];
