<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'page_title'   => 'إنشاء مدير جديد',
    'user_title'   => 'المديرين',
    'page_edit'   => 'تعديل المدير',
    'new-user'   => 'مدير جديد',
    'date'   => 'التاريخ',
    'name'   => 'الاسم',
    'email'   => 'البريد الالكتروني',
    'image'   => 'الصورة',
    'choose_image' => 'اختر الصورة',
    'mobile'   => 'الموبايل',
    'password'   => 'كلمة المرور',
    'password-confirmed'   => 'تأكيد كلمة المرور',
    'save'   => 'حفظ',
    'cancel'   => 'رجوع',
    'edit'   => 'تعــديل',
    'delete'   => 'حــذف',
    'not_delete' => 'لا تستطيع حذف نفسك',
    'operations' => 'عمليات',
    'success_delete' => 'تم الحذف',
    'roles' => 'الصلاحيات', 
    'success_add_user'=>'تمت اضافه المدير بنجاح',
    'success_edit_user'=>'تم تعديل المدير بنجاح',
    'error_add_user'=>'خطأ في اضافه المدير',
    'select_roles'=>'اختر الصلاحيات',
    'js_lang' =>[
        'user_add_error'=>'خطأ في انشاء المدير',
        'success_add_user'=>'تمت اضافه المدير بنجاح',
        'choose-rols' => 'اختر الصلاحيات',
        'success_edit_user'=>'تم تعديل المدير بنجاح',
    ],
    
    
];
