<?php

return [
    'dir' => 'ltr',
    'ar'=>'عربى',
    'en'=>'English',
    'save'=>'Save',
    'reset'=>'Reset',
    'logout'=>'Logout',
    'home'=>'Home',
    'lang'=>'Arabic',
    'create'=>'Add',
    'delete_selected'=>'Delete select',
    'operations'=>'Operations',
	'active_selected'=>'Activate selected',
    'deActive_selected'=>'Deactivate selected',
    'created_at'=>'Created at',
    'edit_profile'=>'Profile',
    'loading'=>'Loading save',
];
