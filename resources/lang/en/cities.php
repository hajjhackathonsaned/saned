<?php

return [
    'page_title' => 'Cities',
    'content_title' => 'Cities',
    'table_title' => 'Cities',
    'page_title_form' => 'Add City',
    'title' => 'City name',
    'country' => 'Country',
    'status' => 'Status',
    'js_lang' =>[
        'success_update_status'=>'City status updated successfully',
        'error_update_status'=>'Error in update status of city',
        'update_status_confirm'=>'Are you sure edit status of city?',
        'cities_add_error'=>'Error in add city',
        'translation_add_error'=>'Error in add translation',
        'success_add_cities'=>'City added successfully',
        'database_error'=>'Database Error',
        'success_update_cities'=>'City edited successfully',
        'cities_update_error'=>'Error in edit city',
        'add_region' =>'Add region',
        'regions' => 'Regions',           
    ],
];
