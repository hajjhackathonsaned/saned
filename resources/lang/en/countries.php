<?php

return [
    'page_title' => 'Countries',
    'content_title' => 'Countries',
    'table_title' => 'Countries',
    'page_title_form' => 'Add country',
    'title' => 'Add country',
    'code' => 'Code',
    'prefix' => 'Prefix',
    'currency' => 'Currency',
    'status' => 'Status',
    'js_lang' =>[
        'success_update_status'=>'Country status updated successfully',
        'error_update_status'=>'Error in added status of country',
        'update_status_confirm'=>'Are you sure to update status of country',
        'countries_add_error'=>'Error in added country',
        'translation_add_error'=>'Error in add translation',
        'success_add_countries'=>'Country added successfully',
        'database_error'=>'Database error',
        'success_update_countries'=>'Country updated successfully',
        'countries_update_error'=>'Error in update country',
        'add_city' => 'Add city',
        'cities' => 'Cities',
    ],
];
