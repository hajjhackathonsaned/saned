<?php

return [
    'page-title' => 'Dwaa',
    'search-for' => 'Search for',
    'page-header' => 'Best medical service',
    'register' => 'Register',
	'welcome' => 'Welcome',
	'login' => 'Login',
	'sign-out' => 'Sign out',
	'profile' => 'Profile',
	'site-close' => 'Under construction',
	'register-close' => 'Registration closed',
	'my-reservation' => 'My reservations',
	'clinic' => 'Clinic',
	'doctor' => 'Doctor',
	'hospital' => 'Hospital',
	'follow-us' => 'Folllow us',
	'contact-us' => 'Contact us',
];
