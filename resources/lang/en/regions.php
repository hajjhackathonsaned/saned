<?php

return [
    'page_title' => 'Regions',
    'content_title' => 'Regions',
    'table_title' => 'Regions',
    'page_title_form' => 'Add region',
    'title' => 'Region name',
    'country' => 'Country',
    'city' => 'City',
    'status' => 'Status',
    'js_lang' =>[
        'success_update_status'=>'Region status successfully updated',
        'error_update_status'=>'Error in update status of region',
        'update_status_confirm'=>'Are you sure to update status of region',
        'regions_add_error'=>'Error in adding region',
        'translation_add_error'=>'Error in adding translation',
        'success_add_regions'=>'Region added successfully',
        'database_error'=>'Database error',
        'success_update_regions'=>'Region updated successfully',
        'regions_update_error'=>'Error in udating region',
        'add_regions' =>'Add region',
        'regions' => 'Regions',        
    ],
];
