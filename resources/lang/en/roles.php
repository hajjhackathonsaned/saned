<?php

return [
    'page_title' => 'Roles',
    'content_title' => 'Roles',
    'table_title' => 'View all roles',
    'page_title_form' => 'Add new role',
    'name' => 'Title',
    'js_lang' =>[
        'add_error'=>'An error occurred while adding role',
        'success_add'=>'Role added successfully',
        'database_error'=>'Un expected error happened',
        'success_update'=>'role data has been successfully modified',
        'update_error'=>'Failed to modify role data',
        'role_already_exists'=>'Role already exists',
        'select_one_permission'=>'Must select one permission at least',
    ],
];
