<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'page_title'   => 'New admin',
    'user_title'   => 'Admins',
    'page_edit'   => 'Edit admin',
    'new-user'   => 'new admin',
    'date'   => 'Date',
    'name'   => 'Name',
    'email'   => 'Email',
    'image'   => 'Image',
    'choose_image' => 'Choose image',
    'mobile'   => 'Mobile',
    'password'   => 'Password',
    'password-confirmed'   => 'Password confirmation',
    'save'   => 'Save',
    'cancel'   => 'Cancel',
    'edit'   => 'Edit',
    'delete'   => 'Delete',
    'not_delete' => 'Not delete yourself',
    'operations' => 'Operations',
    'success_delete' => 'Deleted successfully',
    'roles' => 'Roles', 
    'success_add_user'=>'Success adding user',
    'success_edit_user'=>'Success edit user',
    'error_add_user'=>'Error in adding user',
	'select_roles'=>'Select roles',
    'js_lang' =>[
        'user_add_error'=>'Error in adding admin',
        'success_add_user'=>'Admin adding successfully',
        'choose-rols' => 'Choose roles',
        'success_edit_user'=>'Success edit user',
    ],
    
    
];
