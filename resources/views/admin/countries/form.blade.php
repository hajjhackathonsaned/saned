<div class="modal fade" id="admins-form-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('countries.page_title')}}</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="countries_form">
                    {!! csrf_field() !!}
                    <input type='hidden' name='id' value=''/>
                    <div class="form-body" id="">
                        <div class="alert alert-danger" style="display: none;">{{trans('common.correct_form_error')}}</div>

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach(config('app.supported_lang') as $lang)
                            <li role="presentation" class="@if ($lang == \App::getLocale())  active @endif"><a href="#{{$lang}}" aria-controls="#{{$lang}}" role="tab" data-toggle="tab">{{trans("app.{$lang}")}}</a></li>
                            @endforeach
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            @foreach(config('app.supported_lang') as $k=>$lang)
                            <div role="tabpanel" class="tab-pane @if ($lang == \App::getLocale())  active @endif" id="{{$lang}}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-md-line-input form-md-floating-label">
                                            <input type="text" class="form-control" name="title[{{$lang}}]" id="title_{{$lang}}"/>
                                            <label for="site_title">{{trans('countries.title')}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input id="code" type="text" name="code" value="" class="form-control"/>
                                    <label for="code">{{trans('countries.code')}}</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input id="prefix" type="text" name="prefix" value="" class="form-control"/>
                                    <label for="prefix">{{trans('countries.prefix')}}</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input id="currency" type="text" name="currency" value="" class="form-control"/>
                                    <label for="currrency">{{trans('countries.currency')}}</label>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{trans('app.loading')}}"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('common.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>