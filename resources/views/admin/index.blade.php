@extends('layouts.app')

@section('page-site-title')
    {{trans('dashboard.page_title')}}
@endsection

@section('page-title')
    {{trans('dashboard.content_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">

    </div>
@endsection

@section('page-footer-js')
    <script type="application/javascript">
        var model_js_lang = {!! json_encode(trans('dashboard.js_lang'))  !!};
    </script>
@endsection

@section('page-heads')
@endsection