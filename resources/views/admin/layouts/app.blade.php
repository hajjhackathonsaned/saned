<!DOCTYPE html>
<html lang="en" dir="{{trans('app.dir')}}">
    <head>
        <title>@yield('page-site-title') - {{config('general.site_title')}}</title>
        <meta content="{{config('site_meta_desc')}}" name="description" />
        @include('layouts.in-page-files.page-head')
        @yield('page-heads')
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">

            @yield('page_navbar')
            <div class="clearfix"> </div>
            <div class="page-container">
                @yield('page_side')
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-head">
                            <div class="page-title">
                                <h1>@yield('page-title')</h1>
                            </div>
                        </div>
                        @yield('page_breadcrumb')
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                            </div>
                        @endif
                        @if(Session::has('message'))
                            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                {{ Session::get('message') }}
                                <br />
                            </div>
                        @endif

                        <div class="row">
                            <div class='col-md-12'>
                                @yield('page-content')
                                @yield('page-content-footer')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-footer">
                <div class="page-footer-inner">  {{config('general.copy_right')}} &copy; {{date('Y')}}
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>

        <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
            <ul>
                @if(trans('app.dir')=='rtl')
                    <li>
                    <a href="{{route('change-language',['locale'=>'en'])}}" class="active">
                        <span>{{trans('app.lang')}}</span>
                        <i class="icon-map"></i>
                    </a>
                </li>
                @else
                    <li>
                        <a href="{{route('change-language',['locale'=>'ar'])}}" class="active">
                            <span>{{trans('app.lang')}}</span>
                            <i class="icon-map"></i>
                        </a>
                    </li>
                @endif

                <li>
                    <a href="{{route('profile')}}">
                        <span>{{trans('app.edit_profile')}}</span>
                        <i class="icon-users"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/showcast/" target="_blank">
                        <span>Showcase</span>
                        <i class="icon-user"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                        <span>Changelog</span>
                        <i class="icon-graph"></i>
                    </a>
                </li>
            </ul>
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
        <!-- JavaScripts -->
        <script>
            var js_lang = {!! json_encode(trans('common.js_lang'))  !!};
            var assets_path = '{{asset("assets")}}/';
        </script>
        @include('layouts.in-page-files.page-footer')
        @yield('page-footer-js')
    </body>
</html>
