<!DOCTYPE html>
<html lang="en" dir="{{trans('app.dir')}}" >
    <head>
        <meta charset="utf-8" />
        <title>@yield('page-site-title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/earlyaccess/droidarabickufi.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        @if(trans('app.dir')=='rtl')
            <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css')}}" rel="stylesheet" type="text/css" />

        @else
            <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />

        @endif

        <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        @if(trans('app.dir')=='rtl')
            <link href="{{ asset('assets/global/css/components-rtl.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('assets/global/css/plugins-rtl.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('assets/pages/css/login-5-rtl.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('assets/models/css/common.css')}}" rel="stylesheet" type="text/css" />
        @else
        <link href="{{ asset('assets/global/css/components.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/pages/css/login-5.min.css')}}" rel="stylesheet" type="text/css" />
        @endif
        <link rel="shortcut icon" href="{{ asset('assets/favicon.ico')}}" />
    </head>

    <body class=" login">
        @yield('content')
        <script>
            var js_lang = {!! json_encode(trans('common.js_lang'))  !!};
            var assets_path = '{{asset("assets")}}/';
        </script>
        <!--[if lt IE 9]>
        <script src="{{ asset('assets/global/plugins/respond.min.js')}}"></script>
        <script src="{{ asset('assets/global/plugins/excanvas.min.js')}}"></script>
        <script src="{{ asset('assets/global/plugins/ie8.fix.min.js')}}"></script>
        <![endif]-->
        <script src="{{ asset('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/backstretch/jquery.backstretch.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/models/js/login.js')}}" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>

</html>