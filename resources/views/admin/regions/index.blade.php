@extends('layouts.app')

@section('page-site-title')
    {{trans('regions.page_title')}}
@endsection

@section('page-title')
    {{trans('regions.content_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{trans('regions.table_title')}}</span>
                    </div>
                    <div class="pull-right">
                        <div class="table-toolbar pull-right">
                            @can('add.regions')
                            <button class="btn green" name="create" id="create_btn"><i class="fa fa fa-plus"></i>
                                {{trans('app.create')}}
                            </button>
                            @endcan
                            @can('delete.regions')
                            <button type="button" class="btn red" name="delete" id="data_delete_btn"><i class="fa fa-trash-o"></i>
                                {{trans('app.delete_selected')}}
                            </button>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="" id='table_form' method="post">
                        <table class="table table-striped table-hover table-bordered table-checkbox table-advance table-with-sorting" id="table_view">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div style="width: 100%;text-align: center;">
                                            <input type="checkbox" id="table_checkall" data-set="#table_view .checkboxes"/>
                                            </div>
                                        </th>
                                        <th>{{trans('regions.title')}}</th>
                                        <th>{{trans('regions.city')}}</th>
                                        <th>{{trans('regions.status')}}</th>
                                        <th>{{trans('app.created_at')}}</th>
                                        <th>{{trans('app.operations')}}</th>
                                    </tr>
                                    </thead>
                                </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('regions.form')
@endsection

@section('page-footer-js')
    <script type="application/javascript">
        var model_js_lang = {!! json_encode(trans('regions.js_lang'))  !!};
        var urls = {!! json_encode($urls) !!};
        var city = {{$city_id}};
        var permissions = {!! json_encode($permissions) !!};
    </script>
    <script type = "text/javascript" src = "{{ asset('assets/global/plugins/datatables/datatables.min.js')}}" ></script>
    <script src = "{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/models/js/regions.js')}}" type = "text/javascript" ></script>

@endsection

@section('page-heads')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    @if(trans('app.dir') == 'rtl')
        <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css') }}" rel="stylesheet" type="text/css" />
    @else
        <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    @endif
@endsection