<div class="modal fade" id="admins-form-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans('roles.page_title')}}</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" class="form-horizontal" id="users_form">
                    {!! csrf_field() !!}
                    <input type='hidden' name='id' value=''/>
                    <div class="form-body" id="">
                        <div class="alert alert-danger" style="display: none;">{{trans('common.correct_form_error')}}</div>
                        <div class="form-group">
                            <label class="control-label col-md-3">{{trans('roles.name')}}<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="name"/>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover table-bordered table-checkbox table-advance" id="table_view">
                                    <thead>
                                    <tr>
                                        <td colspan="1">
                                            {{trans('common.check_all')}}
                                        </td>
                                        <td colspan="5">
                                            <label><input data-size="small" type="checkbox" name="" value="1" class="make-switch" id="check_all_permissions" data-on-color="success" data-off-color="danger" data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"/>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{trans('common.permission_group')}}</th>
                                        <th>{{trans('common.check_row')}}</th>
                                        <th>{{trans('common.create')}}</th>
                                        <th>{{trans('common.delete')}}</th>
                                        <th>{{trans('common.edit')}}</th>
                                        <th>{{trans('common.show')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($permissions_list)
                                        @foreach($permissions_list as $group => $row)
                                            <tr>
                                                <td>
                                                    {{trans('menu.' . $group)}}
                                                </td>
                                                <td>
                                                    <label>
                                                        <input data-size="mini" type="checkbox" name="" value="1" class="make-switch permission_row {{$group}}_check_row" data-on-color="success" data-off-color="danger" data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">
                                                    </label>
                                                </td>
                                                @foreach($row as $id => $field)
                                                    <td>
                                                        <input data-size="mini" type="checkbox" name="permission[{{$field}}.{{$group}}]" value="1" class="make-switch permission_check {{$group}}_check permission_{{$id}}" data-on-color="success" data-off-color="danger" data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{trans('app.loading')}}"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('common.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>