@extends('layouts.app')

@section('page-site-title')
    {{trans('setting.page_title')}}
@endsection

@section('page-title')
    {{trans('setting.page_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">
        <form method="post" action="{{route('setting::update')}}">
            <input type="hidden" name="type" value="{{$group}}">
        <div class="col-md-12">
            <div class="portlet light portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red sbold uppercase"> {{trans('setting.page_title_form_'.$group)}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                        <div class="form-body">
                            {!! csrf_field() !!}
                            <div class="form-group form-md-line-input form-md-floating-label">
                                    <input value="@if(isset($site_title)){{$site_title}}@endif"  id="site_title" name="site_title" class="form-control" type="text">
                                    <label for="site_title">{{trans('setting.app_name')}}</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                    <input value="@if(isset($version)){{$version}}@endif"  id="version" name="version" class="form-control number" type="text">
                                    <label for="version">{{trans('setting.version')}}</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                    <input value="@if(isset($phone)){{$phone}}@endif"  id="phone"  name="phone" class="form-control number" type="text">
                                    <label for="phone">{{trans('setting.phone')}}</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                    <input value="@if(isset($email)){{$email}}@endif"  id="email" name="email" class="form-control number" type="text">
                                    <label for="email">{{trans('setting.email')}}</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                    <input value="@if(isset($copy_right)){{$copy_right}}@endif"  id="copy_right" name="copy_right" class="form-control" type="text">
                                    <label for="copy_right">{{trans('setting.copy_right')}}</label>
                            </div>

                            <div class="form-group form-md-line-input form-md-floating-label">
                                <label for="number_to">{{trans('setting.close_site')}}</label>
                                <input type="checkbox" name="close_site" @if($close_site==1) checked  @endif value="1" class="make-switch" id="test" data-size="small">
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    @can('edit.setting')
                                        <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                                    @endcan
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </form>
    </div>
@endsection

@section('page-footer-js')
    <script type="application/javascript">
    </script>
@endsection

@section('page-heads')
@endsection