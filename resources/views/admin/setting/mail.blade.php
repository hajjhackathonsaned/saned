@extends('layouts.app')

@section('page-site-title')
    {{trans('setting.page_title')}}
@endsection

@section('page-title')
    {{trans('setting.page_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class=" fa fa-envelope font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">  {{trans('setting.page_title_form_'.$group)}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" method="post" action="{{route('setting::update')}}">
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="mail_driver">{{trans('setting.mail_driver')}} </label>
                                <div class="col-md-5">
                                    <select id="mail_driver"  name="mail_driver" class="form-control" >

                                        <option value="mail" @if(isset($mail_driver) && $mail_driver=='mail')selected="selected"@endif >Mail</option>
                                        <option value="smtp" @if(isset($mail_driver) && $mail_driver=='smtp')selected="selected"@endif>Smtp</option>
                                        <option value="ses" @if(isset($mail_driver) && $mail_driver=='ses')selected="selected"@endif>Ses</option>
                                        <option value="sendmail" @if(isset($mail_driver) && $mail_driver=='sendmail')selected="selected"@endif>Sendmail</option>
                                        <option value="mailgun" @if(isset($mail_driver) && $mail_driver=='mailgun')selected="selected"@endif>Mailgun</option>
                                        <option value="mandrill" @if(isset($mail_driver) && $mail_driver=='mandrill')selected="selected"@endif>Mandrill</option>

                                        <option value="sparkpost" @if(isset($mail_driver) && $mail_driver=='sparkpost')selected="selected"@endif>Sparkpost</option>
                                    </select>
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div id="smtp"  class="driver_group"  @if(isset($mail_driver) && $mail_driver!='smtp') style="display: none;" @endif>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_smtp_host">{{trans('setting.mail_smtp_host')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_smtp_host" placeholder="{{trans('setting.mail_smtp_host')}}"
                                               value="@if(isset($mail_smtp_host)){{$mail_smtp_host}}@endif" name="mail_smtp_host" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_smtp_port">{{trans('setting.mail_smtp_port')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_smtp_port" placeholder="{{trans('setting.mail_smtp_port')}}"
                                               value="@if(isset($mail_smtp_port)){{$mail_smtp_port}}@endif" name="mail_smtp_port" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_smtp_username">{{trans('setting.mail_smtp_username')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_smtp_username" placeholder="{{trans('setting.mail_smtp_username')}}"
                                               value="@if(isset($mail_smtp_username)){{$mail_smtp_username}}@endif" name="mail_smtp_username" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_smtp_password">{{trans('setting.mail_smtp_password')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_smtp_password" placeholder="{{trans('setting.mail_smtp_password')}}"
                                               value="@if(isset($mail_smtp_password)){{$mail_smtp_password}}@endif" name="mail_smtp_password" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_smtp_encryption">{{trans('setting.mail_smtp_encryption')}} </label>
                                    <div class="col-md-5">
                                        <select id="mail_smtp_encryption"   name="mail_smtp_encryption" class="form-control">
                                            <option value="-" @if(isset($mail_smtp_encryption) && $mail_smtp_encryption=='-') selected="selected" @endif>No Encryption</option>
                                            <option value="ssl" @if(isset($mail_smtp_encryption) && $mail_smtp_encryption=='ssl') selected="selected" @endif>SSl</option>
                                            <option value="tls" @if(isset($mail_smtp_encryption) && $mail_smtp_encryption=='tls') selected="selected" @endif>Tls</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ses"  class="driver_group" @if(isset($mail_driver) && $mail_driver!='ses') style="display: none;" @endif>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_ses_key">{{trans('setting.mail_ses_key')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_ses_key" placeholder="{{trans('setting.mail_ses_key')}}"
                                               value="@if(isset($mail_ses_key)){{$mail_ses_key}}@endif" name="mail_ses_key" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_ses_secret">{{trans('setting.mail_ses_secret')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_ses_secret" placeholder="{{trans('setting.mail_ses_secret')}}"
                                               value="@if(isset($mail_ses_secret)){{$mail_ses_secret}}@endif" name="mail_ses_secret" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div id="mailgun" class="driver_group " @if(isset($mail_driver) && $mail_driver!='mailgun') style="display: none;" @endif>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_mailgun_domain">{{trans('setting.mail_mailgun_domain')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_mailgun_domain" placeholder="{{trans('setting.mail_mailgun_domain')}}"
                                               value="@if(isset($mail_mailgun_domain)){{$mail_mailgun_domain}}@endif" name="mail_mailgun_domain" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_mailgun_secret">{{trans('setting.mail_mailgun_secret')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_mailgun_secret" placeholder="{{trans('setting.mail_mailgun_secret')}}"
                                               value="@if(isset($mail_mailgun_secret)){{$mail_mailgun_secret}}@endif" name="mail_mailgun_secret" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div id="mandrill" class="driver_group"  @if(isset($mail_driver) && $mail_driver!='mandrill') style="display: none" @endif>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_mandrill_secret">{{trans('setting.mail_mandrill_secret')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_mandrill_secret" placeholder="{{trans('setting.mail_mandrill_secret')}}"
                                               value="@if(isset($mail_mandrill_secret)){{$mail_mandrill_secret}}@endif" name="mail_mandrill_secret" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div id="sparkpost" class="driver_group" @if(isset($mail_driver) && $mail_driver!='sparkpost') style="display: none" @endif>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_sparkpost_secret">{{trans('setting.mail_sparkpost_secret')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_sparkpost_secret" placeholder="{{trans('setting.mail_sparkpost_secret')}}"
                                               value="@if(isset($mail_sparkpost_secret)){{$mail_sparkpost_secret}}@endif" name="mail_sparkpost_secret" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div id="sendmail" class="driver_group"  @if(isset($mail_driver) && $mail_driver!='sendmail') style="display: none;" @endif>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="mail_sendmail_path">{{trans('setting.mail_sendmail_path')}} </label>
                                    <div class="col-md-5">
                                        <input dir="ltr" id="mail_sendmail_path" placeholder="{{trans('setting.mail_sendmail_path')}}"
                                               value="@if(isset($mail_sendmail_path)){{$mail_sendmail_path}}@endif" name="mail_sendmail_path" class="form-control"
                                               type="text">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    @can('edit.setting')
                                    <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                               @endcan
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-footer-js')
    <script type="text/javascript">
        $(function(){
            $('select#mail_driver').change(function(){
                $('div.driver_group').hide();
                $('div.driver_group#'+$(this).val()).show();
            }) ;
        });
    </script>
@endsection

@section('page-heads')
@endsection