@extends('layouts.app')

@section('page-site-title')
    {{trans('setting.page_title')}}
@endsection

@section('page-title')
    {{trans('setting.page_title')}}
@endsection

@section('page_navbar')
    @include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
    @include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
    @include('layouts.widgets.page-side')
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="portlet light portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class=" icon-social-dribbble font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">  {{trans('setting.page_title_form_'.$group)}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" method="post" action="{{route('setting::update')}}">
                        <div class="form-body">
                            {!! csrf_field() !!}
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="facebook">Facebook </label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-facebook-official"></i>
                                        </span>
                                        <input value="@if(isset($facebook)){{$facebook}}@endif"  id="facebook" placeholder="Facebook"  name="facebook" class="form-control" type="url">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="twitter">Twitter </label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-twitter-square"></i>
                                        </span>
                                        <input value="@if(isset($twitter)){{$twitter}}@endif"  id="twitter" placeholder="Twitter"  name="twitter" class="form-control" type="url">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="youtube">Youtube </label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-youtube-square"></i>
                                        </span>
                                        <input value="@if(isset($youtube)){{$youtube}}@endif"  id="youtube" placeholder="Youtube"  name="youtube" class="form-control" type="url">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="instagram">Instagram </label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-instagram"></i>
                                        </span>
                                    <input value="@if(isset($instagram)){{$instagram}}@endif"  id="instagram" placeholder="Instagram"  name="instagram" class="form-control" type="url">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    @can('edit.setting')
                                    <button type="submit" class="btn btn-success" id="add_admins_btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> جارى الحفظ"><i class="fa fa-save"></i> {{trans('common.save')}}</button>
                                @endcan
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection

@section('page-footer-js')
    <script type="application/javascript">
    </script>
@endsection

@section('page-heads')
@endsection