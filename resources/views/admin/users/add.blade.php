@extends('layouts.app')

@section('page-site-title')
@if(isset($user))
{{trans('users.page_edit')}} {{$user->name}}
@else
{{trans('users.page_title')}}
@endif
@endsection

@section('page-title')
@if(isset($user))
{{trans('users.page_edit')}} {{$user->name}}
@else
{{trans('users.page_title')}}
@endif
@endsection

@section('page_navbar')
@include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
@include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
@include('layouts.widgets.page-side')
@endsection

@section('page-content')
<div class="col-md-8 ">
    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-user font-green"></i>
                <span class="caption-subject bold uppercase">@if(isset($user)) {{$user->name}} @else {{trans('users.new-user')}} @endif</span>
            </div>
        </div>
        <div class="portlet-body form">
            @if(isset($user))
            <form id="users_form" role="form" action="{{route('user::update')}}" method="POST" enctype="multipart/form-data">              
                @else
                <form  id="users_form" role="form" action="{{route('user::create')}}" method="POST" enctype="multipart/form-data">
                    @endif    
                    {!! csrf_field() !!}
                    @if(isset($user))
                    <input type="hidden" value="{{$user->id}}" name="id" />
                    @endif

                    <div class="alert alert-danger" style="display: none;">{{trans('common.correct_form_error')}}</div>
                    <div class="form-body">
                        <div class="form-group">
                            <select id="multiple" name="role[]" class="form-control select2-multiple" multiple>
                                @foreach ($roles as $role)
                                <option value="{{$role->name}}" @if(isset($user)) @foreach($user_roles as $roles) @if($role->name == $roles->name) selected  @endif  @endforeach @endif>{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input type="text" name="name" class="form-control input-sm"  value="@if(isset($user)){{$user->name}}@else{{old('name')}}@endif">
                            <label for="name">{{trans('users.name')}}</label>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input type="text" name="email" class="form-control input-sm" value="@if(isset($user)){{$user->email}}@else{{old('email')}}@endif">
                            <label for="email">{{trans('users.email')}}</label>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input type="text" name="mobile" class="form-control input-sm"  value="@if(isset($user)){{$user->mobile}}@else{{old('mobile')}}@endif">
                            <label for="mobile">{{trans('users.mobile')}}</label>
                        </div>  
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                    @if(isset($user) && ($user->image != NULL) )
                                    <img src="{{asset('uploads/users/'.$user->image)}}" />
                                    @else
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                    @endif
                                </div>
                                <div>
                                    <span class="btn red btn-outline btn-file">
                                        <span class="fileinput-new"> {{trans('users.choose_image')}} </span>
                                        <span class="fileinput-exists"> {{trans('common.change')}} </span>
                                        <input type="file" name="image"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('common.remove')}} </a>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input type="password" name="password" id="password" class="form-control input-sm">
                            <label for="password">{{trans('users.password')}}</label>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input type="password" name="password_confirmation" class="form-control input-sm">
                            <label for="password_confirmation">{{trans('users.password-confirmed')}}</label>
                        </div>

                        <div class="form-actions noborder">
                            <button type="submit" class="btn blue">{{trans('users.save')}}</button>
                            <a href="#" class="btn default">{{trans('users.cancel')}}</a>
                        </div>
                </form>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
</div>

@endsection

@section('page-footer-js')
<script type="application/javascript">
    var model_js_lang = {!! json_encode(trans('users.js_lang'))  !!};
    var placeholder = '{{trans('users.select_roles')}}';

    $(".select2, .select2-multiple").select2({
    placeholder: placeholder,
    width: 630,
    });
</script>
<script src = "{{ asset('assets/models/js/users_form.js')}}" type = "text/javascript" ></script>
<script src = "{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type = "text/javascript" ></script>
@endsection

@section('page-heads')
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
