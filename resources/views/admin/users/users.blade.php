@extends('layouts.app')

@section('page-site-title')
{{trans('users.user_title')}}
@endsection

@section('page-title')
{{trans('users.user_title')}}
@endsection

@section('page_navbar')
@include('layouts.widgets.page-navbar')
@endsection

@section('page_breadcrumb')
@include('layouts.widgets.page-breadcrumb')
@endsection

@section('page_side')
@include('layouts.widgets.page-side')
@endsection

@section('page-content')
<div class="row">
    <div class="col-lg-12">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> {{trans('users.user_title')}}</span>
            </div>
            <div class="pull-right">
                <div class="table-toolbar pull-right">
                    <a href="{{route('user::add')}}" class="btn green" name="create" id="create_btn"><i class="fa fa fa-plus"></i>
                        {{trans('users.new-user')}}
                    </a>
                </div>
            </div>
        </div>    
        <div class="portlet-body">
            <table class="table table-striped table-hover table-bordered table-checkbox table-advance table-with-sorting" id="table_view">
                <thead>
                    <tr>
                        <th> {{trans('users.name')}} </th>
                        <th> {{trans('users.email')}} </th>
                        <th> {{trans('users.mobile')}} </th>
                        <th> {{trans('users.date')}} </th>
                        <th> {{trans('users.operations')}} </th>
                    </tr>
                </thead>
            </table> 
        </div>
    </div>
    </div>     
</div>
    @endsection

    @section('page-footer-js')
    <script type="application/javascript">
        var model_js_lang = {!! json_encode(trans('dashboard.js_lang'))  !!};
        var urls = {!! json_encode($urls) !!};
    </script>
    <script type = "text/javascript" src = "{{ asset('assets/global/plugins/datatables/datatables.min.js')}}" ></script>
    <script src = "{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type = "text/javascript" ></script>
    <script src = "{{ asset('assets/models/js/users.js')}}" type = "text/javascript" ></script>
    @endsection

    @section('page-heads')
        <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
        @if(trans('app.dir') == 'rtl')
            <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css') }}" rel="stylesheet" type="text/css" />
        @else
            <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        @endif
    @endsection
