<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

$this->get('/', function () {
    return redirect()->route('dashboard');
});
$local = \Request::segment(1);
if (in_array($local,config('app.supported_lang'))) {
    \App::setLocale($local);
} else {
    $local = \App::getLocale();
}
$this->group(['prefix' => $local, 'middleware' => ['setting']], function () {
    $this->get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('auth-login-form');
    $this->get('auth/login', 'Admin\Auth\LoginController@login')->name('auth-login-action');
    $this->post('auth/login', 'Admin\Auth\LoginController@login')->name('auth-login-action');
    $this->get('auth/logout', 'Admin\Auth\LoginController@logout')->name('auth-logout-action');

    $this->group(['prefix' => 'admin', 'middleware' => ['auth:web']], function () {
        $this->get('/dashboard', 'Admin\HomeController@index')->name('dashboard');
        $this->get('/changeLang/{locale}', ['as' => 'change-language', 'uses' => 'Admin\HomeController@changeLang']);
        $this->get('/profile', ['as' => 'profile', 'uses' => 'Admin\HomeController@profile']);
        $this->post('/profile/saveProfile', ['as' => 'save-profile', 'uses' => 'Admin\HomeController@saveProfile']);
        $this->post('/profile/saveAvatar', ['as' => 'save-avatar', 'uses' => 'Admin\HomeController@saveAvatar']);
        $this->post('/profile/savePass', ['as' => 'save-pass', 'uses' => 'Admin\HomeController@savePass']);

        //users
        $this->group(['prefix' => 'user', 'as' => 'user::'], function () {
            $this->get('/', ['middleware' => ['permission:show.users'], 'as' => 'index', 'uses' => 'Admin\UsersController@index']);
            $this->get('/add', ['middleware' => ['permission:add.users'], 'as' => 'add', 'uses' => 'Admin\UsersController@add']);
            $this->post('/create', ['middleware' => ['permission:add.users'], 'as' => 'create', 'uses' => 'Admin\UsersController@create']);
            $this->post('/getTableData/', ['middleware' => ['permission:show.users'], 'as' => 'get-table-data', 'uses' => 'Admin\UsersController@getData']);
            $this->get('/edit/{id}', ['middleware' => ['permission:edit.users'], 'as' => 'edit', 'uses' => 'Admin\UsersController@edit']);
            $this->post('/update', ['middleware' => ['permission:edit.users'], 'as' => 'update', 'uses' => 'Admin\UsersController@update']);
            $this->post('/destroy', ['middleware' => ['permission:delete.users'], 'as' => 'destroy', 'uses' => 'Admin\UsersController@destroy']);
        });

        // countries route
        $this->group(['prefix' => 'countries', 'as' => 'countries::'], function () {
            $this->get('/', ['middleware' => ['permission:show.countries'], 'as' => 'index', 'uses' => 'Admin\CountriesController@index']);
            $this->post('/getTableData/', ['middleware' => ['permission:show.countries'], 'as' => 'get-table-data', 'uses' => 'Admin\CountriesController@getData']);
            $this->post('/create', ['middleware' => ['permission:add.countries'], 'as' => 'create', 'uses' => 'Admin\CountriesController@create']);
            $this->post('/delete', ['middleware' => ['permission:delete.countries'], 'as' => 'delete', 'uses' => 'Admin\CountriesController@delete']);
            $this->post('/deleteMulti', ['middleware' => ['permission:delete.countries'], 'as' => 'delete-multi', 'uses' => 'Admin\CountriesController@deleteAll']);
            $this->post('/update', ['middleware' => ['permission:edit.countries'], 'as' => 'update', 'uses' => 'Admin\CountriesController@update']);
            $this->post('/updateStatus', ['middleware' => ['permission:edit.countries'], 'as' => 'update-status', 'uses' => 'Admin\CountriesController@updateStatus']);
        });

        // cities route
        $this->group(['prefix' => 'cities', 'as' => 'cities::'], function () {
            $this->get('/', ['middleware' => ['permission:show.cities'], 'as' => 'index', 'uses' => 'Admin\CitiesController@index']);
            $this->post('/getTableData/', ['middleware' => ['permission:show.cities'], 'as' => 'get-table-data', 'uses' => 'Admin\CitiesController@getData']);
            $this->post('/create', ['middleware' => ['permission:add.cities'], 'as' => 'create', 'uses' => 'Admin\CitiesController@create']);
            $this->post('/delete', ['middleware' => ['permission:delete.cities'], 'as' => 'delete', 'uses' => 'Admin\CitiesController@delete']);
            $this->post('/deleteMulti', ['middleware' => ['permission:delete.cities'], 'as' => 'delete-multi', 'uses' => 'Admin\CitiesController@deleteAll']);
            $this->post('/update', ['middleware' => ['permission:edit.cities'], 'as' => 'update', 'uses' => 'Admin\CitiesController@update']);
            $this->post('/updateStatus', ['middleware' => ['permission:edit.cities'], 'as' => 'update-status', 'uses' => 'Admin\CitiesController@updateStatus']);
        });

        // regions route
        $this->group(['prefix' => 'regions', 'as' => 'regions::'], function () {
            $this->get('/', ['middleware' => ['permission:show.regions'], 'as' => 'index', 'uses' => 'Admin\RegionsController@index']);
            $this->post('/getTableData/', ['middleware' => ['permission:show.regions'], 'as' => 'get-table-data', 'uses' => 'Admin\RegionsController@getData']);
            $this->post('/create', ['middleware' => ['permission:add.regions'], 'as' => 'create', 'uses' => 'Admin\RegionsController@create']);
            $this->post('/delete', ['middleware' => ['permission:delete.regions'], 'as' => 'delete', 'uses' => 'Admin\RegionsController@delete']);
            $this->post('/deleteMulti', ['middleware' => ['permission:delete.regions'], 'as' => 'delete-multi', 'uses' => 'Admin\RegionsController@deleteAll']);
            $this->post('/update', ['middleware' => ['permission:edit.regions'], 'as' => 'update', 'uses' => 'Admin\RegionsController@update']);
            $this->post('/updateStatus', ['middleware' => ['permission:edit.regions'], 'as' => 'update-status', 'uses' => 'Admin\RegionsController@updateStatus']);
        });

        // settings
        $this->group(['prefix' => 'setting', 'as' => 'setting::'], function () {
            $this->get('edit/{group}', ['as' => 'index', 'uses' => 'Admin\SettingController@index']);
            $this->post('/update', ['as' => 'update', 'uses' => 'Admin\SettingController@update']);
            $this->post('/save/template', ['as' => 'update-template', 'uses' => 'Admin\SettingController@updateTemplate']);
            $this->post('/test_send', ['as' => 'test_send', 'uses' => 'Admin\SettingController@send']);
            $this->post('/get_balance', ['as' => 'get_balance', 'uses' => 'Admin\SettingController@balance']);
        });
        // roles route
        $this->group(['prefix' => 'roles', 'as' => 'roles::'], function () {
            $this->get('/', ['middleware' => ['permission:show.roles'], 'as' => 'index', 'uses' => 'Admin\RolesController@index']);
            $this->post('/getTableData/', ['middleware' => ['permission:show.roles'], 'as' => 'get-table-data', 'uses' => 'Admin\RolesController@getData']);
            $this->post('create', ['middleware' => ['permission:add.roles'], 'as' => 'create', 'uses' => 'Admin\RolesController@create']);
            $this->post('/delete', ['middleware' => ['permission:delete.roles'], 'as' => 'delete', 'uses' => 'Admin\RolesController@delete']);
            $this->post('/deleteMulti', ['middleware' => ['permission:delete.roles'], 'as' => 'delete-multi', 'uses' => 'Admin\RolesController@deleteAll']);
            $this->post('/update', ['middleware' => ['permission:edit.roles'], 'as' => 'update', 'uses' => 'Admin\RolesController@update']);
        });

    });
    //front
    $this->group(['middleware' => ['close']], function () {
    });
});
